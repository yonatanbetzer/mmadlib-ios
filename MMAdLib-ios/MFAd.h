//
//  MFAd.h
//  MMAdLib-ios
//
//  Created by Yonatan Betzer on 10/24/14.
//  Copyright (c) 2014 mako. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VAST.h"

@interface MFAd : NSObject

@property (nonatomic, strong) NSString *adUrl;
@property BOOL isBumper;
@property BOOL played;
@property BOOL wasLoaded;
@property (nonatomic) int positionInPod;
@property (nonatomic) int pod;
@property (nonatomic, strong) VAST * vast;
+(MFAd*)fromObject:(NSObject*)obj;

@end
