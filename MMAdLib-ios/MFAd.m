//
//  MFAd.m
//  MMAdLib-ios
//
//  Created by Yonatan Betzer on 10/24/14.
//  Copyright (c) 2014 mako. All rights reserved.
//

#import "MFAd.h"

@implementation MFAd
{
    BOOL podSet;
    BOOL pposSet;
}

+(MFAd*)fromObject:(NSObject*)obj
{
    MFAd *ad = [[MFAd alloc] init];
    if([obj isKindOfClass:[NSString class]])
    {
        ad.adUrl = (NSString*)obj;
        ad.adUrl = [ad.adUrl stringByReplacingOccurrencesOfString:@"correlator=" withString:[NSString stringWithFormat:@"correlator=%.0f", [[NSDate date] timeIntervalSince1970]]];
        ad.isBumper = NO;
    }
    if([obj isKindOfClass:[NSDictionary class]])
    {
        NSDictionary *dic = (NSDictionary*)obj;
        ad.adUrl = dic[@"__text"];
        ad.adUrl = [ad.adUrl stringByReplacingOccurrencesOfString:@"correlator=" withString:[NSString stringWithFormat:@"correlator=%.0f", [[NSDate date] timeIntervalSince1970]]];
        ad.isBumper = dic[@"_bumper"] != nil;
    }
    
    return ad;
}

-(void)setPod:(int)pod
{
    _pod = pod;
    podSet = YES;
    if(podSet && pposSet)
    {
        [self updateUrlWithPodAndPpos];
    }
}

-(void)setPositionInPod:(int)positionInPod
{
    _positionInPod = positionInPod;
    pposSet = YES;
    if(podSet && pposSet)
    {
        [self updateUrlWithPodAndPpos];
    }
}

-(void)updateUrlWithPodAndPpos
{
    NSInteger positionOfCustParams = [self.adUrl rangeOfString:@"cust_params"].location;
    NSString *stuffToAdd = [NSString stringWithFormat:@"pod%%3D%d%%2Fppos%%3D%d%%2F", self.pod, self.positionInPod];
    self.adUrl = [NSString stringWithFormat:@"%@%@%@", [self.adUrl substringToIndex:positionOfCustParams + 12], stuffToAdd, [self.adUrl substringFromIndex:positionOfCustParams + 12]];
}

@end
