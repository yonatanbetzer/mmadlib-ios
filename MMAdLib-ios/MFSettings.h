//
//  MFSettings.h
//  MMAdLib-ios
//
//  Created by Yonatan Betzer on 10/23/14.
//  Copyright (c) 2014 mako. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MFSettings : NSObject

@property (nonatomic, strong) NSString *language;

@end
