//
//  MFManager.h
//  MMAdLib-ios
//
//  Created by Yonatan Betzer on 10/23/14.
//  Copyright (c) 2014 mako. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "MFAdRules.h"

#define DONE_BUTTON_TAG 297869124

@class MFManager;
@class MFEvent;

@protocol MFManagerDelegate <NSObject>

- (NSTimeInterval)currentTime;
- (void)adsManagerDidRequestContentPause:(MFManager *)adsManager;
- (void)adsManagerDidRequestContentResume:(MFManager *)adsManager;
- (void)adsManager:(MFManager *)adsManager didReceiveAdEvent:(MFEvent *)event;
- (void)adsManager:(MFManager *)adsManager didReceiveAdError:(NSDictionary *)error;

@end

@interface MFManager : NSObject
-(void)destroy;
-(void)pause;
-(void)resume;
-(void)contentComplete;
-(void)startOnView:(UIView*)view;

@property id delegate;
@property (nonatomic, strong) MFAdRules *adRules;

@property (nonatomic, strong) NSString* currentAdVideoUrl;

-(id)initWithAdRules:(MFAdRules*)adrules;
@end
