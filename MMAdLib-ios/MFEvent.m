//
//  MFEvent.m
//  MMAdLib-ios
//
//  Created by Yonatan Betzer on 10/23/14.
//  Copyright (c) 2014 mako. All rights reserved.
//

#import "MFEvent.h"

@implementation MFEvent

-(id)initWithType:(MFAdEventType)type
{
    self = [super init];
    if (self)
    {
        self.type = type;
    }
    return self;
}

@end
