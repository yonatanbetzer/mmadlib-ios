//
//  MFManager.m
//  MMAdLib-ios
//
//  Created by Yonatan Betzer on 10/23/14.
//  Copyright (c) 2014 mako. All rights reserved.
//

#import "MFManager.h"
#import "MFAd.h"
#import "MFEvent.h"
#import "VAST.h"
#import "MMAdManager.h"
#import "AVPlayer+Volume.h"
#import <sys/sysctl.h>
#import <CoreText/CoreText.h>

#define kNotificationRequestAd @"RequestAd"
#define SCREEN_WIDTH (MAX([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height))
#define SCREEN_HEIGHT (MIN([UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height))
#define FONT_REFORMA_NARROW(SIZE) [UIFont fontWithName:@"fbreformanarrow_regularRg" size:SIZE]
#define FONT_FB_REFORMA_NARROW(SIZE) [UIFont fontWithName:@"fbreformanarrow_regularRg" size:SIZE]
#define FONT_REFORMA_NARROW_LIGHT(SIZE) [UIFont fontWithName:@"fbreformanarrowlight" size:SIZE]
#define FONT_REFORMA_NARROW_MEDIUM(SIZE) [UIFont fontWithName:@"fbreformanarrowmedium" size:SIZE]
#define FONT_REFORMA_NARROW_BOLD(SIZE) [UIFont fontWithName:@"fbreformanarrow_boldbold" size:SIZE]


#import <AVFoundation/AVFoundation.h>

typedef void(^APIReturnsURL) (NSURL* url);

@interface MFManager(Private)

@property (strong) id playerObserver;

@end

@implementation MFManager
{
    NSTimer *podTimer;
    UIView *parentView;
    UIView *view;
    AVQueuePlayer *avPlayer;
    NSObject *avPlayerTimeObserver;
    MFPod *currentPod;
    MFAd *currentAd;
    MFAd *nextLoadedAd;
    UIView *doneButton;
    UIButton *moreInfoButton;
    UIButton *callForAction;
    UIView *headerBackgroundView;
    UIView *skipButton;
    UILabel *skipButtonText;

    BOOL missingAdURL;
    UIButton *playButton;
    BOOL destroyCalled;
    UITapGestureRecognizer *skipTap;
}

-(id)initWithAdRules:(MFAdRules*)adrules
{
    self = [super init];
    if(self)
    {
        self.adRules = adrules;
        [self loadFont:@"fbreformanarrow_boldbold"];
        [self loadFont:@"fbreformanarrow_regularRg"];
        [self loadFont:@"fbreformanarrowlight"];
        [self loadFont:@"fbreformanarrowmedium"];
    }
    return self;
}

-(void)loadFont:(NSString*)fileName
{
    NSString *fontPath = [[NSBundle bundleForClass:[MFManager class]] pathForResource:fileName ofType:@"ttf"];
    NSData *inData = [NSData dataWithContentsOfFile:fontPath];
    CFErrorRef error;
    CGDataProviderRef provider = CGDataProviderCreateWithCFData((CFDataRef)inData);
    CGFontRef font = CGFontCreateWithDataProvider(provider);
    if (! CTFontManagerRegisterGraphicsFont(font, &error)) {
        CFStringRef errorDescription = CFErrorCopyDescription(error);
        NSLog(@"Failed to load font: %@", errorDescription);
        CFRelease(errorDescription);
    }
    CFRelease(font);
    CFRelease(provider);
}

-(void)createAndAddMoreInfoButton {
    if(!moreInfoButton)
    {
        moreInfoButton = [[UIButton alloc] initWithFrame:CGRectMake(view.frame.size.width - 100, 0, 100,  30)];
    }
    [moreInfoButton setTitle:@"עוד פרטים" forState:UIControlStateNormal];
    moreInfoButton.titleLabel.font = [UIFont fontWithName:@"Arial" size:16];
    moreInfoButton.titleLabel.textColor = [UIColor whiteColor];
    [view addSubview:moreInfoButton];
    moreInfoButton.backgroundColor = [UIColor clearColor];
    [moreInfoButton addTarget:self action:@selector(moreInfoClicked) forControlEvents:UIControlEventTouchUpInside];
    if(!headerBackgroundView)
    {
        headerBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, view.frame.size.width, 35)];
    }
    headerBackgroundView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.6];
    
    if(!callForAction)
    {
        CGFloat fontSize = 35;
        CGFloat callForActionHeight = 80;
        NSString *deviceType = [self deviceType];
        if([deviceType isEqualToString:@"iPhone 6 Plus"] ||  [deviceType isEqualToString:@"iPhone 6S Plus"])
        {
            fontSize = 30;
            callForActionHeight = 55;
        }
        if([deviceType isEqualToString:@"iPhone 6"] || [deviceType isEqualToString:@"iPhone 6S"])
        {
            fontSize = 30;
            callForActionHeight = 55;
        }
        
        callForAction = [[UIButton alloc] initWithFrame:CGRectMake(0, view.frame.size.height - callForActionHeight, view.frame.size.width,  callForActionHeight)];
        callForAction.opaque = NO;
        callForAction.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.8];
        callForAction.titleLabel.font = [UIFont fontWithName:@"FbReformaNarrow-Medium" size:fontSize];
        
        [callForAction setTitle:@"לפרטים נוספים הקלק כאן >>" forState:UIControlStateNormal];
        [callForAction setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [callForAction addTarget:self action:@selector(moreInfoClicked) forControlEvents:UIControlEventTouchUpInside];
    }
    [view insertSubview:headerBackgroundView belowSubview:moreInfoButton];
    [view insertSubview:callForAction belowSubview:headerBackgroundView];
}

-(void)createSkipButton{
    NSLog(@"createSkipButton");
    if(skipButton == nil)
    {
        skipButton = [[UIView alloc] initWithFrame:CGRectMake(SCREEN_WIDTH - 110, SCREEN_HEIGHT / 2.0 - 20, 110, 40)];
        skipButton.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.6];
        skipTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(skipClicked:)];
        [skipButton addGestureRecognizer:skipTap];
        skipTap.enabled = NO;
    }
    skipButton.hidden = YES;
    [view insertSubview:skipButton belowSubview:headerBackgroundView];
}

-(void)updateSkipButtonWithTime:(CGFloat)time{
    NSLog(@"updateSkipButtonWithTime %.0f / %.0f", time, currentAd.vast.skipOffset);
    if(currentAd.vast.skipOffset > 0)
    {
        skipButton.hidden = NO;
        if(skipButtonText == nil)
        {
            skipButtonText = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, skipButton.frame.size.width - 20, skipButton.frame.size.height - 10)];
            skipButtonText.textColor = [UIColor whiteColor];
            skipButtonText.textAlignment = NSTextAlignmentCenter;
            [skipButton addSubview:skipButtonText];
        }
        
        if(ceil(time) < currentAd.vast.skipOffset)
        {
            skipTap.enabled = NO;
            skipButtonText.font = FONT_REFORMA_NARROW(13);
            skipButtonText.text = [NSString stringWithFormat:@"אפשר לדלג על הפרסומת בעוד %.0f", currentAd.vast.skipOffset - time];
            skipButtonText.textColor = [UIColor colorWithWhite:1.0 alpha:0.7];
            skipButtonText.numberOfLines = 2;
        }
        else
        {
            skipTap.enabled = YES;
            skipButtonText.font = FONT_REFORMA_NARROW_MEDIUM(16);
            skipButtonText.text = @"דלג על הפרסומת";
            skipButtonText.textColor = [UIColor colorWithWhite:1.0 alpha:1.0];
            skipButtonText.numberOfLines = 1;
        }
    }
    else {
        skipButton.hidden = YES;
    }
}

-(void)skipClicked:(UITapGestureRecognizer*)gestureRecognizer {
    [currentAd.vast reportSkip];
    if(![self finishAd])
    {
        [avPlayer advanceToNextItem];
    }
}



-(void)moreInfoClicked {
    NSURL *url = [currentAd.vast clickThroughUrl];
    if (url) {
        if ([[UIApplication sharedApplication] canOpenURL: url]) {
            [currentAd.vast reportClick];
            [[UIApplication sharedApplication] openURL: url];
        }
    }
}

-(void)contentComplete
{
    if(self.adRules.postrollPod)
    {
        [self playPod:self.adRules.postrollPod];
    }
    [podTimer invalidate];
    podTimer = nil;
}

-(void)destroy
{
    //    NSLog(@"Destroy adManager called");
    //    NSLog(@"avPlayer: %@", avPlayer);
    //    NSLog(@"podTimer: %@", podTimer);
    //    NSLog(@"view: %@", view);
    //    NSLog(@"superview: %@", view.superview);
    destroyCalled = YES;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [avPlayer removeObserver:self forKeyPath:@"status"];
    [podTimer invalidate];
    podTimer = nil;
    [avPlayer pause];
    avPlayer = nil;
    // [view removeFromSuperview];
}

-(void)pause
{
    if(!playButton)
    {
        playButton = [[UIButton alloc] initWithFrame:CGRectMake(view.frame.size.width / 2 - 50,
                                                                view.frame.size.height / 2 - 50,
                                                                100,
                                                                100)];
    }
    [playButton setBackgroundImage:[UIImage imageNamed:@"btn_play_white"] forState:UIControlStateNormal];
    [view addSubview:playButton];
    [playButton addTarget:self action:@selector(resume) forControlEvents:UIControlEventTouchUpInside];
    [avPlayer pause];
    [currentAd.vast reportPause];
}

-(void)resume
{
    [playButton removeFromSuperview];
    [avPlayer play];
    [currentAd.vast reportResume];
}

-(void)midrollTimerTick:(NSTimer*)timer
{
    if([self.delegate respondsToSelector:@selector(currentTime)])
    {
        id<MFManagerDelegate> delegate = self.delegate;
        NSTimeInterval time = [delegate currentTime];
        MFPod *podToPlayNow;
        for (MFPod *pod in self.adRules.midrollPods) {
            if(pod.state == PodStateWaiting &&
               pod.timeOffset < time)
            {
                pod.state = PodStatePlaying;
                podToPlayNow = pod;
                break;
            }
        }
        if(podToPlayNow)
        {
            [self playPod:podToPlayNow];
        }
    }
}

-(MFAd*)getNextAdInPod
{
    for(int i=0; i<currentPod.ads.count; i++)
    {
        MFAd *ad = currentPod.ads[i];
        if(i > 0)
        {
            MFAd *previousAd = currentPod.ads[i-1];
            if(previousAd.wasLoaded &&
               !ad.wasLoaded &&
               previousAd == currentAd)
            {
                return ad;
            }
        }
        else
        {
            if(!ad.wasLoaded)
            {
                ad.pod = currentPod.podNumberInStream;
                ad.positionInPod = i;
                return ad;
            }
        }
    }
    return nil;
}

-(void)adComplete:(NSNotification*)notification
{
    [self sendEvent:kMFAdEvent_COMPLETE];
    [currentAd.vast reportComplete];
    [self finishAd];
}

-(BOOL)finishAd{
    currentAd = nextLoadedAd;
    if(currentAd)
    {
        currentAd.played = YES;
    }
    if([avPlayer.items indexOfObject:avPlayer.currentItem] >= avPlayer.items.count - 1 || missingAdURL)
    {
        [self finishPod];
        return YES;
    }
    else
    {
        [currentAd.vast reportImpression];
        if(currentAd.isBumper)
        {
            doneButton.hidden = YES;
            moreInfoButton.hidden = YES;
            headerBackgroundView.hidden = YES;
        }
        else
        {
            doneButton.hidden = NO;
            moreInfoButton.hidden = NO;
            headerBackgroundView.hidden = NO;
        }
        return NO;
    }
}


-(void)finishPod
{
    [avPlayer pause];
    if([self.delegate respondsToSelector:@selector(adsManagerDidRequestContentResume:)])
    {
        [self hideAdView];
        [avPlayer removeTimeObserver:avPlayerTimeObserver];
        avPlayer = nil;
        currentAd = nil;
        id<MFManagerDelegate> delegate = self.delegate;
        [delegate adsManagerDidRequestContentResume:self];
        if(currentPod == self.adRules.postrollPod ||
           (currentPod == self.adRules.prerollPod && self.adRules.midrollPods.count == 0 && self.adRules.postrollPod.ads.count == 0) ||
           (currentPod == self.adRules.prerollPod && currentPod.ads.count == 0) ||
           currentPod == nil)
        {
            [self sendEvent:kMFAdEvent_ALL_ADS_COMPLETED];
            [self destroy];
        }
    }
}

-(void)showAdView
{
    doneButton = [parentView viewWithTag:DONE_BUTTON_TAG];
    if(doneButton)
    {
        [parentView insertSubview:view belowSubview:doneButton];
    }
    else
    {
        [parentView addSubview:view];
    }
    
    if(doneButton.superview == parentView)
    {
        [view addSubview:doneButton];
    }
    doneButton.hidden = NO;
}

-(void)hideAdView
{
    doneButton.hidden = YES;
    [view removeFromSuperview];
}

-(void)startOnView:(UIView*)videoView
{
    if(!view)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(adComplete:)
                                                     name:AVPlayerItemDidPlayToEndTimeNotification
                                                   object:nil];
        [[NSNotificationCenter defaultCenter] addObserverForName:AVPlayerItemPlaybackStalledNotification
                                                          object:nil
                                                           queue:[NSOperationQueue mainQueue]
                                                      usingBlock:^(NSNotification *note) {
                                                          [avPlayer play];
                                                      }];
        parentView = videoView;
        view = [[UIView alloc] initWithFrame:parentView.frame];
        view.backgroundColor = [UIColor blackColor];
        podTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                    target:self
                                                  selector:@selector(midrollTimerTick:)
                                                  userInfo:nil
                                                   repeats:YES];
        if(self.adRules.prerollPod &&
           self.adRules.prerollPod.ads.count > 0)
        {
            [self playPod:self.adRules.prerollPod];
        }
        else
        {
            [self finishPod];
        }
    }
}

-(void)sendEvent:(MFAdEventType)type
{
    if(self && [self.delegate respondsToSelector:@selector(adsManager:didReceiveAdEvent:)])
    {
        id<MFManagerDelegate> delegate = self.delegate;
        [delegate adsManager:self didReceiveAdEvent:[[MFEvent alloc] initWithType:type]];
    }
}

-(void)vastFromVASTUrl:(NSString*)VASTURL completion:(VASTParserComplete)completion
{
    [VAST fromUrl:VASTURL
       completion:^(VAST *vast) {
           completion(vast);
       }];
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    if (object == avPlayer && [keyPath isEqualToString:@"status"]) {
        if (avPlayer.currentItem.status == AVPlayerStatusFailed || [MMAdManager getInstance].externalDeviceIsPlayingTheVideo) {
            if([self.delegate respondsToSelector:@selector(adsManager:didReceiveAdError:)])
            {
                [self hideAdView];
                id<MFManagerDelegate> delegate = self.delegate;
                [delegate adsManager:self didReceiveAdError:@{}];
            }
        }
        else
        {
            [self sendEvent:kMFAdEvent_LOADED];
            AVPlayerLayer *layer = [AVPlayerLayer playerLayerWithPlayer:avPlayer];
            avPlayer.actionAtItemEnd = AVPlayerActionAtItemEndAdvance;
            layer.frame = view.frame;
            [self showAdView];
            
            [view.layer addSublayer:layer];
            [self createAndAddMoreInfoButton];
            
            [self createSkipButton];
            [avPlayer play];
            [self sendEvent:kMFAdEvent_STARTED];
            
            
            if([self.delegate respondsToSelector:@selector(adsManagerDidRequestContentPause:)])
            {
                id<MFManagerDelegate> delegate = self.delegate;
                [delegate adsManagerDidRequestContentPause:self];
            }
        }
    }
}

-(void)prepareAdAtNextIndex
{
    MFAd *nextAd = [self getNextAdInPod];
    if(nextAd)
    {
        nextLoadedAd = nextAd;
        nextAd.wasLoaded = YES;
        __weak MFManager *me = self;
        
        [[NSNotificationCenter defaultCenter]
         postNotificationName:kNotificationRequestAd
         object:nextAd.adUrl
         userInfo:nil];
        
        [self vastFromVASTUrl:nextAd.adUrl
                   completion:^(VAST *vast) {
                       
                       nextAd.vast = vast;
                       
                       NSURL *url = [vast playableUrl];
                       if(url)
                       {
                           AVPlayerItem *adPlayerItem = [AVPlayerItem playerItemWithURL:url];
                           self.currentAdVideoUrl = url.absoluteString;
                           if(avPlayer == nil && !destroyCalled)
                           {
                               currentAd = nextAd;
                               if(currentAd.isBumper)
                               {
                                   doneButton.hidden = YES;
                                   moreInfoButton.hidden = YES;
                                   headerBackgroundView.hidden = YES;
                               }
                               else
                               {
                                   if(doneButton.superview == view)
                                   {
                                       doneButton.hidden = NO;
                                   }
                                   moreInfoButton.hidden = NO;
                                   headerBackgroundView.hidden = NO;
                               }
                               
                               nextAd.played = YES;
                               [vast reportImpression];
                               avPlayer = [[AVQueuePlayer alloc] initWithItems:@[adPlayerItem]];
                               __weak AVQueuePlayer *player = avPlayer;
                               
                               [avPlayer setVolume:[MMAdManager getInstance].ad_volume];
                               
                               avPlayerTimeObserver = [avPlayer addPeriodicTimeObserverForInterval:CMTimeMake(1, 10) queue:NULL usingBlock:^(CMTime time) {
                                   CGFloat duration = CMTimeGetSeconds(player.currentItem.duration);
                                   CGFloat timeDouble = CMTimeGetSeconds(time);
                                   //                                   NSLog(@"%f / %f",
                                   //                                         CMTimeGetSeconds(time),
                                   //                                         CMTimeGetSeconds(player.currentItem.duration));
                                   [vast reportStart];
                                   [vast reportTime:timeDouble fromTotalDuration:duration];
                                   [me updateSkipButtonWithTime:timeDouble];
                                   if(duration - timeDouble < 10.0 || (vast.skipOffset > 0 && timeDouble > vast.skipOffset - 2.0))
                                   {
                                       dispatch_async(dispatch_get_main_queue(), ^{
                                           [me prepareAdAtNextIndex];
                                       });
                                   }
                               }];
                               [avPlayer addObserver:self forKeyPath:@"status" options:0 context:nil];
                           }
                           else
                           {
                               if([avPlayer canInsertItem:adPlayerItem afterItem:nil])
                               {
                                   [avPlayer insertItem:adPlayerItem afterItem:nil];
                               }
                           }
                       }
                       else
                       {
                           missingAdURL = YES;
                           if(avPlayer == nil)
                           {
                               [self adComplete:nil];
                           }
                       }
                   }
         ];
    }
}

-(void)playPod:(MFPod*)pod
{
    currentAd = nil;
    currentPod = pod;
    [self prepareAdAtNextIndex];
}

-(NSString *)deviceType
{
    size_t size;
    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    char *machine = malloc(size);
    sysctlbyname("hw.machine", machine, &size, NULL, 0);
    NSString *platform = [NSString stringWithCString:machine encoding:NSUTF8StringEncoding];
    if ([platform isEqualToString:@"iPhone1,1"])    return @"iPhone 1G";
    if ([platform isEqualToString:@"iPhone1,2"])    return @"iPhone 3G";
    if ([platform isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS";
    if ([platform isEqualToString:@"iPhone3,1"])    return @"iPhone 4";
    if ([platform isEqualToString:@"iPhone3,3"])    return @"Verizon iPhone 4";
    if ([platform isEqualToString:@"iPhone4,1"])    return @"iPhone 4S";
    if ([platform isEqualToString:@"iPhone5,1"])    return @"iPhone 5 (GSM)";
    if ([platform isEqualToString:@"iPhone5,2"])    return @"iPhone 5 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone5,3"])    return @"iPhone 5c (GSM)";
    if ([platform isEqualToString:@"iPhone5,4"])    return @"iPhone 5c (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone6,1"])    return @"iPhone 5s (GSM)";
    if ([platform isEqualToString:@"iPhone6,2"])    return @"iPhone 5s (GSM+CDMA)";
    if ([platform isEqualToString:@"iPhone7,2"])    return @"iPhone 6";
    if ([platform isEqualToString:@"iPhone7,1"])    return @"iPhone 6 Plus";
    if ([platform isEqualToString:@"iPhone8,1"])    return @"iPhone 6s";
    if ([platform isEqualToString:@"iPhone8,2"])    return @"iPhone 6s Plus";
    if ([platform isEqualToString:@"iPod1,1"])      return @"iPod Touch 1G";
    if ([platform isEqualToString:@"iPod2,1"])      return @"iPod Touch 2G";
    if ([platform isEqualToString:@"iPod3,1"])      return @"iPod Touch 3G";
    if ([platform isEqualToString:@"iPod4,1"])      return @"iPod Touch 4G";
    if ([platform isEqualToString:@"iPod5,1"])      return @"iPod Touch 5G";
    if ([platform isEqualToString:@"iPad1,1"])      return @"iPad";
    if ([platform isEqualToString:@"iPad2,1"])      return @"iPad 2 (WiFi)";
    if ([platform isEqualToString:@"iPad2,2"])      return @"iPad 2 (GSM)";
    if ([platform isEqualToString:@"iPad2,3"])      return @"iPad 2 (CDMA)";
    if ([platform isEqualToString:@"iPad2,4"])      return @"iPad 2 (WiFi)";
    if ([platform isEqualToString:@"iPad2,5"])      return @"iPad Mini (WiFi)";
    if ([platform isEqualToString:@"iPad2,6"])      return @"iPad Mini (GSM)";
    if ([platform isEqualToString:@"iPad2,7"])      return @"iPad Mini (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad3,1"])      return @"iPad 3 (WiFi)";
    if ([platform isEqualToString:@"iPad3,2"])      return @"iPad 3 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad3,3"])      return @"iPad 3 (GSM)";
    if ([platform isEqualToString:@"iPad3,4"])      return @"iPad 4 (WiFi)";
    if ([platform isEqualToString:@"iPad3,5"])      return @"iPad 4 (GSM)";
    if ([platform isEqualToString:@"iPad3,6"])      return @"iPad 4 (GSM+CDMA)";
    if ([platform isEqualToString:@"iPad4,1"])      return @"iPad Air (WiFi)";
    if ([platform isEqualToString:@"iPad4,2"])      return @"iPad Air (Cellular)";
    if ([platform isEqualToString:@"iPad4,3"])      return @"iPad Air";
    if ([platform isEqualToString:@"iPad4,4"])      return @"iPad Mini 2G (WiFi)";
    if ([platform isEqualToString:@"iPad4,5"])      return @"iPad Mini 2G (Cellular)";
    if ([platform isEqualToString:@"iPad4,6"])      return @"iPad Mini 2G";
    if ([platform isEqualToString:@"iPad4,7"])      return @"iPad Mini 3 (WiFi)";
    if ([platform isEqualToString:@"iPad4,8"])      return @"iPad Mini 3 (Cellular)";
    if ([platform isEqualToString:@"iPad4,9"])      return @"iPad Mini 3 (China)";
    if ([platform isEqualToString:@"iPad5,3"])      return @"iPad Air 2 (WiFi)";
    if ([platform isEqualToString:@"iPad5,4"])      return @"iPad Air 2 (Cellular)";
    if ([platform isEqualToString:@"AppleTV2,1"])   return @"Apple TV 2G";
    if ([platform isEqualToString:@"AppleTV3,1"])   return @"Apple TV 3";
    if ([platform isEqualToString:@"AppleTV3,2"])   return @"Apple TV 3 (2013)";
    if ([platform isEqualToString:@"i386"])         return @"Simulator";
    if ([platform isEqualToString:@"x86_64"])       return @"Simulator";
    return platform;
}

@end
