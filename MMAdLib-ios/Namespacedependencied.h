//
//  Namespacedependencied.h
//  MMAdLib-ios
//
//  Created by Ran Greenberg on 9/18/14.
//  Copyright (c) 2014 mako. All rights reserved.
//

// Namespaced Header

#ifndef __NS_SYMBOL
// We need to have multiple levels of macros here so that __NAMESPACE_PREFIX_ is
// properly replaced by the time we concatenate the namespace prefix.
#define __NS_REWRITE(ns, symbol) ns ## _ ## symbol
#define __NS_BRIDGE(ns, symbol) __NS_REWRITE(ns, symbol)
#define __NS_SYMBOL(symbol) __NS_BRIDGE(MAKO, symbol)
#endif


// Classes
#ifndef ACTStub
#define ACTStub __NS_SYMBOL(ACTStub)
#endif

#ifndef AFHTTPBodyPart
#define AFHTTPBodyPart __NS_SYMBOL(AFHTTPBodyPart)
#endif

#ifndef AFHTTPClient
#define AFHTTPClient __NS_SYMBOL(AFHTTPClient)
#endif

#ifndef AFHTTPRequestOperation
#define AFHTTPRequestOperation __NS_SYMBOL(AFHTTPRequestOperation)
#endif

#ifndef AFImageCache
#define AFImageCache __NS_SYMBOL(AFImageCache)
#endif

#ifndef AFImageRequestOperation
#define AFImageRequestOperation __NS_SYMBOL(AFImageRequestOperation)
#endif

#ifndef AFJSONRequestOperation
#define AFJSONRequestOperation __NS_SYMBOL(AFJSONRequestOperation)
#endif

#ifndef AFMultipartBodyStream
#define AFMultipartBodyStream __NS_SYMBOL(AFMultipartBodyStream)
#endif

#ifndef AFNetworkActivityIndicatorManager
#define AFNetworkActivityIndicatorManager __NS_SYMBOL(AFNetworkActivityIndicatorManager)
#endif

#ifndef AFPropertyListRequestOperation
#define AFPropertyListRequestOperation __NS_SYMBOL(AFPropertyListRequestOperation)
#endif

#ifndef AFQueryStringPair
#define AFQueryStringPair __NS_SYMBOL(AFQueryStringPair)
#endif

#ifndef AFStreamingMultipartFormData
#define AFStreamingMultipartFormData __NS_SYMBOL(AFStreamingMultipartFormData)
#endif

#ifndef AFURLConnectionOperation
#define AFURLConnectionOperation __NS_SYMBOL(AFURLConnectionOperation)
#endif

#ifndef AFXMLRequestOperation
#define AFXMLRequestOperation __NS_SYMBOL(AFXMLRequestOperation)
#endif

#ifndef ASIdentifierManager
#define ASIdentifierManager __NS_SYMBOL(ASIdentifierManager)
#endif

#ifndef AVAsset
#define AVAsset __NS_SYMBOL(AVAsset)
#endif

#ifndef AVAssetImageGenerator
#define AVAssetImageGenerator __NS_SYMBOL(AVAssetImageGenerator)
#endif

#ifndef AVAudioSession
#define AVAudioSession __NS_SYMBOL(AVAudioSession)
#endif

#ifndef AVPlayer
#define AVPlayer __NS_SYMBOL(AVPlayer)
#endif

#ifndef AVPlayerItem
#define AVPlayerItem __NS_SYMBOL(AVPlayerItem)
#endif

#ifndef AVPlayerLayer
#define AVPlayerLayer __NS_SYMBOL(AVPlayerLayer)
#endif

#ifndef CAShapeLayer
#define CAShapeLayer __NS_SYMBOL(CAShapeLayer)
#endif

#ifndef CXHTMLDocument
#define CXHTMLDocument __NS_SYMBOL(CXHTMLDocument)
#endif

#ifndef CXMLDocument
#define CXMLDocument __NS_SYMBOL(CXMLDocument)
#endif

#ifndef CXMLElement
#define CXMLElement __NS_SYMBOL(CXMLElement)
#endif

#ifndef CXMLNamespaceNode
#define CXMLNamespaceNode __NS_SYMBOL(CXMLNamespaceNode)
#endif

#ifndef CXMLNode
#define CXMLNode __NS_SYMBOL(CXMLNode)
#endif

#ifndef MLQoEMeasurement
#define MLQoEMeasurement __NS_SYMBOL(MLQoEMeasurement)
#endif

#ifndef OCClassMockObject
#define OCClassMockObject __NS_SYMBOL(OCClassMockObject)
#endif

#ifndef OCMAnyConstraint
#define OCMAnyConstraint __NS_SYMBOL(OCMAnyConstraint)
#endif

#ifndef OCMArg
#define OCMArg __NS_SYMBOL(OCMArg)
#endif

#ifndef OCMBlockCaller
#define OCMBlockCaller __NS_SYMBOL(OCMBlockCaller)
#endif

#ifndef OCMBlockConstraint
#define OCMBlockConstraint __NS_SYMBOL(OCMBlockConstraint)
#endif

#ifndef OCMBoxedReturnValueProvider
#define OCMBoxedReturnValueProvider __NS_SYMBOL(OCMBoxedReturnValueProvider)
#endif

#ifndef OCMConstraint
#define OCMConstraint __NS_SYMBOL(OCMConstraint)
#endif

#ifndef OCMExceptionReturnValueProvider
#define OCMExceptionReturnValueProvider __NS_SYMBOL(OCMExceptionReturnValueProvider)
#endif

#ifndef OCMIndirectReturnValueProvider
#define OCMIndirectReturnValueProvider __NS_SYMBOL(OCMIndirectReturnValueProvider)
#endif

#ifndef OCMInvocationConstraint
#define OCMInvocationConstraint __NS_SYMBOL(OCMInvocationConstraint)
#endif

#ifndef OCMIsNilConstraint
#define OCMIsNilConstraint __NS_SYMBOL(OCMIsNilConstraint)
#endif

#ifndef OCMIsNotEqualConstraint
#define OCMIsNotEqualConstraint __NS_SYMBOL(OCMIsNotEqualConstraint)
#endif

#ifndef OCMIsNotNilConstraint
#define OCMIsNotNilConstraint __NS_SYMBOL(OCMIsNotNilConstraint)
#endif

#ifndef OCMNotificationPoster
#define OCMNotificationPoster __NS_SYMBOL(OCMNotificationPoster)
#endif

#ifndef OCMObserverRecorder
#define OCMObserverRecorder __NS_SYMBOL(OCMObserverRecorder)
#endif

#ifndef OCMPassByRefSetter
#define OCMPassByRefSetter __NS_SYMBOL(OCMPassByRefSetter)
#endif

#ifndef OCMRealObjectForwarder
#define OCMRealObjectForwarder __NS_SYMBOL(OCMRealObjectForwarder)
#endif

#ifndef OCMReturnValueProvider
#define OCMReturnValueProvider __NS_SYMBOL(OCMReturnValueProvider)
#endif

#ifndef OCMockObject
#define OCMockObject __NS_SYMBOL(OCMockObject)
#endif

#ifndef OCMockRecorder
#define OCMockRecorder __NS_SYMBOL(OCMockRecorder)
#endif

#ifndef OCObserverMockObject
#define OCObserverMockObject __NS_SYMBOL(OCObserverMockObject)
#endif

#ifndef OCPartialMockObject
#define OCPartialMockObject __NS_SYMBOL(OCPartialMockObject)
#endif

#ifndef OCPartialMockRecorder
#define OCPartialMockRecorder __NS_SYMBOL(OCPartialMockRecorder)
#endif

#ifndef OCProtocolMockObject
#define OCProtocolMockObject __NS_SYMBOL(OCProtocolMockObject)
#endif

#ifndef SKMutablePayment
#define SKMutablePayment __NS_SYMBOL(SKMutablePayment)
#endif

#ifndef SKPaymentQueue
#define SKPaymentQueue __NS_SYMBOL(SKPaymentQueue)
#endif

#ifndef SKProductsRequest
#define SKProductsRequest __NS_SYMBOL(SKProductsRequest)
#endif

#ifndef SKStoreProductViewController
#define SKStoreProductViewController __NS_SYMBOL(SKStoreProductViewController)
#endif

#ifndef VideoAdEntry
#define VideoAdEntry __NS_SYMBOL(VideoAdEntry)
#endif

// Functions
#ifndef AFContentTypesFromHTTPHeader
#define AFContentTypesFromHTTPHeader __NS_SYMBOL(AFContentTypesFromHTTPHeader)
#endif

#ifndef AdLog
#define AdLog __NS_SYMBOL(AdLog)
#endif

#ifndef GADAdSupportedOrientationsFromString
#define GADAdSupportedOrientationsFromString __NS_SYMBOL(GADAdSupportedOrientationsFromString)
#endif

#ifndef GADDelegateManagerDidYouNilOutYourDelegate
#define GADDelegateManagerDidYouNilOutYourDelegate __NS_SYMBOL(GADDelegateManagerDidYouNilOutYourDelegate)
#endif

#ifndef GADSessionGenerateSessionID
#define GADSessionGenerateSessionID __NS_SYMBOL(GADSessionGenerateSessionID)
#endif

#ifndef GADStaticLibraryCategoriesAreLoaded
#define GADStaticLibraryCategoriesAreLoaded __NS_SYMBOL(GADStaticLibraryCategoriesAreLoaded)
#endif

#ifndef GADiTunesMetadataForFileAtPath
#define GADiTunesMetadataForFileAtPath __NS_SYMBOL(GADiTunesMetadataForFileAtPath)
#endif

#ifndef audioRouteChangeListenerCallback
#define audioRouteChangeListenerCallback __NS_SYMBOL(audioRouteChangeListenerCallback)
#endif

#ifndef gad_gig
#define gad_gig __NS_SYMBOL(gad_gig)
#endif

#ifndef gad_gmrs
#define gad_gmrs __NS_SYMBOL(gad_gmrs)
#endif

#ifndef GADStringArrayToURLArray
#define GADStringArrayToURLArray __NS_SYMBOL(GADStringArrayToURLArray)
#endif

#ifndef gad_gmcs
#define gad_gmcs __NS_SYMBOL(gad_gmcs)
#endif

#ifndef CustomGADAdSizeForCGSize
#define CustomGADAdSizeForCGSize __NS_SYMBOL(CustomGADAdSizeForCGSize)
#endif

#ifndef gad_htg
#define gad_htg __NS_SYMBOL(gad_htg)
#endif

#ifndef GADAdSizeFullWidthPortraitWithHeight
#define GADAdSizeFullWidthPortraitWithHeight __NS_SYMBOL(GADAdSizeFullWidthPortraitWithHeight)
#endif

#ifndef GADAdSizeFullWidthLandscapeWithHeight
#define GADAdSizeFullWidthLandscapeWithHeight __NS_SYMBOL(GADAdSizeFullWidthLandscapeWithHeight)
#endif

#ifndef GADAdSizeEqualToSize
#define GADAdSizeEqualToSize __NS_SYMBOL(GADAdSizeEqualToSize)
#endif

#ifndef CGSizeFromGADAdSize
#define CGSizeFromGADAdSize __NS_SYMBOL(CGSizeFromGADAdSize)
#endif

#ifndef GADAdSizeIsFullWidth
#define GADAdSizeIsFullWidth __NS_SYMBOL(GADAdSizeIsFullWidth)
#endif

#ifndef CGSizeFromGADAdSizeSmartBanner
#define CGSizeFromGADAdSizeSmartBanner __NS_SYMBOL(CGSizeFromGADAdSizeSmartBanner)
#endif

#ifndef GADAdSizeIsAutoHeight
#define GADAdSizeIsAutoHeight __NS_SYMBOL(GADAdSizeIsAutoHeight)
#endif

#ifndef GADAdSizeIsCustom
#define GADAdSizeIsCustom __NS_SYMBOL(GADAdSizeIsCustom)
#endif

#ifndef IsGADAdSizeValid
#define IsGADAdSizeValid __NS_SYMBOL(IsGADAdSizeValid)
#endif

#ifndef GADAdSizeIsStandard
#define GADAdSizeIsStandard __NS_SYMBOL(GADAdSizeIsStandard)
#endif

#ifndef NSStringFromGADAdSize
#define NSStringFromGADAdSize __NS_SYMBOL(NSStringFromGADAdSize)
#endif

#ifndef AFQueryStringFromParametersWithEncoding
#define AFQueryStringFromParametersWithEncoding __NS_SYMBOL(AFQueryStringFromParametersWithEncoding)
#endif

#ifndef AFQueryStringPairsFromDictionary
#define AFQueryStringPairsFromDictionary __NS_SYMBOL(AFQueryStringPairsFromDictionary)
#endif

#ifndef AFQueryStringPairsFromKeyAndValue
#define AFQueryStringPairsFromKeyAndValue __NS_SYMBOL(AFQueryStringPairsFromKeyAndValue)
#endif

#ifndef GADAdSizeIsStandardFixed
#define GADAdSizeIsStandardFixed __NS_SYMBOL(GADAdSizeIsStandardFixed)
#endif

#ifndef GADCGSizeFromString
#define GADCGSizeFromString __NS_SYMBOL(GADCGSizeFromString)
#endif

// Externs
#ifndef GAD_SBJSONErrorDomain
#define GAD_SBJSONErrorDomain __NS_SYMBOL(GAD_SBJSONErrorDomain)
#endif

#ifndef IMA_SBJSONErrorDomain
#define IMA_SBJSONErrorDomain __NS_SYMBOL(IMA_SBJSONErrorDomain)
#endif

#ifndef OCMRealMethodAliasPrefix
#define OCMRealMethodAliasPrefix __NS_SYMBOL(OCMRealMethodAliasPrefix)
#endif

#ifndef AdapterHtmlVersionString
#define AdapterHtmlVersionString __NS_SYMBOL(AdapterHtmlVersionString)
#endif

#ifndef GoogleAdMobAdsVersionString
#define GoogleAdMobAdsVersionString __NS_SYMBOL(GoogleAdMobAdsVersionString)
#endif

#ifndef GoogleIMA3ForAdMobVersionString
#define GoogleIMA3ForAdMobVersionString __NS_SYMBOL(GoogleIMA3ForAdMobVersionString)
#endif

#ifndef GoogleIMA3VersionString
#define GoogleIMA3VersionString __NS_SYMBOL(GoogleIMA3VersionString)
#endif

#ifndef kGADSDKMajorVersion
#define kGADSDKMajorVersion __NS_SYMBOL(kGADSDKMajorVersion)
#endif

#ifndef kGADSDKMinorVersion
#define kGADSDKMinorVersion __NS_SYMBOL(kGADSDKMinorVersion)
#endif

#ifndef kGADSDKPatchVersion
#define kGADSDKPatchVersion __NS_SYMBOL(kGADSDKPatchVersion)
#endif

#ifndef AdapterHtmlVersionNumber
#define AdapterHtmlVersionNumber __NS_SYMBOL(AdapterHtmlVersionNumber)
#endif

#ifndef GoogleAdMobAdsVersionNumber
#define GoogleAdMobAdsVersionNumber __NS_SYMBOL(GoogleAdMobAdsVersionNumber)
#endif

#ifndef GoogleIMA3VersionNumber
#define GoogleIMA3VersionNumber __NS_SYMBOL(GoogleIMA3VersionNumber)
#endif

#ifndef GoogleIMA3ForAdMobVersionNumber
#define GoogleIMA3ForAdMobVersionNumber __NS_SYMBOL(GoogleIMA3ForAdMobVersionNumber)
#endif

#ifndef kIMAAutodetectBitrate
#define kIMAAutodetectBitrate __NS_SYMBOL(kIMAAutodetectBitrate)
#endif

#ifndef kGADErrorDomain
#define kGADErrorDomain __NS_SYMBOL(kGADErrorDomain)
#endif

#ifndef kIMAHtml5SdkRequestLoadingTimeout
#define kIMAHtml5SdkRequestLoadingTimeout __NS_SYMBOL(kIMAHtml5SdkRequestLoadingTimeout)
#endif

#ifndef kIMAHtml5SdkTotalLoadingTimeout
#define kIMAHtml5SdkTotalLoadingTimeout __NS_SYMBOL(kIMAHtml5SdkTotalLoadingTimeout)
#endif

#ifndef kGADAdSizeInvalid
#define kGADAdSizeInvalid __NS_SYMBOL(kGADAdSizeInvalid)
#endif

#ifndef kGADAdSizeBanner
#define kGADAdSizeBanner __NS_SYMBOL(kGADAdSizeBanner)
#endif

#ifndef kGADAdSizeLargeBanner
#define kGADAdSizeLargeBanner __NS_SYMBOL(kGADAdSizeLargeBanner)
#endif

#ifndef kGADAdSizeMediumRectangle
#define kGADAdSizeMediumRectangle __NS_SYMBOL(kGADAdSizeMediumRectangle)
#endif

#ifndef kGADAdSizeFullBanner
#define kGADAdSizeFullBanner __NS_SYMBOL(kGADAdSizeFullBanner)
#endif

#ifndef kGADAdSizeLeaderboard
#define kGADAdSizeLeaderboard __NS_SYMBOL(kGADAdSizeLeaderboard)
#endif

#ifndef kGADAdSizeSkyscraper
#define kGADAdSizeSkyscraper __NS_SYMBOL(kGADAdSizeSkyscraper)
#endif

#ifndef kGADAdSizeSmartBannerPortrait
#define kGADAdSizeSmartBannerPortrait __NS_SYMBOL(kGADAdSizeSmartBannerPortrait)
#endif

#ifndef kGADAdSizeSmartBannerLandscape
#define kGADAdSizeSmartBannerLandscape __NS_SYMBOL(kGADAdSizeSmartBannerLandscape)
#endif

#ifndef kGADAdSizeSmartBannerSkinnyPortrait
#define kGADAdSizeSmartBannerSkinnyPortrait __NS_SYMBOL(kGADAdSizeSmartBannerSkinnyPortrait)
#endif

#ifndef kGADAdSizeSmartBannerSkinnyLandscape
#define kGADAdSizeSmartBannerSkinnyLandscape __NS_SYMBOL(kGADAdSizeSmartBannerSkinnyLandscape)
#endif

#ifndef kGADApplicationDiscardableStoreKitControllerAppCacheMaxObjectCount
#define kGADApplicationDiscardableStoreKitControllerAppCacheMaxObjectCount __NS_SYMBOL(kGADApplicationDiscardableStoreKitControllerAppCacheMaxObjectCount)
#endif

#ifndef kGADAdKeyPassbackURL
#define kGADAdKeyPassbackURL __NS_SYMBOL(kGADAdKeyPassbackURL)
#endif

#ifndef kGADAdKeyIsJavascript
#define kGADAdKeyIsJavascript __NS_SYMBOL(kGADAdKeyIsJavascript)
#endif

#ifndef AFNetworkingErrorDomain
#define AFNetworkingErrorDomain __NS_SYMBOL(AFNetworkingErrorDomain)
#endif

#ifndef AFNetworkingOperationFailingURLRequestErrorKey
#define AFNetworkingOperationFailingURLRequestErrorKey __NS_SYMBOL(AFNetworkingOperationFailingURLRequestErrorKey)
#endif

#ifndef AFNetworkingOperationFailingURLResponseErrorKey
#define AFNetworkingOperationFailingURLResponseErrorKey __NS_SYMBOL(AFNetworkingOperationFailingURLResponseErrorKey)
#endif

#ifndef AFNetworkingOperationDidStartNotification
#define AFNetworkingOperationDidStartNotification __NS_SYMBOL(AFNetworkingOperationDidStartNotification)
#endif

#ifndef AFNetworkingOperationDidFinishNotification
#define AFNetworkingOperationDidFinishNotification __NS_SYMBOL(AFNetworkingOperationDidFinishNotification)
#endif

#ifndef AFNetworkingReachabilityDidChangeNotification
#define AFNetworkingReachabilityDidChangeNotification __NS_SYMBOL(AFNetworkingReachabilityDidChangeNotification)
#endif

#ifndef AFNetworkingReachabilityNotificationStatusItem
#define AFNetworkingReachabilityNotificationStatusItem __NS_SYMBOL(AFNetworkingReachabilityNotificationStatusItem)
#endif

#ifndef kAFUploadStream3GSuggestedPacketSize
#define kAFUploadStream3GSuggestedPacketSize __NS_SYMBOL(kAFUploadStream3GSuggestedPacketSize)
#endif

#ifndef kAFUploadStream3GSuggestedDelay
#define kAFUploadStream3GSuggestedDelay __NS_SYMBOL(kAFUploadStream3GSuggestedDelay)
#endif

