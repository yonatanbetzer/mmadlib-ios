//
//  MFAdRules.h
//  MMAdLib-ios
//
//  Created by Yonatan Betzer on 10/23/14.
//  Copyright (c) 2014 mako. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MFPod.h"

@interface MFAdRules : NSObject

@property (nonatomic, strong) MFPod *prerollPod;
@property (nonatomic, strong) NSArray *midrollPods;
@property (nonatomic, strong) MFPod *postrollPod;

+(MFAdRules*)fromDictionary:(NSDictionary*)adrulesDictionary;
@end
