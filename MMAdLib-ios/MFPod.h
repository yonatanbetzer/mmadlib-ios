//
//  MFPod.h
//  MMAdLib-ios
//
//  Created by Yonatan Betzer on 10/23/14.
//  Copyright (c) 2014 mako. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, PodState)
{
    PodStateWaiting = 0,
    PodStatePlaying,
    PodStateComplete
};

@interface MFPod : NSObject

@property NSTimeInterval timeOffset;
@property int numberOfAdsExcludingBumpers;
@property int numberOfBumpers;
@property int podNumberInStream;
@property PodState state;
@property (nonatomic, strong) NSMutableArray *ads;

+(MFPod*)fromDictionary:(NSDictionary*)podDictionary;
+(NSArray*)fromArray:(NSArray*)podDictionaries;
@end
