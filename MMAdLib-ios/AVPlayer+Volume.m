//
//  AVPlayer+Volume.m
//  mako_mobile_ipad
//
//  Created by Yehonatan Betzer on 11/10/2015.
//  Copyright © 2015 mako. All rights reserved.
//

#import "AVPlayer+Volume.h"

@implementation AVPlayer (Volume)

- (NSURL *)currentURL {
    AVAsset *asset = self.currentItem.asset;
    if ([asset isMemberOfClass:[AVURLAsset class]])
        return ((AVURLAsset *)asset).URL;
    return nil;
}

- (void)setVolume:(CGFloat)volume {
    NSArray *audioTracks = [self.currentItem.asset tracksWithMediaType:AVMediaTypeAudio];
    NSMutableArray *allAudioParams = [NSMutableArray array];
    for (AVAssetTrack *track in audioTracks) {

        AVMutableAudioMixInputParameters *audioInputParams = [AVMutableAudioMixInputParameters audioMixInputParameters];
        [audioInputParams setVolume:volume atTime:kCMTimeZero];
        [audioInputParams setTrackID:[track trackID]];
        [allAudioParams addObject:audioInputParams];
    }
    AVMutableAudioMix *audioMix = [AVMutableAudioMix audioMix];
    [audioMix setInputParameters:allAudioParams];
    [self.currentItem setAudioMix:audioMix];
}

@end
