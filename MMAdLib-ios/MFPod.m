//
//  MFPod.m
//  MMAdLib-ios
//
//  Created by Yonatan Betzer on 10/23/14.
//  Copyright (c) 2014 mako. All rights reserved.
//

#import "MFPod.h"
#import "MFAd.h"
@implementation MFPod

+(MFPod*)fromDictionary:(NSDictionary*)podDictionary
{
    MFPod *result = [[MFPod alloc] init];
    result.state = PodStateWaiting;
    
    NSArray *rawAds = podDictionary[@"Ad"];
    if([rawAds isKindOfClass:[NSString class]])
    {
        NSMutableDictionary *dic = [NSMutableDictionary dictionary];
        [dic setObject:rawAds forKey:@"__text"];
        rawAds = @[dic];
    }
    if([rawAds isKindOfClass:[NSDictionary class]])
    {
        rawAds = @[rawAds];
    }
    result.ads = [NSMutableArray array];
    for (NSObject *rawAd in rawAds) {
        MFAd *ad = [MFAd fromObject:rawAd];
        if(ad)
        {
            if(ad.isBumper)
            {
                result.numberOfBumpers++;
            }
            else
            {
                result.numberOfAdsExcludingBumpers++;
            }
            [result.ads addObject:ad];
        }
    }
    
    if(podDictionary[@"_timeOffset"] &&
       [podDictionary[@"_timeOffset"] isKindOfClass:[NSString class]])
    {
        NSArray *timeParts =[podDictionary[@"_timeOffset"] componentsSeparatedByString:@":"];
        if(timeParts.count == 3)
        {
            NSInteger hours = [timeParts[0] integerValue];
            NSInteger minutes = [timeParts[1] integerValue];
            NSInteger seconds = [timeParts[2] integerValue];
            result.timeOffset = (hours * 60 * 60) + (minutes * 60) + seconds;
        }
    }
    return result;
}

+(NSArray*)fromArray:(NSArray*)podDictionaries
{
    if([podDictionaries isKindOfClass:[NSDictionary class]])
    {
        podDictionaries = @[podDictionaries];
    }
    NSMutableArray *result = [[NSMutableArray alloc] init];
    int index = 0;
    for (NSDictionary *podDic in podDictionaries) {
        MFPod *item = [MFPod fromDictionary:podDic];
        item.podNumberInStream = index;
        [result addObject:item];
    }
    return result;
}

@end
