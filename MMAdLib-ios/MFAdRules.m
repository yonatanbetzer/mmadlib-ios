//
//  MFAdRules.m
//  MMAdLib-ios
//
//  Created by Yonatan Betzer on 10/23/14.
//  Copyright (c) 2014 mako. All rights reserved.
//

#import "MFAdRules.h"

@implementation MFAdRules

+(MFAdRules*)fromDictionary:(NSDictionary*)adrulesDictionary
{
    MFAdRules *rules = [[MFAdRules alloc] init];
//    NSLog(@"%@", adrulesDictionary);
    rules.prerollPod = [MFPod fromDictionary:adrulesDictionary[@"Preroll"]];
    rules.postrollPod = [MFPod fromDictionary:adrulesDictionary[@"Postroll"]];
    rules.midrollPods = [MFPod fromArray:adrulesDictionary[@"Midroll"]];
    return rules;
}
@end
