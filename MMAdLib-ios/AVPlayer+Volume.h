//
//  AVPlayer+Volume.h
//  mako_mobile_ipad
//
//  Created by Yehonatan Betzer on 11/10/2015.
//  Copyright © 2015 mako. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>

@interface AVPlayer (Volume)

- (void)setVolume:(CGFloat)volume;

@end
