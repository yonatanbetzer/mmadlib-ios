//
//  MFLoader.h
//  MMAdLib-ios
//
//  Created by Yonatan Betzer on 10/23/14.
//  Copyright (c) 2014 mako. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MFSettings.h"
@class MFManager;
@class MFLoader;

@protocol MFLoaderDelegate <NSObject>

- (void)adsLoader:(MFLoader *)loader failedWithErrorData:(NSDictionary *)adErrorData;
- (void)adsLoaderLoaded:(MFLoader *)loader withManager:(MFManager *)adsManager;

@end

@interface MFLoader : NSObject

@property id /*<MFLoaderDelegate>*/ delegate;
@property BOOL adLoaderAborted;

-(id)initWithSettings:(MFSettings*)settings;
-(void)requestAdsAdRulesUrl:(NSString*)url;

@end
