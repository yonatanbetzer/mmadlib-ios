//
//  MFEvent.h
//  MMAdLib-ios
//
//  Created by Yonatan Betzer on 10/23/14.
//  Copyright (c) 2014 mako. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    /// All ads managed by the ads manager have completed.
    kMFAdEvent_ALL_ADS_COMPLETED,
    /// Ad clicked.
    kMFAdEvent_CLICKED,
    /// Single ad has finished.
    kMFAdEvent_COMPLETE,
    /// First quartile of a linear ad was reached.
    kMFAdEvent_FIRST_QUARTILE,
    /// An ad was loaded.
    kMFAdEvent_LOADED,
    /// Midpoint of a linear ad was reached.
    kMFAdEvent_MIDPOINT,
    /// Ad paused.
    kMFAdEvent_PAUSE,
    /// Ad resumed.
    kMFAdEvent_RESUME,
    /// Third quartile of a linear ad was reached.
    kMFAdEvent_THIRD_QUARTILE,
    // Ad has started.
    kMFAdEvent_STARTED
} MFAdEventType;

@interface MFEvent : NSObject

@property (nonatomic) MFAdEventType type;

-(id)initWithType:(MFAdEventType)type;

@end
