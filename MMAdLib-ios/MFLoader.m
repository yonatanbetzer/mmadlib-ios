//
//  MFLoader.m
//  MMAdLib-ios
//
//  Created by Yonatan Betzer on 10/23/14.
//  Copyright (c) 2014 mako. All rights reserved.
//

#import "MFLoader.h"
#import "XMLDictionary.h"
#import "MFAdRules.h"
#import "MFManager.h"

@implementation MFLoader
{
    NSString *language;
}

-(id)initWithSettings:(MFSettings*)settings
{
    self = [super init];
    if(self)
    {
        language = settings.language;
    }
    return self;
}

-(void)requestAdsAdRulesUrl:(NSString*)urlStr
{
    /*
     Call delegate methods:
     
     - (void)adsLoader:(MFLoader *)loader failedWithErrorData:(NSDictionary *)adErrorData;
     - (void)adsLoaderLoaded:(MFLoader *)loader withManager:(MFManager *)adsManager;
     
     */
    urlStr = [urlStr stringByReplacingOccurrencesOfString:@"[timestamp]" withString:[NSString stringWithFormat:@"%.0f", [[NSDate date] timeIntervalSince1970]]];
    NSURL *url = [ NSURL URLWithString:urlStr];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [AFXMLRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/xml"]];
    
    
    AFXMLRequestOperation *operation = [AFXMLRequestOperation XMLParserRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, NSXMLParser *XMLParser) {
        NSDictionary *dictionary = [NSDictionary dictionaryWithXMLParser:XMLParser];
        MFAdRules *adRules = [MFAdRules fromDictionary:dictionary];
        MFManager *manager = [[MFManager alloc] initWithAdRules:adRules];
        if([self.delegate respondsToSelector:@selector(adsLoaderLoaded:withManager:)] && !self.adLoaderAborted)
        {
            id<MFLoaderDelegate> delegate = self.delegate;
            [delegate adsLoaderLoaded:self withManager:manager];
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, NSXMLParser *XMLParser) {
        if([self.delegate respondsToSelector:@selector(adsLoader:failedWithErrorData:)])
        {
            id<MFLoaderDelegate> delegate = self.delegate;
            [delegate adsLoader:self failedWithErrorData:error.userInfo];
        }
    }];
    [operation start];
}
@end
