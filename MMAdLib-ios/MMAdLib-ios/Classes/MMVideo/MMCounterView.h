//
//  MMCounterView.h
//  MMAd
//
//  Created by Nir Etzion on 3/5/14.
//  Copyright (c) 2014 Mako. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MMCounterView : UIView

@property (nonatomic, strong) NSString *text;

@end
