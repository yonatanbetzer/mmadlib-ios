//
//  MPMoviePlayerController+MakoAdvertising.h
//  IMATest
//
//  Created by Ran Greenberg on 8/5/14.
//  Copyright (c) 2014 mako. All rights reserved.
//

#import <MediaPlayer/MediaPlayer.h>

#define VideoAd_Action                          @"videoAdAction"
#define VideoAd_Action_LoadAd                   @"videoAdActionLoadAd"
#define VideoAd_Action_StartShowAd              @"videoAdActionStartShowAd"
#define VideoAd_Action_CurrentAdCompleted       @"videoAdCurrentComplted"
#define VideoAd_Action_CurrentAdPaused          @"videoAdCurrentPaused"
#define VideoAd_Action_CurrentAdResumed         @"videoAdCurrentResumed"
#define VideoAd_Action_FinishShowAllAds         @"VideoAd_Action_FinishShowAllAds"
#define VideoAd_Action_WillShowAd               @"videoAdActionWillShowAd"
#define VideoAd_Action_FinishShowCurrentAdPod   @"videoAdActionFinishShowCurrentAdPod"
#define VideoAd_ErrorLoad                       @"videoAdErrorLoad"
#define VideoAd_DonePressed                     @"videoAdDonePressed"


@interface MPMoviePlayerController (MakoAdvertising)

@property (nonatomic) BOOL isAllVideoAdsCompleted;

-(void)playWithMakoCatDFP:(NSDictionary*)makoCatDFPDict andVidId:(NSString *)vidId;

-(void)destroyAd;

@end
