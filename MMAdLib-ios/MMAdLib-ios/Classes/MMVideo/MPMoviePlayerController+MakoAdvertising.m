//
//  MPMoviePlayerController+MakoAdvertising.m
//  IMATest
//
//  Created by Ran Greenberg on 8/5/14.
//  Copyright (c) 2014 mako. All rights reserved.
//

#import "MPMoviePlayerController+MakoAdvertising.h"
#import <objc/runtime.h>
#import "MFLoader.h"
#import "MFManager.h"
#import "MMAdManager.h"
#import "MFSettings.h"
#import "MFEvent.h"
#import "MMAdUtil.h"
#import "MMParameterStringReplacement.h"

#define SCREEN_HEIGHT  [ [ UIScreen mainScreen ] bounds ].size.height
#define SCREEN_WIDTH  [ [ UIScreen mainScreen ] bounds ].size.width

@interface MPMoviePlayerController (MakoAdvertisingPrivate)

@property (nonatomic, strong) MFLoader *adsLoader;
@property (nonatomic, strong) MFManager *adsManager;
@property (nonatomic) NSTimeInterval currentTime;
@property (nonatomic, strong) NSTimer *playbackTimer;
@property (nonatomic, strong) NSNumber *accumulatedTime;
@property (nonatomic, strong) NSNumber *previousTime;
@property (nonatomic, strong) UIButton *doneButton;

@end

@implementation MPMoviePlayerController (MakoAdvertising)


#pragma mark - FUCKING getters and setters beacuse of the FUCKING category

//-(UIActivityIndicatorView *)loader {
//    return objc_getAssociatedObject(self, @"loader");
//}
//
//- (void)setLoader:(UIActivityIndicatorView *)loader{
//    objc_setAssociatedObject(self, @"loader", loader, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
//}


-(NSString *)doneButton {
    return objc_getAssociatedObject(self, @"doneButton");
}

- (void)setDoneButton:(UIButton *)button{
    objc_setAssociatedObject(self, @"doneButton", button, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(BOOL)isAllVideoAdsCompleted {
    NSNumber *number = objc_getAssociatedObject(self, @"isAllVideoAdsCompleted");
    
    return [number boolValue];
}

-(void)setIsAllVideoAdsCompleted:(BOOL)isAllVideoAdsCompleted {
    NSNumber *number = [NSNumber numberWithBool: isAllVideoAdsCompleted];
    objc_setAssociatedObject(self, @"isAllVideoAdsCompleted", number, OBJC_ASSOCIATION_RETAIN);
    
}

-(NSString *)language {
    return objc_getAssociatedObject(self, @"language");
}

- (void)setLanguage:(NSString *)language{
    objc_setAssociatedObject(self, @"language", language, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(NSTimer *)playbackTimer {
    return objc_getAssociatedObject(self, @"playbackTimer");
}


- (void)setPlaybackTimer:(NSTimer *)playbackTimer{
    objc_setAssociatedObject(self, @"playbackTimer", playbackTimer, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(NSNumber*)accumulatedTime {
    return objc_getAssociatedObject(self, @"accumulatedTime");
}


- (void)setAccumulatedTime:(NSNumber *)accumulatedTime{
    objc_setAssociatedObject(self, @"accumulatedTime", accumulatedTime, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(NSNumber*)previousTime {
    return objc_getAssociatedObject(self, @"previousTime");
}


- (void)setPreviousTime:(NSNumber *)previousTime{
    objc_setAssociatedObject(self, @"previousTime", previousTime, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}


-(NSTimeInterval)currentTime
{
    // NSLog(@"%f", [self.accumulatedTime floatValue]);
    return [self.accumulatedTime floatValue];
}

-(void)setCurrentTime:(NSTimeInterval)currentTime
{
    
}

- (void)updatePlaybackTime:(NSTimer*)timer {
    if(self.playbackState == MPMoviePlaybackStatePlaying)
    {
        [self willChangeValueForKey:@"currentTime"];
        
        CGFloat accumulatedTimeSoFar = [self.accumulatedTime floatValue];
        CGFloat timeDiff = MAX(0, self.currentPlaybackTime - [self.previousTime floatValue]);
        if(timeDiff > 0.6)
        {
            timeDiff = 0;
        }
        accumulatedTimeSoFar += timeDiff;
        
        self.accumulatedTime = @(accumulatedTimeSoFar);
        self.previousTime = @(self.currentPlaybackTime);
        
        self.currentTime = [self.accumulatedTime floatValue];
        [self didChangeValueForKey:@"currentTime"];
    }
}

-(MFLoader *)adsLoader {
    
    MFLoader *loader = objc_getAssociatedObject(self, @"adsLoader");
    if (!loader) {
        MFSettings *settings = [[MFSettings alloc] init];
        if(!self.language)
        {
            self.language = @"en";
        }
        settings.language = self.language;
        loader = [[MFLoader alloc] initWithSettings:settings];
        loader.delegate = self;
        
        objc_setAssociatedObject(self, @"adsLoader", loader, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    
    return objc_getAssociatedObject(self, @"adsLoader");
}


- (void)setAdsLoader:(MFLoader*)adsLoader {
    objc_setAssociatedObject(self, @"adsLoader", adsLoader, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}


-(MFManager *)adsManager {
    return objc_getAssociatedObject(self, @"adsManager");
}


- (void)setAdsManager:(MFManager*)adsManager {
    objc_setAssociatedObject(self, @"adsManager", adsManager, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}


#pragma mark - player delegate

-(void)playerPlaybackStateDidChangeNotification:(NSNotification*)notification
{
    if (self.playbackState == MPMoviePlaybackStatePlaying)
    {
        [self startTimer];
    }
    if (self.playbackState == MPMoviePlaybackStateStopped)
    {
        //        [self.adsManager destroy];
        [self.playbackTimer invalidate];
        self.playbackTimer = nil;
    }
    if (self.playbackState == MPMoviePlaybackStatePaused)
    {
        [self.playbackTimer invalidate];
        self.playbackTimer = nil;
    }
    if (self.playbackState == MPMoviePlaybackStateInterrupted)
    {
        [self.playbackTimer invalidate];
        self.playbackTimer = nil;
    }
}


- (void) movieContentDidFinish:(NSNotification*)notification{
    int reason = [[[notification userInfo] valueForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey] intValue];
    
    if (reason == MPMovieFinishReasonPlaybackEnded)
    {
        [self.adsManager contentComplete];
    }
}


#pragma mark -


-(void)startTimer
{
    if(!self.playbackTimer)
    {
        self.playbackTimer = [NSTimer scheduledTimerWithTimeInterval:0.3
                                                              target:self
                                                            selector:@selector(updatePlaybackTime:)
                                                            userInfo:nil
                                                             repeats:YES];
    }
}

-(void)playWithMakoCatDFP:(NSDictionary*)makoCatDFPDict andVidId:(NSString *)vidId {
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerPlaybackStateDidChangeNotification:)
                                                 name:MPMoviePlayerPlaybackStateDidChangeNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(movieContentDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appWillEnterForeground)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appWillGoToBackground)
                                                 name:UIApplicationWillResignActiveNotification
                                               object:nil];
    
    [self addDoneButton];
    
    if(!makoCatDFPDict) {
        
        makoCatDFPDict = [[MMAdManager getInstance] defaultMakoCatDFP];
    }
    
    NSDictionary *makoCatDFPDictExtration = [MMAdUtil extractMakoCatDFPDictionary:makoCatDFPDict];
    makoCatDFPDict = [[[MMParameterStringReplacement alloc] init] replaceVariablesInCustParams:makoCatDFPDictExtration];
    
    NSString *iu = makoCatDFPDict[@"iu"];
    
    // ochel tov issues
    NSString *bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
    if ([bundleIdentifier isEqualToString:@"com.keshet.OchelTov"]) {
        iu = [iu stringByReplacingOccurrencesOfString:@"mako_mobile" withString:@"Ochel_Tov"];
    }
    
    NSDictionary *config = [[MMAdManager getInstance] adAreaConfigForForIU:iu];
    NSDictionary *format = [[MMAdManager getInstance] formatDictionaryForIU:iu formatID:@"video_ad_rules"];
    
    if(config && config[@"force_replace_iu"])
    {
        iu = config[@"force_replace_iu"];
    }
    if([MMAdManager getInstance].advertising_tree_force_replace)
    {
        iu = [MMAdUtil stringByReplacingTreeOnIU:iu];
    }
    
    if ([format count] > 0 && ![MMAdManager getInstance].isPayingUser) {
        self.language = [format objectForKey:@"title_language"];
        NSString *adRulesUrl = [format objectForKey:@"adrules_url"];
        NSString *iuString = iu;
        iuString = ([iuString hasSuffix:@"/"]) ? iuString : ([NSString stringWithFormat:@"%@/", iuString]);
        iuString = [iuString stringByReplacingOccurrencesOfString:@"/" withString:@"%2F"];
        
        adRulesUrl = [adRulesUrl stringByReplacingOccurrencesOfString:@"%IU_PARAMS_ENCODED%" withString:iuString];
        adRulesUrl = [adRulesUrl stringByReplacingOccurrencesOfString:@"%VID%" withString:vidId];
        NSDictionary *cust_params_dictionary =  [MMAdUtil dictionaryWithNonEmptyValuesForDictionary:makoCatDFPDict[@"cust_params"]];
        NSString *cust_params = [[self custParamsStringFromDictionary:cust_params_dictionary] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        cust_params = [cust_params stringByReplacingOccurrencesOfString:@"=" withString:@"%3D"];
        cust_params = [cust_params stringByReplacingOccurrencesOfString:@"&" withString:@"%26"];
        adRulesUrl = [adRulesUrl stringByReplacingOccurrencesOfString:@"%CUST_PARAMS%" withString:cust_params];
        
        [self requestAds:adRulesUrl];
    }
    else
    {
        self.isAllVideoAdsCompleted = YES;
        [[NSNotificationCenter defaultCenter] postNotificationName:VideoAd_Action object:@{VideoAd_Action : VideoAd_ErrorLoad}];
    }
}


-(void)addDoneButton {
    
    NSDictionary *buttonPreferences = [[MMAdManager getInstance] getDoneButtonPreferences];
    
    if (!buttonPreferences) return;
    
    NSString *buttonLocation = buttonPreferences[DONE_BUTTON_LOCATION_STRING] ? buttonPreferences[DONE_BUTTON_LOCATION_STRING] : @"top-left";
    NSString *buttonMarginX = buttonPreferences[DONE_BUTTON_MARGIN_X_STRING] ? buttonPreferences[DONE_BUTTON_MARGIN_X_STRING] : @"10";
    NSString *buttonMarginY = buttonPreferences[DONE_BUTTON_MARGIN_Y_STRING] ? buttonPreferences[DONE_BUTTON_MARGIN_Y_STRING] : @"10";
    NSString *buttonWidth = buttonPreferences[DONE_BUTTON_WIDTH] ? buttonPreferences[DONE_BUTTON_WIDTH] : @"100";
    NSString *buttonHeight = buttonPreferences[DONE_BUTTON_HEIGHT] ? buttonPreferences[DONE_BUTTON_HEIGHT] : @"100";
    NSString *buttonTextColor = buttonPreferences[DONE_BUTTON_TEXT_COLOR] ? buttonPreferences[DONE_BUTTON_TEXT_COLOR] : @"255.0,255.0,255.0";
    NSString *buttonText = buttonPreferences[DONE_BUTTON_TEXT] ? buttonPreferences[DONE_BUTTON_TEXT] : @"חזור";
    buttonTextColor = [buttonTextColor stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSArray *buttonColorArray = [buttonTextColor componentsSeparatedByString:@","];
    if ([buttonColorArray count] != 3) {
        buttonColorArray = @[@"255.0,255.0,255.0"];
    }
    NSArray *buttonLicationArray = [buttonLocation componentsSeparatedByString:@"-"];
    if ([buttonLicationArray count] != 2) {
        buttonLicationArray = @[@"top", @"left"];
    }
    
    NSString *buttonLocationVertical = [buttonLicationArray objectAtIndex:0];   // top/bottom
    NSString *buttonLocationHorizontal = [buttonLicationArray objectAtIndex:1]; // left/right
    
    CGFloat x;
    CGFloat y;
    CGFloat width = buttonWidth.floatValue;
    CGFloat height = buttonHeight.floatValue;
    
    if ([buttonLocationVertical isEqualToString:@"top"]) {
        y = buttonMarginY.floatValue;
        
        if ([buttonLocationHorizontal isEqualToString:@"left"]) { // top-left
            x = buttonMarginY.floatValue;
        }
        else { // top-right
            x = MAX(SCREEN_WIDTH, SCREEN_HEIGHT) - buttonWidth.floatValue - buttonMarginX.floatValue;
        }
    }
    
    else {
        y = MIN(SCREEN_WIDTH, SCREEN_HEIGHT) - buttonHeight.floatValue - buttonMarginY.floatValue;
        
        if ([buttonLocationHorizontal isEqualToString:@"left"]) { // bottom-left
            x = buttonMarginY.floatValue;
        }
        
        else {
            x = MAX(SCREEN_WIDTH, SCREEN_HEIGHT) - buttonWidth.floatValue - buttonMarginX.floatValue;
        }
    }
    
    UIColor *buttonColor = [UIColor colorWithRed:((NSString*)buttonColorArray[0]).floatValue/255.0
                                           green:((NSString*)buttonColorArray[1]).floatValue/255.0
                                            blue:((NSString*)buttonColorArray[2]).floatValue/255.0
                                           alpha:1.0];
    
    self.doneButton = [[UIButton alloc] initWithFrame:CGRectMake(x, y, width, height)];
    self.doneButton.tag = DONE_BUTTON_TAG;
    [self.doneButton setTitle:buttonText forState:UIControlStateNormal];
    [self.doneButton addTarget:self action:@selector(doneButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    self.doneButton.hidden = YES;
    self.doneButton.backgroundColor = [UIColor clearColor];
    [self.doneButton.titleLabel setFont:[UIFont fontWithName:@"Arial" size:16]];
    [self.doneButton setTitleColor:buttonColor forState:UIControlStateNormal];
    [self.view addSubview:self.doneButton];
    
}


-(void)doneButtonPressed:(id)sender {
    
    [self.adsManager destroy];
    [[NSNotificationCenter defaultCenter] postNotificationName:VideoAd_Action object:@{VideoAd_Action : VideoAd_DonePressed}];
}


-(NSString*)custParamsStringFromDictionary:(NSDictionary*)custParammsDictionary
{
    NSMutableArray *cust_params_array = [NSMutableArray array];
    
    for (NSString *key in custParammsDictionary.allKeys) {
        NSString *pair = [NSString stringWithFormat:@"%@=%@", key, custParammsDictionary[key]];
        [cust_params_array addObject:pair];
    }
    
    NSString *result = [cust_params_array componentsJoinedByString:@"&"];
    return result;
}


-(void)appWillEnterForeground {
    [self.adsManager resume];
}


-(void)appWillGoToBackground {
    [self.adsManager pause];
}


- (void)requestAds:(NSString*)makoCatDFP {
    NSString *adTag = makoCatDFP;
    [self.adsLoader requestAdsAdRulesUrl:adTag];
}


#pragma mark - adsLoaderDelegate

- (void)adsLoaderLoaded:(MFLoader *)loader withManager:(MFManager *)adsManager {
    
    self.adsManager = adsManager;
    self.adsManager.delegate = self;
    
    [self.adsManager startOnView:self.view];
}


- (void)adsLoader:(MFLoader *)loader failedWithErrorData:(NSDictionary *)adErrorData {
    // Loading failed, log it.
    //    NSLog(@"Ad loading error: %@", adErrorData);
    [self.playbackTimer invalidate];
    self.playbackTimer = nil;
    
    //    [self.loader stopAnimating];
    //    self.loader.hidden = YES;
    
    self.isAllVideoAdsCompleted = YES;
    [[NSNotificationCenter defaultCenter] postNotificationName:VideoAd_Action object:@{VideoAd_Action : VideoAd_ErrorLoad}];
}

#pragma mark - adsManagerDelegate


- (void)adsManagerDidRequestContentPause:(MFManager *)adsManager {
    // Pause the content.
    
    [self pause];
    //    [self.loader startAnimating];
    //    self.loader.hidden = NO;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:VideoAd_Action object:@{VideoAd_Action : VideoAd_Action_StartShowAd}];   
}


- (void)adsManagerDidRequestContentResume:(MFManager *)adsManager {
    //    [self.loader stopAnimating];
    //    self.loader.hidden = YES;
    
    self.doneButton.hidden = YES;
    [[NSNotificationCenter defaultCenter] postNotificationName:VideoAd_Action object:@{VideoAd_Action : VideoAd_Action_FinishShowCurrentAdPod}];
}

// Process ad events.
- (void)adsManager:(MFManager *)adsManager didReceiveAdEvent:(MFEvent *)event {
    if (event.type == kMFAdEvent_PAUSE) {
        
    }
    //    [self.loader stopAnimating];
    //    self.loader.hidden = YES;
    
    if(event.type == kMFAdEvent_LOADED) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:VideoAd_Action object:@{VideoAd_Action : VideoAd_Action_LoadAd}];
    }
    
    if (event.type == kMFAdEvent_STARTED) {
        
        [self.view addSubview:self.doneButton];
        if(adsManager.currentAdVideoUrl.length == 0)
        {
           adsManager.currentAdVideoUrl = @"";
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:VideoAd_Action object:@{VideoAd_Action : VideoAd_Action_WillShowAd, @"ad_url": adsManager.currentAdVideoUrl}];
        

    }
    
    if (event.type == kMFAdEvent_ALL_ADS_COMPLETED) {
        self.doneButton.hidden = YES;
        self.isAllVideoAdsCompleted = YES;
        [[NSNotificationCenter defaultCenter] postNotificationName:VideoAd_Action object:@{VideoAd_Action : VideoAd_Action_FinishShowAllAds}];
    }
    
    if (event.type == kMFAdEvent_COMPLETE) {
        self.doneButton.hidden = YES;
        [[NSNotificationCenter defaultCenter] postNotificationName:VideoAd_Action object:@{VideoAd_Action : VideoAd_Action_CurrentAdCompleted}];
    }
    
    if (event.type == kMFAdEvent_PAUSE) {
        [[NSNotificationCenter defaultCenter] postNotificationName:VideoAd_Action object:@{VideoAd_Action : VideoAd_Action_CurrentAdPaused}];
    }
    
    if (event.type == kMFAdEvent_RESUME) {
        [[NSNotificationCenter defaultCenter] postNotificationName:VideoAd_Action object:@{VideoAd_Action : VideoAd_Action_CurrentAdResumed}];
    }
}

-(void)adsManager:(MFManager *)adsManager didReceiveAdError:(NSDictionary *)error {
    //    NSLog(@"Error during ad playback: %@", error);
    [self destroyAd];
}

#pragma mark -

-(void)destroyAd {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.adsLoader.adLoaderAborted = YES;
    if(self.adsManager)
    {
        [self.adsManager destroy];
    }
}

@end
