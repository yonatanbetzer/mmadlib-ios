//
//  MMCounterView.m
//  MMAd
//
//  Created by Nir Etzion on 3/5/14.
//  Copyright (c) 2014 Mako. All rights reserved.
//

#import "MMCounterView.h"

@interface MMCounterView ()
@property (nonatomic, strong) UILabel *lbl;
@end

@implementation MMCounterView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.lbl = [[UILabel alloc] initWithFrame:self.bounds];
        [self addSubview:self.lbl];
        self.lbl.backgroundColor = [UIColor colorWithRed:29.0/256.0 green:29.0/256.0 blue:29.0/256.0 alpha:1.0];
        self.lbl.textColor = [UIColor colorWithRed:142.0/256.0 green:142.0/256.0 blue:142.0/256.0 alpha:1.0];
        self.lbl.textAlignment = NSTextAlignmentCenter;
        self.lbl.font = [UIFont systemFontOfSize:12];
    }
    return self;
}

- (void) setFrame:(CGRect)frame{
    [super setFrame:frame];
    self.lbl.frame = self.bounds;
}

- (void) setText:(NSString *)text{
    self.lbl.text = text;   
}


@end
