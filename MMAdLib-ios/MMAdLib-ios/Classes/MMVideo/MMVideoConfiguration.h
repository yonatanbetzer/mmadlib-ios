//
//  MMVideoConfiguration.h
//  MMAd
//
//  Copyright (c) 2013 Mako. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MMVideoConfiguration : NSObject

@property (nonatomic, strong) NSString *strUrl;
@property (nonatomic, strong) NSString *castupFallbackParam;
@property (nonatomic, strong) NSString *castupFallbackDelimiter;
@property (nonatomic) NSInteger requestTimeout;
@property (nonatomic) NSInteger showBackButton;
@property (nonatomic) NSInteger showCountdown;

@end
