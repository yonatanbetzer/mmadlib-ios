//
//  MMClickableStripView.m
//  Mako
//
//  Copyright (c) 2013 Mako. All rights reserved.
//

#import "MMClickableStripView.h"
#import "MMAdManager.h"
#import "MMAdUtil.h"
//#import <MakoLocation/AFNetworking.h>

@implementation MMClickableStripView


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithRed:22.0/255.0 green:22.0/255.0 blue:22.0/255.0 alpha:1.0];
        [self.layer setBorderColor:[UIColor colorWithRed:44.0/255.0 green:44.0/255.0 blue:44.0/255.0 alpha:1.0].CGColor];
        [self.layer setBorderWidth:1.0];
        self.btnMessage = [UIButton buttonWithType:UIButtonTypeCustom];
        [self addSubview:self.btnMessage];
        [self.btnMessage addTarget:self action:@selector(showFullSizeAd:) forControlEvents:UIControlEventTouchUpInside];
        [self.btnMessage setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.supportedOrientations = UIInterfaceOrientationMaskAll;
        [self.btnMessage.titleLabel setFont:STRIP_FONT_NEW(24)];
    }
    
    return self;
}

- (void) setFrame:(CGRect)frame{
    [super setFrame:frame];
    self.btnMessage.frame = self.bounds;
}

- (void) setStrMessage:(NSString *)stringMessage{
    _strMessage = stringMessage;
    [self.btnMessage setTitle:stringMessage forState:UIControlStateNormal];
}

- (IBAction)showFullSizeAd:(id)sender {
    // load the landing page
    NSURL *url = [NSURL URLWithString:self.strUrl];

    if (url)
    {
        landingPageViewController = [[MMLandingPageViewController alloc] initWithNibName:nil bundle:nil];
        landingPageViewController.supportedOrientations = self.supportedOrientations;
        landingPageViewController.openInExternalBrowser = self.external;
        [landingPageViewController displayLandingPageWithURL:url];
        
        //  report the click
        [self sendReportClick];
    }
    else
        // malformed URL
        AdLog(@"VideoAd landing page URL error: %@", self.strUrl);
}

- (void) sendReportClick{
    for (NSString *strUrl in self.arrReportClick){
        NSURL *url = [NSURL URLWithString:strUrl];
        NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:kAD_MANAGER_LOADING_TIMEOUT];
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *op, id responseObject){
        }
                                         failure:^(AFHTTPRequestOperation *op, NSError *error){                                             
                                         }];
        [operation start];
    }
}


- (void) fullSizeAdWasClosed {
    // to resume the preroll movie

}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    
    
//    // Drawing code
//    CGContextRef context =UIGraphicsGetCurrentContext();
//    
//    // black line
//    [[UIColor colorWithRed:0 green:0 blue:0 alpha:0.5] setFill];
//    CGRect firstStrip = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, 1);
//    CGContextFillRect(context, firstStrip);
//    
//    // grey line
//    [[UIColor colorWithRed:138.0/255.0 green:138.0/255.0 blue:138.0/255.0 alpha:0.5] setFill];
//    CGRect secondStrip = CGRectMake(rect.origin.x, 1, rect.size.width, 1);
//    CGContextFillRect(context, secondStrip);
//    
//    // gradient
//    CGFloat colors [] = {
//        94.0/255.0, 94.0/255.0, 94.0/255.0, 0.5,
//        32.0/255.0, 32.0/255.0, 32.0/255.0, 0.5
//    };
//
//    
//    CGColorSpaceRef baseSpace = CGColorSpaceCreateDeviceRGB();
//    CGGradientRef gradient = CGGradientCreateWithColorComponents(baseSpace, colors, NULL, 2);
//    CGColorSpaceRelease(baseSpace), baseSpace = NULL;
//    
//    CGContextSaveGState(context);
//    CGRect upperRect = CGRectMake(rect.origin.x,  2 , rect.size.width, rect.size.height/2 - 1);
//    CGContextClip(context);
//    
//    CGPoint startPoint = CGPointMake(CGRectGetMidX(upperRect), CGRectGetMinY(upperRect));
//    CGPoint endPoint = CGPointMake(CGRectGetMidX(upperRect), CGRectGetMaxY(upperRect));
//    
//    CGContextDrawLinearGradient(context, gradient, startPoint, endPoint, 0);
//    CGGradientRelease(gradient), gradient = NULL;
//    
//    CGContextRestoreGState(context);
//    CGContextDrawPath(context, kCGPathStroke);
//    
//    // black rectengular
//    [[UIColor colorWithRed:0 green:0 blue:0 alpha:0.5] setFill];
//
//    CGRect lowerRect = CGRectMake(rect.origin.x, rect.size.height/2 + 1 , rect.size.width, rect.size.height/2 - 1);
//    CGContextFillRect(context, lowerRect);
    
//    self.alpha = 0.5;
}


@end
