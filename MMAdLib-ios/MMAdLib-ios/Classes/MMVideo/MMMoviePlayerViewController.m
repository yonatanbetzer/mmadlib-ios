//
//  MMMoviePlayerViewController.m
//  MMAd
//
//  Copyright (c) 2013 Mako. All rights reserved.
//

#import "MMMoviePlayerViewController.h"

#define kSTRIP_HEIGHT 44
#define IS_OS_7_OR_LATER    ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

@interface MMMoviePlayerViewController ()
@property (nonatomic, assign) CGRect counterRect;
@property (nonatomic, strong) NSTimer *backButtonTimer;
@property (nonatomic, strong) NSTimer *counterTimer;

@end

@implementation MMMoviePlayerViewController

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
    self.delegate = nil;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.moviePlayer = [[MPMoviePlayerController alloc] init];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willEnterForeground) name:UIApplicationWillEnterForegroundNotification object:nil];
        self.supportedOrientations = UIInterfaceOrientationMaskAll;
        
        CGFloat y = 0;
        if (IS_OS_7_OR_LATER) y = 0;
        self.counterRect = CGRectMake(0, y, 180, 19);
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.moviePlayer.view.frame = self.view.bounds;
    [self.view addSubview:self.moviePlayer.view];
    
    clickableStripView = [self createClickableStrip];
    if (clickableStripView)
        [self.view addSubview:clickableStripView];
    
    // set back button
    backButton = [[UIButton alloc] initWithFrame:CGRectMake(8, 8, 60, 32)];
    [backButton setBackgroundColor:[UIColor clearColor]];
    [backButton setTitle:@"חזור" forState:UIControlStateNormal];
    [backButton.layer setBorderColor:[UIColor colorWithRed:44.0/255.0 green:44.0/255.0 blue:44.0/255.0 alpha:1.0].CGColor];
    [backButton.layer setBorderWidth:1.0];
    [backButton.titleLabel setFont:STRIP_FONT_NEW(16)];
    [backButton addTarget:self action:@selector(userCanceled) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backButton];
    backButton.hidden = YES;
    if (_showBackButton != -1){
        self.backButtonTimer = [NSTimer scheduledTimerWithTimeInterval:_showBackButton target:self selector:@selector(unhideTheBackButton:) userInfo:nil repeats:NO];
    }
    
    if (self.showCountdown > 0){
        counterView = [[MMCounterView alloc] initWithFrame:self.counterRect];
        [self.view addSubview:counterView];
        counterView.hidden = YES;
        [self.counterTimer invalidate];
        self.counterTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(setCounterText) userInfo:nil repeats:YES];
        
    }
    
    
#ifdef DEBUG
    UIButton* skipAdAndContinueVideo = [[UIButton alloc] initWithFrame:CGRectMake(0, 100, 100, 40)];
    [skipAdAndContinueVideo setBackgroundColor:[UIColor greenColor]];
    [skipAdAndContinueVideo setTitle:@"Continue" forState:UIControlStateNormal];
    [skipAdAndContinueVideo addTarget:self action:@selector(continueToVideo) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:skipAdAndContinueVideo];
#endif
    
}

#ifdef DEBUG
-(void)continueToVideo {
    [self cancelTimers];
    
    if ([self.delegate respondsToSelector:@selector(skipAd)])
        [self.delegate skipAd];
    
}
#endif


-(void) cancelTimers {
    
    [self.counterTimer invalidate];
    self.counterTimer = nil;
    
    [self.backButtonTimer invalidate];
    self.backButtonTimer = nil;
}

- (void)unhideTheBackButton:(NSTimer*)t{
    backButton.hidden = NO;
    t = nil;
}


- (void)viewWillLayoutSubviews {
    self.moviePlayer.view.frame = self.view.bounds;
    
    CGRect containerFrame = self.view.bounds;
    
    CGSize clickbleStripMessageSize = [clickableStripView.strMessage sizeWithFont:clickableStripView.btnMessage.titleLabel.font];
    clickbleStripMessageSize.height += 10;
    clickbleStripMessageSize.width += 30;
    
    CGSize backButtonSize = [backButton.titleLabel.text sizeWithFont:backButton.titleLabel.font];
    backButtonSize.height +=10;
    backButtonSize.width += 30;
    
    CGRect clickableFrame;
    if (self.stripLocation == ClickableStripLocationBottom)
    {
        backButton.frame = CGRectMake(0, containerFrame.size.height-kSTRIP_HEIGHT, backButtonSize.width, kSTRIP_HEIGHT);
        [backButton setBackgroundColor:[UIColor clearColor]];
        [backButton.layer setBorderColor:[UIColor clearColor].CGColor];
        [backButton.layer setBorderWidth:0.0];
        clickableFrame = CGRectMake(containerFrame.origin.x-1, containerFrame.size.height-kSTRIP_HEIGHT, containerFrame.size.width+2, kSTRIP_HEIGHT+1);

    }
    else {
        backButton.frame = CGRectMake(10, containerFrame.size.height-kSTRIP_HEIGHT, backButtonSize.width, backButtonSize.height);
        
        clickableFrame = CGRectMake(containerFrame.origin.x + containerFrame.size.width - clickbleStripMessageSize.width - 5, containerFrame.origin.y + 5, clickbleStripMessageSize.width, kSTRIP_HEIGHT);
        clickableFrame.size = clickbleStripMessageSize;
    }
    
    clickableStripView.frame = clickableFrame;
    
    // if clickable strip is displayed
//    if (clickableStripView)
//        // set button centered to clickable strip
//        [backButton setCenter:CGPointMake(backButton.center.x, clickableStripView.center.y)];
    
    counterView.frame = self.counterRect;
}

//
// to support iOS 4.3
//
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    // Add or remove subviews from the current view hierarchy
    // after rotation animation ends.
    self.moviePlayer.view.frame = self.view.bounds;
    
    CGRect containerFrame = self.view.bounds;
    [backButton setFrame:CGRectMake(8, 8, 60, 32)];
    
    CGRect clickableFrame;
    if (self.stripLocation == ClickableStripLocationBottom)
    {
        clickableFrame = CGRectMake(containerFrame.origin.x, containerFrame.size.height-kSTRIP_HEIGHT, containerFrame.size.width, kSTRIP_HEIGHT);
    }
    else
        clickableFrame = CGRectMake(containerFrame.origin.x, containerFrame.origin.y, containerFrame.size.width, kSTRIP_HEIGHT);
    
    clickableStripView.frame = clickableFrame;
    
    // if clickable strip is displayed
    if (clickableStripView)
        // set button centered to clickable strip
        [backButton setCenter:CGPointMake(backButton.center.x, clickableStripView.center.y)];
}
//
//
//

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//#pragma mark - orientation
//-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
//	return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
////    return YES;
//}
//
//// iOS6
//- (BOOL) shouldAutorotate{
//    return NO;
////    return YES;
//}
//- (NSUInteger)supportedInterfaceOrientations {
//    return UIInterfaceOrientationMaskLandscape;
////    return UIInterfaceOrientationMaskAll;
//}

#pragma mark - orientation
-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return [self shouldAutorotate] && ([self supportedInterfaceOrientations] & (1 << toInterfaceOrientation));
}

// iOS6
- (BOOL) shouldAutorotate{
    return YES;
}
- (NSUInteger)supportedInterfaceOrientations {
    return self.supportedOrientations;
}



- (MMClickableStripView*) createClickableStrip {
    
    MMClickableStripView *clickableStrip = nil;
    // check if preroll is clickable by checking existance of assoicatedURL
    if ((self.strClickURL && ![self.strClickURL isEqualToString:@""]))
    {
        clickableStrip = [[MMClickableStripView alloc] initWithFrame:CGRectZero];
        clickableStrip.strUrl = self.strClickURL;
        clickableStrip.strMessage = self.strMessage;
        clickableStrip.external = self.external;
        clickableStrip.arrReportClick = self.arrReportClick;
        clickableStrip.supportedOrientations = self.supportedOrientations;
        
        
        // check if no strip message was set
        if (!clickableStrip.strMessage || [clickableStrip.strMessage length] == 0)
            // set default strip message
            clickableStrip.strMessage = kClickableStripDefaultMessage;
        
    } else {
        
        AdLog(@"preroll not clickable");
    }
    return clickableStrip;
    
}

- (void) setCounterText {
//    klogdbg_func_arg1s(@"################### TIMER ####################")
    NSInteger secondsLeft = 0;
    NSTimeInterval duration = self.moviePlayer.duration;
    NSTimeInterval currentPlaybackTime = self.moviePlayer.currentPlaybackTime;
    secondsLeft = (long)duration - currentPlaybackTime;
    if (secondsLeft > 0) {
        counterView.hidden = NO;
        counterView.text = [NSString stringWithFormat:@"הפרסומת תסתיים בעוד %d שניות", secondsLeft];
    }
    else {
        counterView.hidden = YES;
        [self.counterTimer invalidate];
        self.counterTimer = nil;
    }
    
}

# pragma mark - Pause/Resume video
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (self.moviePlayer.playbackState == MPMoviePlaybackStatePaused){
        [self.moviePlayer play];
        isPaused = NO;
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if (self.moviePlayer.playbackState == MPMoviePlaybackStatePlaying){
        [self.moviePlayer pause];
        isPaused = YES;
    }
    else {
        isPaused = NO;
    }
}
- (void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}

- (void) willEnterForeground {
    if (self.continuePlaying && !isPaused){
        [self.moviePlayer play];
    }
}

//mp.playbackState == MPMoviePlaybackStatePlaying
- (void) userCanceled {
    
    [self cancelTimers];
    
    [self.delegate videoWasCanceledByUser];
}


@end
