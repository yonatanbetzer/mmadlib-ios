//
//  MMMoviePlayerViewController.h
//  MMAd
//
//  Copyright (c) 2013 Mako. All rights reserved.
//

#import <MediaPlayer/MediaPlayer.h>
#import "MMAdUtil.h"
#import "MMClickableStripView.h"
#import "MMCounterView.h"

@protocol MMMoviePlayerViewControllerDelegate <NSObject>
@optional
- (void) videoWasCanceledByUser;

#ifdef DEBUG
-(void) skipAd;

#endif
@end


@interface MMMoviePlayerViewController : UIViewController {
    
    MMClickableStripView *clickableStripView;
    UIButton * backButton;
    MMCounterView * counterView;
    BOOL isPaused;
}

@property (nonatomic) NSInteger showBackButton;
@property (nonatomic) NSInteger showCountdown;
@property (nonatomic, strong) MPMoviePlayerController *moviePlayer;
@property (nonatomic, strong) NSString *strClickURL;
@property (nonatomic, strong) NSString *strMessage;
@property (nonatomic, strong) NSMutableArray *arrReportClick;
@property (nonatomic) BOOL external;

@property (nonatomic) ClickableStripLocation stripLocation;

@property BOOL continuePlaying;
@property (nonatomic) NSUInteger supportedOrientations;

@property (nonatomic, weak) id <MMMoviePlayerViewControllerDelegate> delegate;


@end
