//
//  PrerollParser.m
//  Mako
//
//  Copyright (c) 2013 Mako. All rights reserved.
//

#import "MMVideoAdParser.h"

#import "MMAdUtil.h"

@implementation VideoAdEntry

- (void) setFallbackURLArray:(NSMutableArray *)fallbackURLArrayArg {
    if (_fallbackURLArray != fallbackURLArrayArg){
        _fallbackURLArray = [fallbackURLArrayArg mutableCopy];
    }
}

@end

@interface  MMVideoAdParser (private)
-(VideoAdEntry*)parseEntry:(CXMLElement *)elem;
-(VideoAdEntry*)parseVastV2:(CXMLDocument *)xmlDoc;
-(VideoAdEntry*)parseVastV1:(CXMLDocument *)xmlDoc;
-(void)noVideoEntryFound;
@end

@implementation MMVideoAdParser


-(id) initWithDelegate:(id<MMVideoParserDelegate>)aDelegate withFormat:(NSDictionary*)formatParam{
    self = [self init];
    
    if (self != nil){
        self.delegate = aDelegate;
        format = formatParam;
        reportStartArr = [[NSMutableArray alloc] initWithCapacity:1];
        reportCompleteArr = [[NSMutableArray alloc] initWithCapacity:1];
        reportClickArr = [[NSMutableArray alloc] initWithCapacity:1];
    }
    return self;
}

- (void)parsePrerollAtURL:(NSString *)sURL withTimeout:(NSInteger)requestTimeout{
    delegateWasMessaged = NO;
    [self internalParsePrerollAtURL:sURL withTimeout:requestTimeout];
}

- (void)internalParsePrerollAtURL:(NSString *)sURL withTimeout:(NSInteger)requestTimeout{
    _connection = nil; _rawData = nil;
    
    NSURL *url = [MMAdUtil urlFromStringWithWhiteSpaces:sURL];
    
    NSURLRequest * request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:requestTimeout];
    
    _connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    if (_rawData == nil) 
		_rawData = [[NSMutableData alloc] initWithCapacity:2048];
	
    [_rawData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    _connection = nil;
    _rawData = nil;
    
    [self noVideoEntryFound];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    _connection = nil;
	
	NSString * sData = [[NSString alloc] initWithData:_rawData encoding:NSUTF8StringEncoding];
    
    if (!sData){
        [self noVideoEntryFound];
    }
    
    NSString * sModifiedData = [sData stringByReplacingOccurrencesOfString:@"&" withString:@"&amp;"];
    sModifiedData = [sModifiedData stringByReplacingOccurrencesOfString:@"&amp;amp;" withString:@"&amp;"];

    _rawData = nil;
    sData = nil;
    
    NSError *error = nil;
    CXMLDocument *xmlDoc = [[CXMLDocument alloc] initWithData:[sModifiedData dataUsingEncoding:NSUTF8StringEncoding] options:0 error:&error];
    
    if (xmlDoc == nil){
        [self videoParserFailedWithError:error];
        return;
    }
        
    NSString * str =  [xmlDoc rootElement].name;
    
    if ([str isEqualToString:@"asx"]){ // handle asx data
        
        // check For "Entry" node -> direct video link 
        NSArray *resultNodes =  [xmlDoc nodesForXPath:@"//Entry" error:nil];
        
        if ([resultNodes count] > 0){
            CXMLElement * elem = [resultNodes objectAtIndex:0];
            
            VideoAdEntry * entry = [self parseEntry:elem];
            entry.reportStartArr = reportStartArr;
            entry.reportCompleteArr = reportCompleteArr;
            entry.reportClickArr = reportClickArr;
            [self videoParsedSuccesfully:entry];
            return;
        }
        
        // check for "Entryref" node -> reload parser with the new url
        resultNodes = [xmlDoc nodesForXPath:@"//Entryref" error:nil];
        
        if ([resultNodes count] > 0){
            CXMLElement * elem = [resultNodes objectAtIndex:0]; // should be only one

            // since this is a reference to another xml with an element or another href
            // accumulate the reportStart , reportComplete and reportClick links
            NSString * reportStartUrl = [[elem attributeForName:@"reportStart"] stringValue];
            if (reportStartUrl)
                [reportStartArr addObject:reportStartUrl];
            NSString * reportCompleteUrl = [[elem attributeForName:@"reportComplete"] stringValue];
            if (reportCompleteUrl)
                [reportCompleteArr addObject:reportCompleteUrl];
            NSString * reportClickUrl = [[elem attributeForName:@"reportClick"] stringValue];
            if (reportClickUrl)
                [reportClickArr addObject:reportClickUrl];
            
            NSString * href = [[elem attributeForName:@"HREF"] stringValue];
            [self internalParsePrerollAtURL:href withTimeout:kVIDEO_REQUEST_TIMEOUT];
             
            return;

        }
        // no valid entry/entryref found
        [self noVideoEntryFound];
    }
    else if ([str isEqualToString:@"VAST"]){ // vast 2.0s
        // parse VAST 2.0
        VideoAdEntry * entry = [self parseVastV2:xmlDoc];
        
        entry.reportStartArr = reportStartArr;
        entry.reportCompleteArr = reportCompleteArr;
        entry.reportClickArr = reportClickArr;
        
        [_delegate videoParserParsedSuccessfully:entry];
    }
    else if ([str isEqualToString:@"VideoAdServingTemplate"]){ // vast 1.0
        // parse VAST 1.0
        VideoAdEntry * entry = [self parseVastV1:xmlDoc];

        entry.reportStartArr = reportStartArr;
        entry.reportCompleteArr = reportCompleteArr;
        entry.reportClickArr = reportClickArr;

        [_delegate videoParserParsedSuccessfully:entry];
    }
    else {
        // no valid entry object was found
        [self noVideoEntryFound];
    }
}

// parse ver 2.0 vast
-(VideoAdEntry *)parseVastV2:(CXMLDocument *)xmlDoc{
    
    VideoAdEntry * _entry  = [[VideoAdEntry alloc] init]; // initialize entry object
    
    _entry.trackingEventsDict = [[NSMutableDictionary alloc] init]; // initialize tracking event dictionary
        
    NSArray * resultNodes = NULL;
    
    // set impression track event
    resultNodes = [xmlDoc nodesForXPath:@"//InLine/Impression" error:nil];
    
    if ([resultNodes count] > 0){
        NSString * temp = [[resultNodes objectAtIndex:0] stringValue];
        
        [_entry.trackingEventsDict setObject:temp forKey:@"impression"];
    }
        
    // set error track event
    resultNodes = [xmlDoc nodesForXPath:@"//InLine/Error" error:nil];
    
    if ([resultNodes count] > 0){
        [_entry.trackingEventsDict setObject:[[resultNodes objectAtIndex:0] stringValue] forKey:@"error"]; 
    }
    
    // get linear element from creatives (handle only linear creatives)
    resultNodes = [xmlDoc nodesForXPath:@"//Creative/Linear" error:nil];
    
    CXMLElement * linear = nil;
    if ([resultNodes count] > 0)
        linear = [resultNodes objectAtIndex:0]; // handle only linear creatives
               
    // get media files
    resultNodes = [linear nodesForXPath:@"//MediaFile" error:nil];
    
    NSMutableArray * arr = [NSMutableArray arrayWithCapacity:1];
    
    for(CXMLElement * mediaElem in resultNodes){
        NSString * sURL = [mediaElem stringValue];
        NSURL *url = [MMAdUtil urlFromStringWithWhiteSpaces:sURL];
        if (url)
            [arr addObject:url];
    } 
    
    _entry.fallbackURLArray = arr;
        
    // get tracking events
    resultNodes = [linear nodesForXPath:@"//TrackingEvents/Tracking" error:nil];
        
    for(CXMLElement * trackingElem in resultNodes){
                
        NSString * sURL = [trackingElem stringValue];
        NSString * sName = [[trackingElem attributeForName:@"event"] stringValue];
        
        if (sURL != nil && sName != nil)
           [_entry.trackingEventsDict setObject:sURL forKey:sName];
    } 
    
    // get click url
    resultNodes = [linear nodesForXPath:@"//VideoClicks/ClickThrough" error:nil];
    
    if ([resultNodes count] > 0){
        CXMLElement * elem = [resultNodes objectAtIndex:0];
        NSString *strToFollow = [[elem stringValue] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        // we agreed with the advertiser that the click-through link will have an additional argument in the url makoExternal=1
        // if they want to open the maavaron with the browser outside the application
        BOOL makoExternal = NO;
        NSRange range = [strToFollow rangeOfString:@"&makoOpenExternal=1" options:NSCaseInsensitiveSearch];
        if (range.location != NSNotFound){
            makoExternal = YES;
            strToFollow = [strToFollow substringToIndex:range.location];
        }
        range = [strToFollow rangeOfString:@"makoOpenExternal=1" options:NSCaseInsensitiveSearch];
        if (range.location != NSNotFound){
            makoExternal = YES;
            strToFollow = [strToFollow substringToIndex:range.location];
        }
        
        _entry.clickThrough = strToFollow;
        _entry.external = makoExternal;
    }
    
//    // set click track event
//    resultNodes = [linear nodesForXPath:@"//VideoClicks/ClickTracking" error:nil];
//    if ([resultNodes count] > 0){
//        CXMLElement * elem = [resultNodes objectAtIndex:0];
//        
//        NSString * 
//        [_entry.trackingEventsDict setObject:[elem stringValue] forKey:@"click"];
//    }
        
    _entry.canSkip = NO;
    _entry.seekable = NO;
    _entry.entryType = @"Advert";

    // finish parsing vast 2.0
    
    return _entry;
}

// parse ver 1.0 vast
-(VideoAdEntry *)parseVastV1:(CXMLDocument *)xmlDoc{
       
    VideoAdEntry * _entry = [[VideoAdEntry alloc] init];
        
    NSArray * resultNodes = [xmlDoc nodesForXPath:@"//Creatives/Creative" error:nil];
    
    CXMLElement * creative = nil;
    if ([resultNodes count] > 0)
        creative = [resultNodes objectAtIndex:0]; // handle only first creative
    
    // get click url
    resultNodes = [creative nodesForXPath:@"//ClickThrough" error:nil];
    
    if ([resultNodes count] > 0){
        CXMLElement * clickElem = [resultNodes objectAtIndex:0];
        _entry.clickThrough = [[clickElem stringValue] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    }
    
    // set media files 
    resultNodes = [creative nodesForXPath:@"//MediaFile" error:nil];
    
    NSMutableArray * arr = [NSMutableArray arrayWithCapacity:1];
    
    for(CXMLElement * mediaElem in resultNodes){
        NSString * sURL = [mediaElem stringValue];
        
        NSURL *url = [MMAdUtil urlFromStringWithWhiteSpaces:sURL];
        if (url)
            [arr addObject:url];
    } 
                
    _entry.fallbackURLArray = arr;
    
    // set tracking events
    resultNodes = [creative nodesForXPath:@"//ClickTracking" error:nil];
    
    NSMutableArray * trackingEventsArray = [[NSMutableArray alloc] initWithCapacity:1];
    
    for(CXMLElement * trackingElem in resultNodes){
        NSString * sURL = [trackingElem stringValue];
        NSURL *url = [MMAdUtil urlFromStringWithWhiteSpaces:sURL];
        
        if (url)
            [trackingEventsArray addObject:url];
    } 

    _entry.canSkip = NO;
    _entry.seekable = NO;
    _entry.entryType = @"Advert";
    
    // finish parsing vast 2.0
    
    return _entry;
}

//
// parses the value inside://asx/Entry/param/ where the attribute name="eventListener"
// the value can be for example:"setOverlayText=לפרטים נוספים לחץ כאן|setOverlayPosition=T/B"
- (NSDictionary*) eventListenerData:(NSString*)eventListenerValues {
    NSMutableDictionary *result = [NSMutableDictionary dictionaryWithCapacity:2];
    // take all the values that within the eventName value
    NSArray *values = [eventListenerValues componentsSeparatedByString:@"|"];
    
    for (int i = 0; i < [values count]; i++){
        NSString *nameAndValue = [values objectAtIndex:i];
        
        // seperate the key and the value
        NSArray *arr = [nameAndValue componentsSeparatedByString:@"="];
        
        if ([arr count] > 1){
            NSString *value = [arr objectAtIndex:1];
            NSString *key   = [arr objectAtIndex:0];
            
            key = [key stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            [result setObject:value forKey:key];
        }
    }
    return  result;
}


-(VideoAdEntry *)parseEntry:(CXMLElement *)elem{
    
    VideoAdEntry * _entry = [[VideoAdEntry alloc] init]; 
    
    _entry.seekable = NO;
    
    NSString * clientSkip = [[elem attributeForName:@"clientskip"] stringValue];
       
    _entry.canSkip = ([clientSkip isEqualToString:@"true"])?YES:NO;
       
    for (CXMLElement * child in [elem children]){
        if ([[child name] isEqualToString:@"param"]){
            NSString * name = [[child attributeForName:@"name"] stringValue];
            NSString * value = [[child attributeForName:@"value"] stringValue];
            
            if ([name isEqualToString:@"EntryType"]){
                _entry.entryType = value;
            }else if ([name isEqualToString:@"AssociatedURL"]){ // click url
                _entry.clickThrough = value;
            } else if ([name isEqualToString:@"eventListener"]){
                // value : setOverlayText='לפרטים נוספים לחץ כאן'&setOverlayPosition='T/B'
                NSDictionary *dic = [self eventListenerData:value];
                NSString *stripText = [dic objectForKey:@"setOverlayText"];
                _entry.clickableStripText = stripText;
                    
                NSString *stripLocation = [dic objectForKey:@"setOverlayPosition"];
                if ([stripLocation isEqualToString:@"T"]){
                    _entry.clickableStripLocation = ClickableStripLocationTop;
                }
                else
                {
                    _entry.clickableStripLocation = ClickableStripLocationBottom;
                }
                NSString *isExternal = [dic objectForKey:@"external"];
                if ([isExternal isEqualToString:@"1"])
                    _entry.external = YES;
            }
            
        }else if ([[child name] isEqualToString:@"Ref"]){
            NSString * ref = [[child attributeForName:@"href"] stringValue];
                        
            _entry.fallbackURLArray = [MMAdUtil createFallbackList:ref WithVideoFallbackParam:format[@"fallbackIndication"] WithFallbackListDelimiter:format[@"fallbackDelimiter"]];
             NSString * reportClick = [[child attributeForName:@"reportClick"] stringValue];
            _entry.reportClick = reportClick;
            NSString * reportStartUrl = [[child attributeForName:@"reportStart"] stringValue];
            [reportStartArr addObject:reportStartUrl];
            NSString * reportCompleteUrl = [[child attributeForName:@"reportComplete"] stringValue];
            [reportCompleteArr addObject:reportCompleteUrl];
            NSString * reportClickUrl = [[child attributeForName:@"reportClick"] stringValue];
            [reportClickArr addObject:reportClickUrl];

            // its the same url in a case of entry
            //_entry.clickThrough = reportClick;
            
        }
    }
    
    // finished parsing Entry
    return _entry;
}

-(void)noVideoEntryFound{
    NSError *emptyFileError = [[NSError alloc] initWithDomain:kMMAdDomainError code:kMMAdEmptyFileError userInfo:nil];
    [self videoParserFailedWithError:emptyFileError];
}

- (void) cancelParser {
    [_connection cancel]; _connection = nil;
    NSError *appCanceledError = [[NSError alloc] initWithDomain:kMMAdDomainError code:kMMAdApplicationCanceledError userInfo:nil];
    [self videoParserFailedWithError:appCanceledError];
}


-(void) videoParserFailedWithError:(NSError*)error {
    @synchronized(self){
        if (!delegateWasMessaged){
            delegateWasMessaged = YES;
            [self.delegate videoParserFailedParsing:error];
        }
    }
}

- (void) videoParsedSuccesfully:(VideoAdEntry*)entry {
    @synchronized(self){
        if (!delegateWasMessaged){
            delegateWasMessaged = YES;
            [self.delegate videoParserParsedSuccessfully:entry];
        }
    }
}


-(void)dealloc{
    self.delegate = nil;
    [_connection cancel]; _connection = nil;
}

@end
