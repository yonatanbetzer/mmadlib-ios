//
//  MMClickableStripView.h
//  Mako
//
//  Copyright (c) 2013 Mako. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMLandingPageViewController.h"

#define STRIP_FONT_NEW(s) [UIFont fontWithName:@"ReformaNarrow" size:s]


@interface MMClickableStripView : UIView /*<MMLandingPageDelegate>*/ {
    MMLandingPageViewController *landingPageViewController;
}
- (IBAction)showFullSizeAd:(id)sender;

@property (nonatomic, weak) IBOutlet UIButton *btnMessage;
@property (nonatomic, strong) NSString *strMessage;
@property (nonatomic, strong) NSString *strUrl;
@property (nonatomic, strong) NSMutableArray *arrReportClick;
@property (nonatomic) BOOL external;
@property (nonatomic) NSUInteger supportedOrientations;

@end
