//
//  MMVideoAd.m
//  MMAd
//
//  Copyright (c) 2013 Mako. All rights reserved.
//

#import "MMVideoAd.h"
#import "MMAdManager.h"
#import "MMAdUtil.h"

#import "MMVideoConfiguration.h"
#import "MMMoviePlayerViewController.h"

#import "MMVideoAdParser.h"


@interface MMVideoAd (Private) <MMVideoParserDelegate, MMMoviePlayerViewControllerDelegate>

@property (nonatomic, assign) BOOL isReport;

@end


@implementation MMVideoAd

- (void) dealloc {
    [playbackPollTimer invalidate];
    self.delegate = nil;
    videoAdPlayerController.delegate = nil;
    
    [videoAdPlayerController.moviePlayer stop];
    
    [[NSNotificationCenter 	defaultCenter]
     removeObserver:self
     name:MPMoviePlayerPlaybackDidFinishNotification
     object:nil];
    
    [[NSNotificationCenter 	defaultCenter]
     removeObserver:self
     name:MPMoviePlayerLoadStateDidChangeNotification
     object:nil];
}

- (void) loadAndPlayWithVcmId:(NSString *)vcmId
                 videoChannelId:(NSString *)videoChannelId
               galleryChannelId:(NSString *)galleryChannelId {
    
    NSString *urlStr = [[MMAdManager getInstance] IMAurl];
    urlStr =  [urlStr stringByReplacingOccurrencesOfString:@"{vcmId}" withString:vcmId];
    urlStr =  [urlStr stringByReplacingOccurrencesOfString:@"{videoChannelId}" withString:videoChannelId];
    urlStr =  [urlStr stringByReplacingOccurrencesOfString:@"{galleryChannelId}" withString:galleryChannelId];
    
    NSURL *url = [ NSURL URLWithString:urlStr];

    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [AFJSONRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/html"]];

    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request
                                         
                                         
    success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        NSDictionary *jsonDict = JSON;
        NSDictionary *makoCatDFP = jsonDict[@"makoCatDFP"];
        [self loadAndPlayWithMakoCat:makoCatDFP];
    
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, id JSON) {

        
    }];
    
    [operation start];
    
    
}



- (void) loadAndPlayWithMakoCat:(NSDictionary*)makoCatDFP{
    self.supportedOrientations = (UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight);
    if (!isLoading){
        isLoading = YES;
        
        videoAdPlayerController = [[MMMoviePlayerViewController alloc] init];
        videoAdPlayerController.moviePlayer.controlStyle = MPMovieControlStyleNone;
        videoAdPlayerController.moviePlayer.fullscreen = YES;
        videoAdPlayerController.delegate = self;
        
        if(!makoCatDFP)
        {
            makoCatDFP =[[MMAdManager getInstance] defaultMakoCatDFP];
        }
        
        NSString *iu = makoCatDFP[@"iu"];
        iu =  [iu stringByReplacingOccurrencesOfString:@"mako" withString:@"makoTV"];
        NSDictionary *format = [[MMAdManager getInstance] formatDictionaryForIU:iu formatID:@"preroll"];
        NSDictionary *config = [[MMAdManager getInstance] adAreaConfigForForIU:iu];
        if(config && config[@"force_replace_iu"])
        {
            iu = config[@"force_replace_iu"];
        }
        if([MMAdManager getInstance].advertising_tree_force_replace)
        {
            iu = [MMAdUtil stringByReplacingTreeOnIU:iu];
        }
        
        
        if(format)
        {
            NSString *url = format[@"inline_call_url"];
            videoAdPlayerController.showBackButton = [format[@"showBackButton"] boolValue];
            videoAdPlayerController.showCountdown = [format[@"showCountdown"] boolValue];
            prerollParser = [[MMVideoAdParser alloc] initWithDelegate:self withFormat:format];
            
            if(iu.length > 0 && ![[iu substringFromIndex:iu.length - 1] isEqualToString:@"/"])
            {
                iu = [NSString stringWithFormat:@"%@/", iu];
            }
            // evaluate ad URL for params and expressions
            url = [MMAdUtil evalAdURL:url withParams:iu, nil];
            if ([self.delegate respondsToSelector:@selector(videoAd:willLoadUrl:)])
                [self.delegate videoAd:self willLoadUrl:url];
            
            // set request timeout (if none set default)
            NSInteger requestTimeout = videoConfiguration.requestTimeout > 0 ? videoConfiguration.requestTimeout : kVIDEO_REQUEST_TIMEOUT;
            [prerollParser parsePrerollAtURL:url withTimeout:requestTimeout];
        }
        else
        {
            [self videoWasCanceledByUser];
        }
    }
}

- (void) cancelLoad{
    [prerollParser cancelParser];
}

#pragma mark - MMVideoAdDelegate

- (void) videoWasCanceledByUser{
    [playbackPollTimer invalidate];
    
    [[NSNotificationCenter 	defaultCenter]
     removeObserver:self
     name:MPMoviePlayerPlaybackDidFinishNotification
     object:nil];
    
    [[NSNotificationCenter 	defaultCenter]
     removeObserver:self
     name:MPMoviePlayerLoadStateDidChangeNotification
     object:nil];
    
    [videoAdPlayerController.moviePlayer stop];
    
    [self onCompleteWithStatus:EVideoAdStatusSkipped];
}

#ifdef DEBUG

-(void)skipAd {
    [self onSuccessfulComplete];
}

#endif


#pragma mark - MMVideoParserDelegate

-(void) videoParserParsedSuccessfully:(VideoAdEntry *)videoEntry{
    parsedVideoAdEntry = videoEntry;
    [self start];
    
}

-(void) videoParserFailedParsing:(NSError*)error{
    [self onError];
}


#pragma mark
-(void)start{
    videoAdPlayerController.strClickURL = parsedVideoAdEntry.clickThrough;
    videoAdPlayerController.strMessage = parsedVideoAdEntry.clickableStripText;
    videoAdPlayerController.arrReportClick = parsedVideoAdEntry.reportClickArr;
    videoAdPlayerController.external = parsedVideoAdEntry.external;
    videoAdPlayerController.stripLocation = parsedVideoAdEntry.clickableStripLocation;
    videoAdPlayerController.supportedOrientations = self.supportedOrientations;
    
    // Register to receive a notification when the movie has finished playing.
    // TODO: move this to init, add object sending the notification
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:nil];
    [self sendMovieStats:@"impression"];
    
    [self playNextURL];
}


-(void)playNextURL{
    if ([parsedVideoAdEntry.fallbackURLArray count] > 0){
        
        
        videoAdPlayerController.moviePlayer.contentURL = [parsedVideoAdEntry.fallbackURLArray objectAtIndex:0];
        
        // May help to reduce latency
        [videoAdPlayerController.moviePlayer prepareToPlay];
        
        // Register that the load state changed (movie is ready)
        // TODO: move to init, ad object that sends the notification
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(moviePlayerLoadStateChanged:)
                                                     name:MPMoviePlayerLoadStateDidChangeNotification
                                                   object:nil];
    } else {
        [self onError];
    }
}

-(void) onError{
    
    [playbackPollTimer invalidate];
    playbackPollTimer = nil;
    
    if ([parsedVideoAdEntry.fallbackURLArray count] > 0)
        [parsedVideoAdEntry.fallbackURLArray removeObjectAtIndex:0];
    
    if ([parsedVideoAdEntry.fallbackURLArray count] == 0){
        [self sendMovieStats:@"error"];
        [self onCompleteWithStatus:EVideoAdStatusFailed];
    }
    else {
        [self playNextURL];
    }
}

- (void)onSuccessfulComplete {
    [self sendMovieStats:@"complete"];
    [self onCompleteWithStatus:EVideoAdStatusFinishedPlaying];
}

-(void)onCompleteWithStatus:(EVideoAdStatus)status{
    
    [playbackPollTimer invalidate];
    playbackPollTimer = nil;
    
    // Remove MPMoviePlayerPlaybackDidFinishNotification  observer
    [[NSNotificationCenter 	defaultCenter]
     removeObserver:self
     name:MPMoviePlayerPlaybackDidFinishNotification
     object:nil];
    isLoading = NO;
    
    // stoping and the moviePlayer, to make sure it doesn't influence other places in the application that are trying to play video.
    [videoAdPlayerController.moviePlayer stop];
    [self dismissTheVideoController];
    if ([self.delegate respondsToSelector:@selector(videoAdDismissed:withStatus:)]){
        [self.delegate videoAdDismissed:self withStatus:status];
    }
    videoAdPlayerController = nil;
    
    [MMAdUtil sendLinksFromArray:parsedVideoAdEntry.reportCompleteArr];
}

#pragma mark - notifications
- (void) moviePlayerLoadStateChanged:(NSNotification*)notification
{
    // Unless state is unknown, start playback
    if ([videoAdPlayerController.moviePlayer loadState] != MPMovieLoadStateUnknown)
    {
        // Remove loadstate did change observer
        [[NSNotificationCenter 	defaultCenter]
         removeObserver:self
         name:MPMoviePlayerLoadStateDidChangeNotification
         object:nil];
        
        // When tapping movie, status bar will appear, it shows up
        // in portrait mode by default. Set orientation to landscape
        [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationLandscapeRight animated:NO];
        [self displayTheVideoController];
        
        if (videoAdPlayerController.moviePlayer.duration != 0){
            playbackPollTimer = [NSTimer scheduledTimerWithTimeInterval:videoAdPlayerController.moviePlayer.duration / 4 target:self selector:@selector(pollAdTime) userInfo:nil repeats:YES];
        }
        
        if ([self.delegate respondsToSelector:@selector(videoAdDidLoad:withUrlString:)])
            [self.delegate videoAdDidLoad:self withUrlString:[[videoAdPlayerController.moviePlayer contentURL] absoluteString]];
        
        // Play the movie
        [videoAdPlayerController.moviePlayer play];
        videoAdPlayerController.continuePlaying = YES;
    }
    
    if ([videoAdPlayerController.moviePlayer loadState] == MPMovieLoadStatePlaythroughOK) {
        //self.isReport = YES;
        [MMAdUtil sendLinksFromArray:parsedVideoAdEntry.reportStartArr];
    }
}

- (void) moviePlayBackDidFinish:(NSNotification*)notification{
    
    videoAdPlayerController.continuePlaying = NO;
    
    if ([[notification userInfo] objectForKey:@"error"] != nil)
	{
		[self onError];
	}else{
        [self onSuccessfulComplete];
    }
}

#pragma mark - Display and Dismiss the Video Controller
- (void) displayTheVideoController {
    UIViewController *rootVC = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    UIViewController *vcToPresentOn = rootVC;
    
    if ([vcToPresentOn respondsToSelector:@selector(presentedViewController)]){
        while (vcToPresentOn.presentedViewController){
            vcToPresentOn = vcToPresentOn.presentedViewController;
        }
    }
    
    if ([vcToPresentOn respondsToSelector:@selector(presentViewController:animated:completion:)]){
        [vcToPresentOn presentViewController:videoAdPlayerController animated:NO completion:^{
            
        }];
    }
}

- (void) dismissTheVideoController {
    //    [UIApplication sharedApplication].statusBarHidden = statusBarWasHiddenInTheApp;
    //if ([videoAdPlayerController respondsToSelector:@selector(dismissViewControllerAnimated:completion:)])
    [videoAdPlayerController dismissViewControllerAnimated:NO completion:nil];
    
    //    }];
}


#pragma mark - Statistics
-(void) sendMovieStats:(NSString*)sKey{
    NSString * url = [parsedVideoAdEntry.trackingEventsDict objectForKey:sKey];
    if (url == nil)
        return;
    
    url = [url stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    url = [url stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];
    //    NSLog(@">>STATS Preroll movie play: %@", url);
    // call statistics service
	NSString * __unused statisticsResponse = [NSString stringWithContentsOfURL:[NSURL URLWithString:url]
                                                                      encoding:NSUTF8StringEncoding
                                                                         error:nil];
    AdLog(@"statisticsResponse: %@", statisticsResponse);
}

-(void) pollAdTime{
    if (videoAdPlayerController.moviePlayer.duration != 0){ // duration is known
        
        //NSLog(@"poll ad time, current time: %f", mp.currentPlaybackTime);
        
        int playbackQuater = lround(videoAdPlayerController.moviePlayer.currentPlaybackTime * 4.0 / videoAdPlayerController.moviePlayer.duration);
        
        //NSLog(@"poll ad time, quarter: %d", playbackQuater);
        
        switch (playbackQuater) {
            case 1:
                [self sendMovieStats:@"firstQuartile"];
                break;
            case 2:
                [self sendMovieStats:@"midpoint"];
                break;
            case 3:
                [self sendMovieStats:@"thirdQuartile"];
                break;
            default:
                break;
        }
    }
}




@end
