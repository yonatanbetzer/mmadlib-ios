//
//  MMVideoAd.h
//  MMAd
//
//  Copyright (c) 2013 Mako. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MMVideoConfiguration;
@class MMMoviePlayerViewController;

typedef enum  {
    EVideoAdStatusEmpty,
    EVideoAdStatusFinishedPlaying,
    EVideoAdStatusFailed,
    EVideoAdStatusSkipped
} EVideoAdStatus;

@class MMVideoAd;
@class VideoAdEntry;
@class MMVideoAdParser;

@protocol MMVideoAdDelegate <NSObject>
@optional

- (void) videoAd:(MMVideoAd *)videoAd willLoadUrl:(NSString*) urlString;
- (void) videoAdDidLoad:(MMVideoAd *)videoAd withUrlString:(NSString*)urlString;
//- (void) videoAdFailedLoading:(MMVideoAd *)videoAd withError:(NSError *)error;

//- (void) videoAdFinishedPlaying:(MMVideoAd *)videoAd ;
//- (void) videoAdFailedPlaying:(MMVideoAd *)videoAd withError:(NSError *)error;

- (void) videoAdDismissed:(MMVideoAd*)videoAd withStatus:(EVideoAdStatus)status;
- (void) loadAndPlayWithMakoCat:(NSDictionary*)makoCatDFP;

@end


@interface MMVideoAd : NSObject  {
    MMMoviePlayerViewController *videoAdPlayerController;
    MMVideoConfiguration *videoConfiguration;
    BOOL isLoading;
    BOOL statusBarWasHiddenInTheApp;
    
    
    MMVideoAdParser *prerollParser;
    VideoAdEntry *parsedVideoAdEntry;
    NSTimer *playbackPollTimer;

    BOOL readyToPlay;
}

@property (nonatomic, weak) id <MMVideoAdDelegate> delegate;
@property (nonatomic) NSUInteger supportedOrientations;
@property (nonatomic, strong) NSString *areaId;
@property (nonatomic, strong) NSString *category;

- (void) loadAndPlayWithMakoCat:(NSDictionary*)makoCatDFP;

- (void) loadAndPlayWithVcmId:(NSString *)vcmId
               videoChannelId:(NSString *)videoChannelId
             galleryChannelId:(NSString *)galleryChannelId;

// this can cancel the load of the video. in case the video started playing, a "back" button will appear that will allow to cancel the playing of the video via the MMMoviePlayerViewController.
- (void) cancelLoad; // not in use right now, because if the cancel is while trying to present the MMMoviePlayerViewController, and the application is dismissing the current modal, it causes a crash.


@end
