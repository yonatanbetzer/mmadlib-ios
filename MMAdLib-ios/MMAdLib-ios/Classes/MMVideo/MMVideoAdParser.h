//
//  PrerollParser.h
//  Mako
//
//  Copyright (c) 2013 Mako. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MMVideoConfiguration.h"

#import "MMAdUtil.h"


@interface VideoAdEntry : NSObject{

}
@property (nonatomic) BOOL canSkip;
@property (nonatomic) BOOL seekable;
@property (nonatomic, copy) NSString * entryType;
@property (nonatomic, copy) NSMutableArray * fallbackURLArray;
@property (nonatomic, strong) NSMutableDictionary * trackingEventsDict;
@property (nonatomic, strong) NSString *clickableStripText;
@property (nonatomic) ClickableStripLocation clickableStripLocation;
@property (nonatomic, copy) NSString *clickThrough;
@property (nonatomic) BOOL external;
@property (nonatomic, copy) NSString *reportClick;

@property (nonatomic, strong) NSMutableArray *reportStartArr;
@property (nonatomic, strong) NSMutableArray *reportClickArr;
@property (nonatomic, strong) NSMutableArray *reportCompleteArr;

@end




@protocol MMVideoParserDelegate <NSObject>
@required

-(void) videoParserParsedSuccessfully:(VideoAdEntry *)videoEntry;
// if there was not target, return MMAdEmptyError;
// domain com.keshet.mmadlib error code: kMMAdEmptyFileError 1000
-(void) videoParserFailedParsing:(NSError*)error;


@end

@interface MMVideoAdParser : NSObject {
    NSMutableData * _rawData;
    NSURLConnection* _connection;
    NSDictionary *format;

    NSMutableArray *reportStartArr;
    NSMutableArray *reportClickArr;
    NSMutableArray *reportCompleteArr;
    
    BOOL delegateWasMessaged;
}

@property (nonatomic, weak) id<MMVideoParserDelegate> delegate;


-(id) initWithDelegate:(id<MMVideoParserDelegate>)aDelegate withFormat:(NSDictionary*)formatParam;
- (void)parsePrerollAtURL:(NSString *)URL withTimeout:(NSInteger)requestTimeout;

- (void) cancelParser;
@end
