//
//  SMSHelper.m
//  MakoTV2
//
//  Created by Ran Greenberg on 3/13/14.
//  Copyright (c) 2014 Tomer Har Yoffi. All rights reserved.
//

#import "MMAdSMSHelper.h"

@implementation MMAdSMSHelper


-(BOOL)handleUrlString:(NSString*)urlString viewController:(UIViewController*)viewController {
    
    if ([urlString hasPrefix:@"sms:"]) {
        
        MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
        if([MFMessageComposeViewController canSendText])
        {
            // decode the url string
            urlString = [urlString stringByReplacingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
            
            NSRange prefixRange = [urlString rangeOfString:@"sms:"];
            urlString = [urlString substringFromIndex:prefixRange.length];
            NSArray* params = [urlString componentsSeparatedByString:@";body="];
            NSString* phoneNumber = nil;
            NSString* bodyString = nil;
            
            if ([params count] > 0)
                phoneNumber = [params objectAtIndex:0];
            if ([params count] > 1)
                bodyString = [params objectAtIndex:1];
            
            controller.body = bodyString;
            controller.recipients = [NSArray arrayWithObject:phoneNumber];
            controller.messageComposeDelegate = self;
            [viewController presentViewController:controller animated:YES completion:nil];
        }
        
        else { // device can't send sms
            
            
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil
                                                            message:@"לא ניתן לשלוח הודעה ממכשיר זה"
                                                           delegate:nil
                                                  cancelButtonTitle:@"אישור"
                                                  otherButtonTitles:nil];
            [alert show];
                                                                                                                                                                               
        }
        return YES;
    }
    return NO;
}

-(void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result {
    
    if (result == MessageComposeResultSent) {
//        klogdbg_func_arg1s(@"SMS sent!")
    }
    
    [controller dismissViewControllerAnimated:YES completion:nil];
}

@end
