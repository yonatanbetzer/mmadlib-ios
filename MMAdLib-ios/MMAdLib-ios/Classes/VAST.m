//
//  VAST.m
//  MMAdLib-ios
//
//  Created by Yonatan Betzer on 10/26/14.
//  Copyright (c) 2014 mako. All rights reserved.
//

#import "VAST.h"
#import "VASTAd.h"

@implementation VAST

+(void)fromUrl:(NSString*)VASTURL completion:(VASTParserComplete)completion
{
    __block int wrapperCount = 0;
    VASTURL = [VASTURL stringByReplacingOccurrencesOfString:@"[timestamp]" withString:[NSString stringWithFormat:@"%.0f", [[NSDate date] timeIntervalSince1970]]];
    NSURL *url = [NSURL URLWithString:VASTURL];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [AFXMLRequestOperation addAcceptableContentTypes:[NSSet setWithObject:@"text/xml"]];
    
    AFXMLRequestOperation *operation = [AFXMLRequestOperation XMLParserRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, NSXMLParser *XMLParser) {
        NSDictionary *dictionary = [NSDictionary dictionaryWithXMLParser:XMLParser];

        VAST * currentVAST = [VAST fromDictionary:dictionary];
        
        for(VASTAd *ad in currentVAST.ads)
        {
            if(ad.wrapperAd)
            {
                wrapperCount++;
                [VAST fromUrl:ad.wrapperAd.vastAdTagURL
                   completion:^(VAST *vast) {
                       wrapperCount--;
                       if(vast)
                       {
                           ad.wrapperAd.redirectVast = vast;
                           [ad.trackableChildren addObject:vast];
                       }
                       if(wrapperCount == 0)
                       {
                           completion(currentVAST);
                       }
                       
                   }
                 ];
            }
        }
        if(wrapperCount == 0)
        {
            completion(currentVAST);
        }
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error, NSXMLParser *XMLParser) {
        
        completion(nil);
        
    }];
    [operation start];
}

+(VAST*)fromDictionary:(NSDictionary*)dictionary
{
    VAST *result = [[VAST alloc] init];
    result.ads = [VASTAd fromArray:dictionary[@"Ad"]];
    [result parseTracking:dictionary];
    
    result.trackableChildren = [NSMutableArray array];
    for (VASTTrackable *trackable in result.ads) {
        [result.trackableChildren addObject:trackable];
    }
    return result;
}

-(NSURL*)playableUrl
{
    for(VASTAd *ad in self.ads)
    {
        NSURL *url = [ad playableUrl];
        if(url)
        {
            self.isActive = YES;
            self.skipOffset = ad.skipOffset;
            return url;
        }
    }
    return nil;
}

-(NSURL*)clickThroughUrl
{
    for(VASTAd *ad in self.ads)
    {
        if(ad.isActive)
        {
            return [ad clickThroughUrl];
        }
    }
    return nil;
}

@end
