//
//  MMBannerView.h
//  MMAd
//
//  Copyright (c) 2013 Mako. All rights reserved.
//


#import <MessageUI/MessageUI.h>
#import "GADBannerView.h"

@class MMBannerView;

@protocol MMBannerViewDelegate <NSObject>


@optional
- (void)bannerViewWillLoadAd:(MMBannerView *)banner; // Not implemented
- (void)bannerViewDidLoadAd:(MMBannerView *)banner;
- (void)bannerView:(MMBannerView *)banner didFailToReceiveAdWithError:(NSError *)error;
- (BOOL)bannerViewActionShouldBegin:(MMBannerView *)banner willLeaveApplication:(BOOL)willLeave;
- (void)bannerViewActionDidFinish:(MMBannerView *)banner;
@end


@interface MMBannerView : UIView

@property (strong) NSString *bannerId;
@property (nonatomic) BOOL isLoading;
@property (nonatomic, weak) id<MMBannerViewDelegate> delegate;
@property (nonatomic, readonly, getter=isBannerLoaded) BOOL bannerLoaded;
@property (nonatomic) NSUInteger supportedOrientations;

-(void)loadBannerWithMakoCatDFPDict:(NSDictionary*)catDFPDict
                        replaceDict:(NSDictionary *)appStateDict
                           formatID:(NSString*)formatIDString;

@end
