//
//  MMBannerView.m
//  MMAd
//
//  Copyright (c) 2013 Mako. All rights reserved.
//

#import "MMBannerView.h"
#import "MMAdManager.h"
#import "MMAdUtil.h"
//#import <MakoLocation/AFNetworking.h>
#import "GADBannerView.h"
#import "MMParameterStringReplacement.h"
#import "DFPExtras.h"

@interface MMBannerView () <UIWebViewDelegate, GADBannerViewDelegate>

@property (nonatomic, strong) NSDictionary *appStateDict;

@end


@implementation MMBannerView
{
    GADBannerView *bannerView;
    UIButton *btnClose;
}

- (void) commonInit:(CGSize)size {
    bannerView = [[GADBannerView alloc] initWithAdSize:GADAdSizeFromCGSize(size)];
    bannerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    bannerView.delegate = self;
    [self addSubview:bannerView];
    bannerView.rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    self.backgroundColor = [UIColor clearColor];
    self.supportedOrientations = IS_DEVICE_IPAD() ? UIInterfaceOrientationMaskLandscape : UIInterfaceOrientationMaskPortrait;
}

-(void)loadBannerWithMakoCatDFPDict:(NSDictionary*)catDFPDict
                        replaceDict:(NSDictionary *)appStateDict
                           formatID:(NSString*)formatIDString {
    if(!catDFPDict)
    {
        catDFPDict = [[MMAdManager getInstance] defaultMakoCatDFP];
    }
    
    NSString *iu = catDFPDict[@"iu"];
    NSDictionary *format = [[MMAdManager getInstance] formatDictionaryForIU:iu formatID:formatIDString];
    NSDictionary *config = [[MMAdManager getInstance] adAreaConfigForForIU:iu];
    if(config && config[@"force_replace_iu"])
    {
        iu = config[@"force_replace_iu"];
    }
    if([MMAdManager getInstance].advertising_tree_force_replace)
    {
        iu = [MMAdUtil stringByReplacingTreeOnIU:iu];
    }
    
    if(iu.length > 0 && [[iu substringFromIndex:iu.length - 1] isEqualToString:@"/"])
    {
        iu = [iu substringToIndex:iu.length - 1];
    }
    NSDictionary *makoCatDFPDictExtration = [MMAdUtil extractMakoCatDFPDictionary:catDFPDict];
    NSDictionary *makoCatDFP = [[[MMParameterStringReplacement alloc] init] replaceVariablesInCustParams:makoCatDFPDictExtration];
    
    NSString *widthString = [format objectForKey:@"width"];
    NSString *heightString = [format objectForKey:@"height"];
    
    if (format.count && widthString.length > 0 && heightString.length > 0) {
        CGSize bannerSize = CGSizeMake(widthString.floatValue, heightString.floatValue);
        [self commonInit:bannerSize];
        self.autoresizesSubviews = NO;
        self.frame = CGRectMake((self.frame.size.width - bannerSize.width) / 2, self.frame.origin.y, bannerSize.width, bannerSize.height);
        bannerView.adUnitID = [NSString stringWithFormat:@"%@/%@", iu, [format objectForKey:@"name"]];
        [bannerView loadRequest:[MMAdUtil createRequestWithDict:makoCatDFP]];
        
        if([format[@"show_closing_x"] isEqualToString:@"true"])
        {
            btnClose = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, bannerSize.height)];
            [btnClose addTarget:self action:@selector(closeBanner) forControlEvents:UIControlEventTouchUpInside];
        }
    }
    else
    {
        @synchronized(self){
            self.isLoading = NO;
        }
        // tell the delegate we failed.
        if ([_delegate respondsToSelector:@selector(bannerView:didFailToReceiveAdWithError:)])
            [_delegate bannerView:self didFailToReceiveAdWithError:nil];
    }
}

-(void)closeBanner
{
    [bannerView removeFromSuperview];
    bannerView = nil;
    [self adView:bannerView didFailToReceiveAdWithError:nil];
}

#pragma mark - GADBannerViewDelegate
- (void)adViewDidReceiveAd:(GADBannerView *)view {
    @synchronized(self){
        self.isLoading = NO;
        _bannerLoaded = YES;
        self.hidden = NO;
        [self performSelector:@selector(addCloseButton) withObject:nil afterDelay:7];
    }
    
    if ([_delegate respondsToSelector:@selector(bannerViewDidLoadAd:)])
        [_delegate bannerViewDidLoadAd:self];
}

-(void)addCloseButton
{
    if(btnClose)
    {
        [self addSubview:btnClose];
    }
}


- (void)adView:(GADBannerView *)view didFailToReceiveAdWithError:(GADRequestError *)error{
    @synchronized(self){
        self.isLoading = NO;
        self.hidden = YES;
    }
    
    // tell the delegate we failed.
    if ([_delegate respondsToSelector:@selector(bannerView:didFailToReceiveAdWithError:)])
        [_delegate bannerView:self didFailToReceiveAdWithError:error];
}



@end

