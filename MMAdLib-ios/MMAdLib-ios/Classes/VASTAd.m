//
//  VASTAd.m
//  MMAdLib-ios
//
//  Created by Yonatan Betzer on 10/26/14.
//  Copyright (c) 2014 mako. All rights reserved.
//

#import "VASTAd.h"

@implementation VASTAd

+(VASTAd*)fromDictionary:(NSDictionary*)dictionary
{
    VASTAd *ad = [[VASTAd alloc] init];
    ad.adId = dictionary[@"_id"];
    ad.sequence = [dictionary[@"_sequence"] integerValue];
    ad.inlineAd = [VASTInline fromDictionary:dictionary[@"InLine"]];
    ad.wrapperAd = [VASTWrapper fromDictionary:dictionary[@"Wrapper"]];
    [ad parseTracking:dictionary];
    
    ad.trackableChildren = [NSMutableArray array];
    if(ad.inlineAd)
    {
        [ad.trackableChildren addObject:ad.inlineAd];
    }
    if(ad.wrapperAd)
    {
        [ad.trackableChildren addObject:ad.wrapperAd];
    }
    return ad;
}

+(NSMutableArray*)fromArray:(NSArray*)array
{
    if([array isKindOfClass:[NSDictionary class]])
    {
        array = @[array];
    }
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    for(NSDictionary *dic in array)
    {
        VASTAd *ad = [VASTAd fromDictionary:dic];
        [result addObject:ad];
    }
    
    return result;
}

-(NSURL*)playableUrl
{
    if(self.inlineAd)
    {
        NSURL *url = [self.inlineAd playableUrl];
        if(url)
        {
            self.isActive = YES;
            self.skipOffset = self.inlineAd.skipOffset;
            return url;
        }
    }
    else
    {
        NSURL *url = [self.wrapperAd playableUrl];
        if(url)
        {
            self.isActive = YES;
            self.skipOffset = self.inlineAd.skipOffset;
            return url;
        }
    }
    return nil;
}

-(NSURL*)clickThroughUrl
{
    if(self.isActive)
    {
        if(self.inlineAd.isActive)
        {
            return [self.inlineAd clickThroughUrl];
        }
        else
        {
            if(self.wrapperAd.isActive)
            {
                return [self.wrapperAd clickThroughUrl];
            }
        }
    }
    return nil;
}

@end
