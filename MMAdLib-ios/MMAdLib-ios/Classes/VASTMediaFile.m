//
//  VASTMediaFile.m
//  MMAdLib-ios
//
//  Created by Yonatan Betzer on 10/26/14.
//  Copyright (c) 2014 mako. All rights reserved.
//

#import "VASTMediaFile.h"

@implementation VASTMediaFile


+(VASTMediaFile*)fromDictionary:(NSDictionary*)dictionary
{
    VASTMediaFile *result = [[VASTMediaFile alloc] init];
    result.url = dictionary[@"__text"];
    result.delivery = dictionary[@"_delivery"];
    result.type = dictionary[@"_type"];
    result.width = [dictionary[@"_width"] floatValue];
    result.height = [dictionary[@"_height"] floatValue];
    return result;
}

+(NSMutableArray*)fromArray:(NSArray*)array
{
    if([array isKindOfClass:[NSDictionary class]])
    {
        array = @[array];
    }
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    for (NSDictionary *dic in array) {
        NSDictionary *dictionary = dic[@"MediaFile"];
        if([dictionary isKindOfClass:[NSDictionary class]])
        {
            VASTMediaFile *item = [VASTMediaFile fromDictionary:dictionary];
            [result addObject:item];
        }
        else if ([dictionary isKindOfClass:[NSArray class]])
        {
            NSArray *innerArray = (NSArray *)dictionary;
            for (NSDictionary *dic2 in innerArray) {
                VASTMediaFile *item = [VASTMediaFile fromDictionary:dic2];
                [result addObject:item];
            }
        }
        
    }
    return result;
}

-(NSURL*)playableUrl
{
    if(self.url)
    {
        return [NSURL URLWithString:self.url];
    }
    return nil;
}

@end
