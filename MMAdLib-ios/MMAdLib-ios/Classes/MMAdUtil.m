//
//  MMAdUtil.m
//  MMAd
//
//  Copyright (c) 2013 Mako. All rights reserved.
//

#import "MMAdUtil.h"
//#import <MakoLocation/AFNetworking.h>
#import <QuartzCore/QuartzCore.h>

#import "DFPExtras.h"

@implementation MMAdUtil

void AdLog(NSString *format, ...) {
#if (ENABLE_CONSOLE_LOG)
    va_list argumentList;
    va_start(argumentList, format);
    NSMutableString * message = [[NSMutableString alloc] initWithFormat:format arguments:argumentList];
    
    [message insertString:@"AdLog>> " atIndex:0];
    
    //    NSLogv(message, argumentList); // Originally NSLog is a wrapper around NSLogv.
    va_end(argumentList);
    
#endif
}


+ (UIImage*)screenshot
{
    // Create a graphics context with the target size
    // On iOS 4 and later, use UIGraphicsBeginImageContextWithOptions to take the scale into consideration
    // On iOS prior to 4, fall back to use UIGraphicsBeginImageContext
    CGSize imageSize = [[UIScreen mainScreen] bounds].size;
    if (NULL != UIGraphicsBeginImageContextWithOptions)
        UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0);
    else
        UIGraphicsBeginImageContext(imageSize);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Iterate over every window from back to front
    for (UIWindow *window in [[UIApplication sharedApplication] windows])
    {
        if (![window respondsToSelector:@selector(screen)] || [window screen] == [UIScreen mainScreen])
        {
            // -renderInContext: renders in the coordinate space of the layer,
            // so we must first apply the layer's geometry to the graphics context
            CGContextSaveGState(context);
            // Center the context around the window's anchor point
            CGContextTranslateCTM(context, [window center].x, [window center].y);
            // Apply the window's transform about the anchor point
            CGContextConcatCTM(context, [window transform]);
            // Offset by the portion of the bounds left of and above the anchor point
            CGContextTranslateCTM(context,
                                  -[window bounds].size.width * [[window layer] anchorPoint].x,
                                  -[window bounds].size.height * [[window layer] anchorPoint].y);
            
            // Render the layer hierarchy to the current context
            [[window layer] renderInContext:context];
            
            // Restore the context
            CGContextRestoreGState(context);
        }
    }
    
    // Retrieve the screenshot image
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return image;
}

+ (NSString*)getAdPlayUrl:(NSString*)htmlContent{
    if ([htmlContent rangeOfString:@"adPlayUrl"].location != NSNotFound){
        // get adplay url string
        NSError *error = NULL;
        NSRegularExpression * containerTypeRegExp = [NSRegularExpression regularExpressionWithPattern:@"<!--adPlayUrl(.*?)-->"
                                                                                              options:NSRegularExpressionCaseInsensitive | NSRegularExpressionDotMatchesLineSeparators
                                                                                                error:&error];
        NSArray * containerMatches = [containerTypeRegExp matchesInString:htmlContent options:NSMatchingReportCompletion range:NSMakeRange(0, [htmlContent length])];
        if ([containerMatches count] > 0) {
            NSTextCheckingResult *cotainerMatch = [containerMatches objectAtIndex:0];
            NSString *containerContentString = [htmlContent substringWithRange:cotainerMatch.range];
            //NSLog(@"Found adplay url with contents :%@", containerContentString);
            
            NSArray * strArr = [containerContentString componentsSeparatedByString: @"\""];
            
            if ([strArr count] > 2){
                return[strArr objectAtIndex:1];
            }
        }
    }
    return nil;
}

+(NSMutableArray*) createFallbackList:(NSString*)url WithVideoFallbackParam:(NSString*)aFallbackParam WithFallbackListDelimiter:(NSString*)aListDelimiter{
    // handle castup server fallback
    NSRange castupFallbackParam = [url rangeOfString: aFallbackParam]; // check if URL is a fallback servers list (by param)
    
    NSMutableArray * castupServerURLArray = [NSMutableArray arrayWithCapacity:1];	// server list
    
    // check if URL is has fallback servers list (if it contains the videoFallbackParam)
    if (castupFallbackParam.location != NSNotFound)
    {
        // synchroniously get server list
        NSString * castupServerListString = [NSString stringWithContentsOfURL: [NSURL URLWithString: url] encoding: NSUTF8StringEncoding error: nil];
        if (castupServerListString == nil)
            return nil;	// response error - can't play movie
        
        NSArray * castupServerURLStrings = [castupServerListString componentsSeparatedByCharactersInSet: [NSCharacterSet characterSetWithCharactersInString: aListDelimiter]];
        for (NSString * movieURLString in castupServerURLStrings)
            // add URL (remove white space and add escape for special chars)
            [castupServerURLArray addObject: [NSURL URLWithString: [movieURLString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] ]];
    }
    else	// no fallback scheme, add URL only
        [castupServerURLArray addObject: [NSURL URLWithString: [url stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]]];
    
    return castupServerURLArray;
}

+ (void) sendLinksFromArray:(NSArray*)linksArr {
    for (NSString *strUrl in linksArr){
        NSURL *url = [NSURL URLWithString:strUrl];
        NSURLRequest *request = [NSURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:kAD_MANAGER_LOADING_TIMEOUT];
        AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
        [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *op, id responseObject){
        }
                                         failure:^(AFHTTPRequestOperation *op, NSError *error){
                                         }];
        [operation start];
    }
    
}

+ (NSString*)evalAdURL:(NSString*)adURL withParams:(NSString*)param1,... {
    
    if (!adURL) return nil; // illegal param
    
    NSString * evalURL = [adURL copy];
    
    /////////////////////////////////////////////
    // replace params
    /////////////////////////////////////////////
    va_list args;
    va_start(args, param1);
    
    // go over given arguments and replace in string
    for (NSString * arg = param1; arg != nil; arg = va_arg(args, NSString*)){
        NSRange varPosition = [evalURL rangeOfString:@"$$"];
        // check if var place holder found
        if (varPosition.location != NSNotFound) {
            // replace var place holder with given arg
            evalURL = [evalURL stringByReplacingCharactersInRange:varPosition withString:arg];
        }
    }
    
    va_end(args);
    
    evalURL = [[[MMParameterStringReplacement alloc] init] replaceVariableInString:evalURL];
    return evalURL;
}


// example: fullPath=mako.mako-tv.%CAT_NAME% varPath=programs.kids
// a dynamic category path is a category path with expecting a category in the format of BASE_PATH.VARIABLE_PATH
+ (NSString*)evalMultiExp:(NSDictionary*)expParams {
    
    NSString * retMultiPath;
    
    // get parameters
    NSString * basePath = [expParams objectForKey:@"base"];
    NSString * varPath = [expParams objectForKey:@"var"];
    NSString * defaultPath = [expParams objectForKey:@"default"];
    
    if (basePath && varPath && defaultPath) {
        retMultiPath = [NSString stringWithFormat:@"%@.%@.%@%@", basePath, varPath, defaultPath, [MMAdUtil buildMultiWithBasePath:basePath varPath:varPath defaultPath:defaultPath]];
    }else{
        retMultiPath = [NSString stringWithFormat:@"%@.%@", basePath, defaultPath];
    }
    return retMultiPath;
}

+ (NSString*) buildMultiWithBasePath:(NSString*)basePath varPath:(NSString*)varPath defaultPath:(NSString*)defaultPath {
    // find last location of "." in the category
    NSRange range = [varPath rangeOfString:@"." options:NSBackwardsSearch];
    // check if ". found
    if(range.location != NSNotFound) {
        // get sub category from path
        NSString *subCategory = [varPath substringToIndex:range.location];
        // concatenate to current path
        return [NSString stringWithFormat:@",%@.%@.%@%@", basePath, subCategory, defaultPath, [MMAdUtil buildMultiWithBasePath:basePath varPath:subCategory defaultPath:defaultPath]];
    }else{
        //mobile.mako-tv.general
        return [NSString stringWithFormat:@",%@.%@", basePath, defaultPath];
    }
}

+ (NSDictionary*)dictionaryFromString:(NSString*)str pairsSeperator:(NSString*)pairsSeperator valueSeperator:(NSString*)valueSeperator {
    
    NSMutableDictionary * dictionray = [NSMutableDictionary dictionary];
    
    // go through param pairs
    for (NSString *paramPairStr in [str componentsSeparatedByString:pairsSeperator]) {
        // get param pair into array
        NSArray *paramPairArr = [paramPairStr componentsSeparatedByString:valueSeperator];
        if ([paramPairArr count] > 1) {
            [dictionray setObject:paramPairArr[1] forKey:paramPairArr[0]];
        }
    }
    
    return dictionray;
}

+ (NSDictionary*)getArgsFromURL:(NSURL*)url{
    NSString *query = [url.query lowercaseString];
    NSArray *argsArr = [query componentsSeparatedByString:@"&"];
    
    NSMutableDictionary *argsDic = [NSMutableDictionary dictionaryWithCapacity:[argsArr count]];
    
    for (NSString *arg in argsArr){
        NSArray *argArr = [arg componentsSeparatedByString:@"="];
        if ([argArr count] == 2){
            NSString *key = [argArr objectAtIndex:0];
            NSString *val = [argArr objectAtIndex:1];
            [argsDic setObject:val forKey:key];
        }
    }
    
    return argsDic;
}

+ (NSURL*) urlFromStringWithWhiteSpaces:(NSString*)stringWithWhiteSpaces{
    NSString *trimmedStr = [stringWithWhiteSpaces stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *encodedStr = trimmedStr;
    NSURL *url = [NSURL URLWithString:encodedStr];
    return url;
}

+(NSDictionary*)extrcaMakoCatDFPMakoTV:(NSDictionary*)data
{
    NSDictionary *dataToExtractDict = data;
    
    //        NSMutableDictionary *makoCatDFPDict = [[NSMutableDictionary alloc] init];
    
    id rootDict = [dataToExtractDict objectForKey:@"root"];
    NSString *reserveIU = @"";
    if (rootDict && [rootDict isKindOfClass:[NSDictionary class]]) {
        rootDict = (NSDictionary*)rootDict;
        
        id breadCrumbArray = [rootDict objectForKey:@"breadcrumb"];
        
        if (breadCrumbArray && [breadCrumbArray isKindOfClass:[NSArray class]]) {
            breadCrumbArray = (NSArray*)breadCrumbArray;
            
            for (id tmpID in breadCrumbArray) {
                if ([tmpID isKindOfClass:[NSDictionary class]]) {
                    NSDictionary *tmpDict = (NSDictionary*)tmpID;
                    
                    id potentailMakoCatDFPDict = [tmpDict objectForKey:@"makoCatDFP"];
                    
                    if (potentailMakoCatDFPDict && [potentailMakoCatDFPDict isKindOfClass:[NSDictionary class]]) {
                        NSDictionary *makoCatDFPDict = (NSDictionary*)potentailMakoCatDFPDict;
                        return makoCatDFPDict;
                    }
                    
                    NSString *banner_cat = [tmpDict objectForKey:@"bannerCat"];
                    if(banner_cat.length > 0)
                    {
                        reserveIU = banner_cat;
                    }
                }
            }
        }
    }
    return @{@"iu":reserveIU};
}


+(NSDictionary*)extractMakoCatDFPDictionary:(id)dataToExtract {
    if ([dataToExtract isKindOfClass:[NSDictionary class]]) {
        NSDictionary *data = (NSDictionary*)dataToExtract;
        
        if(dataToExtract[@"root"])
        {
            [MMAdManager getInstance].lastContext = dataToExtract;
            return [MMAdUtil extrcaMakoCatDFPMakoTV:data];
        }
        else
        {
            return data;
        }
    }
    return nil;
}

+ (NSString*)stringByReplacingTreeOnIU:(NSString*)iu
{
    if([MMAdManager getInstance].advertising_tree_force_replace)
    {
        iu = [iu stringByReplacingOccurrencesOfString:@"mako_mobile" withString:[MMAdManager getInstance].advertising_tree_force_replace];
        iu = [iu stringByReplacingOccurrencesOfString:@"makoTV" withString:[MMAdManager getInstance].advertising_tree_force_replace];
        iu = [iu stringByReplacingOccurrencesOfString:@"Ochel_Tov" withString:[MMAdManager getInstance].advertising_tree_force_replace];
        iu = [iu stringByReplacingOccurrencesOfString:@"mako" withString:[MMAdManager getInstance].advertising_tree_force_replace];
    }
    return iu;
}

+ (GADRequest *)createRequestWithDict:(NSDictionary*)dict {
    
    GADRequest *request = [GADRequest request];
    
    NSDictionary *castParams = [dict objectForKey:@"cust_params"];
    
    castParams = [MMAdUtil dictionaryWithNonEmptyValuesForDictionary:castParams];
    
    
    if (castParams && castParams.count > 0) {
        
        // Don't use autorelease if you are using ARC in your project
        DFPExtras *extras = [[DFPExtras alloc] init];
        extras.additionalParameters = castParams;
        [request registerAdNetworkExtras:extras];
    }
    
    return request;
}

+(NSDictionary*)dictionaryWithNonEmptyValuesForDictionary:(NSDictionary*)dict {
    
    NSMutableDictionary *ansDic = [[NSMutableDictionary alloc] init];
    
    for (NSString *key in dict) {
        
        id value = [dict objectForKey:key];
        if ([value isKindOfClass:[NSString class]]) {
            NSString *valueString = (NSString*)value;
            
            if (valueString && valueString.length > 0) {
                [ansDic setValue:valueString forKey:key];
            }
        }
        if([value isKindOfClass:[NSArray class]])
        {
            [ansDic setValue:value forKey:key];
        }
        
    }
    return ansDic;
    
}




@end

