//
//  VASTCreatives.h
//  MMAdLib-ios
//
//  Created by Yonatan Betzer on 10/26/14.
//  Copyright (c) 2014 mako. All rights reserved.
//

#import "VASTTrackable.h"

typedef enum {
    kMFCreativeTypeLinear,
    kMFCreativeTypeNonLinear,
    kMFCreativeTypeCompanionAds
} MFCreativeType;


@interface VASTCreative : VASTTrackable

@property MFCreativeType type;

+(id)fromDictionary:(NSDictionary*)dictionary;
+(NSMutableArray*)fromArray:(NSArray*)array;

@end
