//
//  VASTVideoClick.m
//  MMAdLib-ios
//
//  Created by Yonatan Betzer on 10/26/14.
//  Copyright (c) 2014 mako. All rights reserved.
//

#import "VASTVideoClick.h"

@implementation VASTVideoClick


+(VASTVideoClick*)fromDictionary:(NSDictionary*)dictionary
{
    VASTVideoClick *result = [[VASTVideoClick alloc] init];
    NSArray *clickThrough = dictionary[@"ClickThrough"];
    NSArray *clickTracking = dictionary[@"ClickTracking"];
    NSArray *customClick = dictionary[@"CustomClick"];
    
    result.clickThrough = [VASTTrackable makeArrayOfValues:clickThrough];
    result.clickTracking = [VASTTrackable makeArrayOfValues:clickTracking];
    result.customClick = [VASTTrackable makeArrayOfValues:customClick];
    
    [result parseTracking:dictionary];
    return result;
}

+(NSMutableArray*)fromArray:(NSArray*)array
{
    if([array isKindOfClass:[NSDictionary class]])
    {
        array = @[array];
    }
    NSMutableArray *result = [[NSMutableArray alloc] init];
    for (NSDictionary *dic in array) {
        VASTVideoClick *item = [VASTVideoClick fromDictionary:dic];
        [result addObject:item];
    }
    return result;
}

@end
