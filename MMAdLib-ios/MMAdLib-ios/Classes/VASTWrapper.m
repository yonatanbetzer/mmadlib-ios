//
//  VASTWrapper.m
//  MMAdLib-ios
//
//  Created by Yonatan Betzer on 10/26/14.
//  Copyright (c) 2014 mako. All rights reserved.
//

#import "VASTWrapper.h"
#import "VAST.h"
#import "VASTCreativeLinear.h"

@implementation VASTWrapper

+(VASTWrapper*)fromDictionary:(NSDictionary*)vastWrapperDictionary
{
    if(vastWrapperDictionary)
    {
        VASTWrapper *result = [[VASTWrapper alloc] init];
        [result parseTracking:vastWrapperDictionary];
        result.linearCreatives = [VASTCreativeLinear fromArray:vastWrapperDictionary[@"Creatives"]];
        result.vastAdTagURL = vastWrapperDictionary[@"VASTAdTagURI"];
        
        result.trackableChildren = [NSMutableArray array];
        for (VASTCreativeLinear *creative in result.linearCreatives) {
            if(creative)
            {
                [result.trackableChildren addObject:creative];
            }
        }
        return result;
    }
    return nil;
}

-(NSURL*)playableUrl
{
    NSURL *url = [self.redirectVast playableUrl];
    if(url)
    {
        self.isActive = YES;
        for (VASTCreativeLinear *creative in self.linearCreatives) {
            creative.isActive = YES;
            self.skipOffset = creative.skipOffset;
            for(VASTTrackable *videoClick in creative.videoClicks)
            {
                videoClick.isActive = YES;
            }
        }
        return url;
    }
    return nil;
}

-(NSURL*)clickThroughUrl
{
    if(self.isActive)
    {
        return [self.redirectVast clickThroughUrl];
    }
    return nil;
}
@end
