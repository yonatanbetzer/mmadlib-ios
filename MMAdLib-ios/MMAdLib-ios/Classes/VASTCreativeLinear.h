//
//  VASTCreativeLinear.h
//  MMAdLib-ios
//
//  Created by Yonatan Betzer on 10/26/14.
//  Copyright (c) 2014 mako. All rights reserved.
//

#import "VASTCreative.h"


@interface VASTCreativeLinear : VASTCreative

@property (nonatomic) NSTimeInterval duration;
@property (nonatomic, strong) NSArray *mediaFiles;
@property (nonatomic, strong) NSArray *videoClicks;
@property (nonatomic, strong) NSArray *icons;

-(NSURL*)playableUrl;
-(NSURL*)clickThroughUrl;
@end
