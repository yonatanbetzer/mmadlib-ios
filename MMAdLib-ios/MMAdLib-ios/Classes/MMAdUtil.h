//
//  MMAdUtil.h
//  MMAd
//
//  Copyright (c) 2013 Mako. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "GADBannerView.h"


#define IS_DEVICE_IPAD() (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

#define kAD_MANAGER_LOADING_TIMEOUT 10 // seconds

#define kBANNER_MIN_REFRESH_RATE 5  //  seconds
#define kBANNER_ANIMATION_DURATION 0.75 // seconds

// To solve the halting problem.
// We get arbitrary number of redirects.
// if a redirect fails, we can stop and report failure
// if we get a success the html should signal the banner that it is done. and we can report success
// if we get a success webViewDidFinishLoad, no more redirects, but the html does not signal the banner.
// in that case we time out the banner load process.
#define kBANNER_LOAD_TIMED_OUT          30  // seconds
#define kINTERSTITIAL_LOAD_TIMED_OUT    5   // seconds
#define kLANDINGPAGE_REQUEST_TIMEOUT    30  // seconds
#define kVIDEO_REQUEST_TIMEOUT          5   // seconds. each request returns an XML that might initiate another request.


#define ENABLE_CONSOLE_LOG      1   // set 1 to enable console logging (AdLog), else set 0

#define kClickableStripDefaultMessage @"לפרטים נוספים לחץ כאן"
#define kMMAdEmptyFileError 1000
#define kMMAdApplicationCanceledError 1001
#define kMMAdNoAdSuccessString 1002
#define kMMAdDomainError @"com.keshet.mmadlib"

#define kAD_SUCCESS_STRING @"#AD_SUCCESS#"



typedef enum {
    ClickableStripLocationBottom,
    ClickableStripLocationTop
} ClickableStripLocation;

@interface MMAdUtil : NSObject

void AdLog(NSString *format, ...);  // implementated in MMAdUtils.m

+ (UIImage*)screenshot;
+ (NSString*)getAdPlayUrl:(NSString*)htmlContent;
+ (NSMutableArray*)createFallbackList:(NSString*)url WithVideoFallbackParam:(NSString*)aFallbackParam WithFallbackListDelimiter:(NSString*)aListDelimiter;
+ (void) sendLinksFromArray:(NSArray*)linksArr;

+ (NSString*)evalAdURL:(NSString*)adURL withParams:(NSString*)param1,...;

+ (NSDictionary*)getArgsFromURL:(NSURL*)url;
+ (NSURL*) urlFromStringWithWhiteSpaces:(NSString*)stringWithWhiteSpaces;
+ (NSDictionary*)extractMakoCatDFPDictionary:(id)dataToExtract;
+ (GADRequest*)createRequestWithDict:(NSDictionary*)dict;
+ (NSString*)stringByReplacingTreeOnIU:(NSString*)iu;
+(NSDictionary*)dictionaryWithNonEmptyValuesForDictionary:(NSDictionary*)dict;
@end

