//
//  VASTWrapper.h
//  MMAdLib-ios
//
//  Created by Yonatan Betzer on 10/26/14.
//  Copyright (c) 2014 mako. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VASTTrackable.h"
#import "VASTInline.h"

@class VAST;

@interface VASTWrapper : VASTInline

@property (nonatomic, strong) VAST *redirectVast;
@property (nonatomic, strong) NSString *vastAdTagURL;
+(VASTWrapper*)fromDictionary:(NSDictionary*)vastWrapperDictionary;

-(NSURL*)playableUrl;
@end
