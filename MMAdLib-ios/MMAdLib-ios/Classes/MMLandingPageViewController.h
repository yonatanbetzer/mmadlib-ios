//
//  MMLandingPageViewController.h
//  MMAd
//
//  Copyright (c) 2013 Mako. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MMLandingPageDelegate <NSObject>
@optional
- (void) landingPageWasDismissed;
@end


@class MMCloseButton;

@interface MMLandingPageViewController : UIViewController <UIWebViewDelegate> {
    UIWebView *landingPageWebView;
    UIButton *btnClose;
    UIButton *btnCloseTop;
    
    UIActivityIndicatorView *spinny;
    UIImageView *snapshotImgView;
    
    BOOL statusBarWasHiddenInTheApp;
}

@property (nonatomic) BOOL openInExternalBrowser;
@property (nonatomic, strong) NSURL *url;
@property (nonatomic, strong) UIImage *snapshotImg;
@property (nonatomic) NSUInteger supportedOrientations;
@property (nonatomic, weak) id<MMLandingPageDelegate> landingPageDelegate;

- (void) displayLandingPageWithURL:(NSURL*)url;
- (void) dismissLandingPage;


@end
