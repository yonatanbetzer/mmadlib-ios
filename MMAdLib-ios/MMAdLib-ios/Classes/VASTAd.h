//
//  VASTAd.h
//  MMAdLib-ios
//
//  Created by Yonatan Betzer on 10/26/14.
//  Copyright (c) 2014 mako. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VASTWrapper.h"
#import "VASTInline.h"
#import "VASTTrackable.h"

@interface VASTAd : VASTTrackable

@property (nonatomic, strong) NSString *adId;
@property NSInteger sequence;
@property (nonatomic, strong) VASTInline *inlineAd;
@property (nonatomic, strong) VASTWrapper *wrapperAd;

+(VASTAd*)fromDictionary:(NSDictionary*)dictionary;
+(NSMutableArray*)fromArray:(NSArray*)array;
-(NSURL*)clickThroughUrl;
-(NSURL*)playableUrl;
@end
