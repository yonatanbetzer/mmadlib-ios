//
//  VASTInline.h
//  MMAdLib-ios
//
//  Created by Yonatan Betzer on 10/26/14.
//  Copyright (c) 2014 mako. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VASTTrackable.h"
#import "VASTCreative.h"

@interface VASTInline : VASTTrackable

@property (nonatomic, strong) NSArray *linearCreatives;
@property (nonatomic, strong) NSArray *nonLinearCreatives;
@property (nonatomic, strong) NSArray *companionAdCreatives;

+(VASTInline*)fromDictionary:(NSDictionary*)vastInlineDictionary;
-(NSURL*)clickThroughUrl;

-(NSURL*)playableUrl;
@end
