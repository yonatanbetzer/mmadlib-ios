//
//  VASTTrackable.h
//  MMAdLib-ios
//
//  Created by Yonatan Betzer on 10/26/14.
//  Copyright (c) 2014 mako. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VASTTrackable : NSObject

@property (nonatomic, strong) NSMutableArray* trackableChildren;
@property (nonatomic) BOOL isActive;
@property (nonatomic) NSTimeInterval skipOffset;

-(void)parseTracking:(NSDictionary*)dictionary;
/*
 report:
 firstQuartile
 midpoint
 thirdQuartile
 */
-(void)reportTime:(NSTimeInterval)time fromTotalDuration:(NSTimeInterval)duration; //
-(void)reportImpression; //
-(void)reportComplete; //
-(void)reportStart; //
-(void)reportError;
-(void)reportPause; //
-(void)reportResume; //
-(void)reportMute;
-(void)reportUnmute;
-(void)reportSkip;
-(void)reportProgress;
-(void)reportClick; //
+(NSArray*)makeArrayOfValues:(NSArray*)array;

@end
