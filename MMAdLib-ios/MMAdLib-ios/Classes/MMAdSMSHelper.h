//
//  SMSHelper.h
//  MakoTV2
//
//  Created by Ran Greenberg on 3/13/14.
//  Copyright (c) 2014 Tomer Har Yoffi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MessageUI/MFMessageComposeViewController.h>

@interface MMAdSMSHelper : NSObject <MFMessageComposeViewControllerDelegate>


-(BOOL)handleUrlString:(NSString*)urlString viewController:(UIViewController*)viewController;

@end
