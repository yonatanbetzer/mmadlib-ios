//
//  VASTVideoClick.h
//  MMAdLib-ios
//
//  Created by Yonatan Betzer on 10/26/14.
//  Copyright (c) 2014 mako. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VASTTrackable.h"

@interface VASTVideoClick : VASTTrackable

@property (nonatomic, strong) NSArray *clickThrough;
@property (nonatomic, strong) NSArray *clickTracking;
@property (nonatomic, strong) NSArray *customClick;

+(VASTVideoClick*)fromDictionary:(NSDictionary*)dictionary;
+(NSMutableArray*)fromArray:(NSArray*)array;


@end
