//
//  VASTInline.m
//  MMAdLib-ios
//
//  Created by Yonatan Betzer on 10/26/14.
//  Copyright (c) 2014 mako. All rights reserved.
//

#import "VASTInline.h"
#import "VASTCreativeLinear.h"

@implementation VASTInline

+(VASTInline*)fromDictionary:(NSDictionary*)vastInlineDictionary
{
    if(vastInlineDictionary)
    {
        VASTInline *result = [[VASTInline alloc] init];
        [result parseTracking:vastInlineDictionary];
        result.linearCreatives = [VASTCreativeLinear fromArray:vastInlineDictionary[@"Creatives"]];
        
        result.trackableChildren = [NSMutableArray array];
        for (VASTCreativeLinear *creative in result.linearCreatives) {
            if(creative)
            {
                [result.trackableChildren addObject:creative];
            }
        }
        
        return result;
    }
    return nil;
}

-(NSURL*)playableUrl
{
    for (VASTCreativeLinear *creative in self.linearCreatives) {
        NSURL *url = [creative playableUrl];
        if(url)
        {
            self.isActive = YES;
            self.skipOffset = creative.skipOffset;
            creative.isActive = YES;
            return url;
        }
    }
    return nil;
}

-(NSURL*)clickThroughUrl
{
    if(self.isActive)
    {
        for (VASTCreativeLinear *creative in self.linearCreatives) {
            if(creative.isActive)
            {
                return [creative clickThroughUrl];
            }
        }
    }
    return nil;
}
@end