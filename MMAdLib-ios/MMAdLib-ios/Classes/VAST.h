//
//  VAST.h
//  MMAdLib-ios
//
//  Created by Yonatan Betzer on 10/26/14.
//  Copyright (c) 2014 mako. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VASTTrackable.h"
#import "XMLDictionary.h"
@class VAST;

typedef void(^VASTParserComplete)(VAST *vast);


@interface VAST : VASTTrackable

@property (nonatomic, strong) NSArray *ads;

+(void)fromUrl:(NSString*)url completion:(VASTParserComplete)completion;

+(VAST*)fromDictionary:(NSDictionary*)dictionary;

-(NSURL*)playableUrl;
-(NSURL*)clickThroughUrl;
@end
