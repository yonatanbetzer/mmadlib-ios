//
//  DFPJsonHandler.h
//  Mako
//
//  Created by Rona Assouline on 6/18/14.
//
//

#import <Foundation/Foundation.h>

@interface MMParameterStringReplacement : NSObject

@property(nonatomic, strong) NSString *regId;
@property(nonatomic, strong) NSString *pushTrigger;
@property(nonatomic, strong) NSString *pushTags;

//-(void)downloadJsonFromUrl:(NSURL *)url;
-(NSDictionary *)replaceVariablesInCustParams:(NSDictionary *)jsonDict;
-(NSString *)replaceVariableInString:(NSString *)string usingContextDictionary:(NSDictionary*)context;
-(NSString *)replaceVariableInString:(NSString *)string;

@end
