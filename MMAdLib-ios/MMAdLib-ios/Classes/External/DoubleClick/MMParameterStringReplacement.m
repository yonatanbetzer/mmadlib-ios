//
//  DFPJsonHandler.m
//  Mako
//
//  Created by Rona Assouline on 6/18/14.
//
//
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>

#import "MMParameterStringReplacement.h"
//#import <MakoLocation/AFNetworking.h>
#import <sys/utsname.h>

#define IS_IPAD() ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)

@interface MMParameterStringReplacement ()

@property (nonatomic) NSDictionary *jsonDictonary;

@end

@implementation MMParameterStringReplacement

-(id)initWithRegId:(NSString *)regId
       pushTrigger:(NSString *)pushTrigger
          pushTags:(NSString *)pushTags {
    
    if (self = [super init]) {
        self.regId = regId;
        self.pushTrigger = pushTrigger;
        self.pushTags = pushTags;
    }
    return self;
}

-(NSDictionary *)replaceVariablesInCustParams:(NSDictionary *)jsonDict {
    NSMutableDictionary *jsonMuted = [jsonDict mutableCopy];
    NSMutableDictionary *custParamsMuted;
    
    for (NSString* key in jsonDict) {
        if ([key isEqualToString:@"cust_params"]) {
            NSMutableDictionary * custParams = [jsonDict objectForKey:key];
            custParamsMuted = [[NSMutableDictionary alloc] init];
            
            [jsonMuted setValue:custParamsMuted forKey:key];
            for (id subKey in custParams) {
                NSString *value =[self replaceVariableInString:[custParams objectForKey:subKey]];
                if(value.length > 0)
                {
                    [custParamsMuted setValue:value forKey:subKey];
                }
            }
        } else {
            NSObject *value =[self replaceVariableInString:jsonDict[key]];
            [jsonMuted setValue:value
                         forKey:key];
        }
    }
    
    self.jsonDictonary = jsonMuted;
    
    return self.jsonDictonary;
}

-(NSString *)replaceVariableInString:(NSString *)string usingContextDictionary:(NSDictionary*)context
{
    // string = [NSString stringWithFormat:@"%@&bla=%%JSON:root;breadcrumb[1];makoCatDFP;iu%%", string];
    int count = 0;
    BOOL jsonExists = YES;
    while (jsonExists) {
        string = [self replaceJSONParameter:string usingContextDictionary:context];
        
        NSRange range = [string rangeOfString:@"%JSON"];
        jsonExists = range.location != NSNotFound;
        
        // prevent infinite loop - just a sanity check...
        count++;
        if(count > 100)
            break;
    }
    return string;
}

-(NSString*)replaceJSONParameter:(NSString*)string usingContextDictionary:(NSDictionary*)context
{
    BOOL replacementDone = NO;
    NSRange range = [string rangeOfString:@"%JSON"];
    if(range.location != NSNotFound &&
       string.length > range.location + 1)
    {
        NSMutableString *stringToReplace = [NSMutableString stringWithString:@"%JSON:"];
        NSString *resultValue = @"";
        NSString *substring = [string substringFromIndex:range.location + 1];
        NSRange endLocation = [substring rangeOfString:@"%"];
        if(endLocation.location != NSNotFound &&
           endLocation.location > 5)
        {
            substring = [substring substringToIndex:endLocation.location];
            substring = [substring substringFromIndex:5];
            [stringToReplace appendString:substring];
            NSArray *parts = [substring componentsSeparatedByString:@";"];
            if(parts &&
               context &&
               [context isKindOfClass:[NSDictionary class]])
            {
                for (NSString *key in parts) {
                    if(key &&
                       context &&
                       [context isKindOfClass:[NSDictionary class]])
                    {
                        NSRange openingBracketsRange = [key rangeOfString:@"["];
                        if(openingBracketsRange.location != NSNotFound &&
                           key.length > openingBracketsRange.location + 2)
                        {
                            // Array !
                            NSString *keyName = [key substringToIndex:openingBracketsRange.location];
                            NSString *indexInArrayString = [key substringFromIndex:openingBracketsRange.location + 1];
                            indexInArrayString = [indexInArrayString substringToIndex:indexInArrayString.length - 1];
                            int indexInArray = [indexInArrayString intValue];
                            NSArray *array = context[keyName];
                            if([array isKindOfClass:[NSArray class]] &&
                               array.count > indexInArray)
                            {
                                context = array[indexInArray];
                            }
                        }
                        else
                        {
                            context = context[key];
                        }
                    }
                }
                if(context &&
                   [context isKindOfClass:[NSString class]])
                {
                    resultValue = (NSString*)context;
                    replacementDone = YES;
                }
            }
        }
        [stringToReplace appendString:@"%"];
        string = [string stringByReplacingOccurrencesOfString:stringToReplace withString:resultValue];
    }
    return string;
}

-(NSString*) machineName
{
    struct utsname systemInfo;
    uname(&systemInfo);
    return [NSString stringWithCString:systemInfo.machine
                              encoding:NSUTF8StringEncoding];
}


-(NSString *)replaceVariableInString:(NSString *)string {
    
    if(![string isKindOfClass:[NSString class]])
    {
        return string;
    }
    
    if([MMAdManager getInstance].lastContext)
    {
        string = [self replaceVariableInString:string usingContextDictionary:[MMAdManager getInstance].lastContext];
    }
    
    NSDictionary *locationParams = [[MMAdManager getInstance].makoLocationManager dictionaryLocationCustParams];
    NSDictionary *server_variables = [MMAdManager getInstance].server_variables;
    
    
    NSMutableDictionary *variables = [[NSMutableDictionary alloc] init];
    [variables addEntriesFromDictionary:locationParams];
    [variables addEntriesFromDictionary:server_variables];
    server_variables = variables;
    
    
    NSArray *keys = [server_variables.allKeys copy];
    for (NSString *key in keys) {
        NSString *value =server_variables[key];
        if([value isKindOfClass:[NSString class]])
        {
            string = [string stringByReplacingOccurrencesOfString:key withString:server_variables[key]];
        }
        else{
            if([value isKindOfClass:[NSArray class]])
            {
                NSArray *array = (NSArray *)value;
                string = [string stringByReplacingOccurrencesOfString:key withString:[array componentsJoinedByString:@","]];
            }
        }
    }
    
    NSString *uniqueIdentifier = [MMAdManager getInstance].current_user_uuid;
    
    uniqueIdentifier = uniqueIdentifier ? uniqueIdentifier : @"";
    string = [string stringByReplacingOccurrencesOfString:@"%DEVICE_ID%"
                                               withString:uniqueIdentifier];
    string = [string stringByReplacingOccurrencesOfString:@"%DEVICE_ID%"
                                               withString:@""];
    NSInteger randNum = arc4random() % 1000000;
    string = [string stringByReplacingOccurrencesOfString:@"%RANDOM_NUMBER%"
                                               withString: [NSString stringWithFormat:@"%i", (int)randNum]];
    string = [string stringByReplacingOccurrencesOfString:@"%RANDOM_NUMBER%"
                                               withString:@""];
    
    NSString *appID = [MMAdManager getInstance].app_id;
    appID = appID ? appID : @"";
    string = [string stringByReplacingOccurrencesOfString:@"%APP_ID%"
                                               withString:appID];
    
    NSString *deviceType = [[UIDevice currentDevice] model];
    deviceType = deviceType ? deviceType : @"";
    string = [string stringByReplacingOccurrencesOfString:@"%DEVICE_TYPE%"
                                               withString:deviceType];
    
    NSString* deviceModel = [self machineName];
    deviceModel = deviceModel ? deviceModel : @"";
    string = [string stringByReplacingOccurrencesOfString:@"%DEVICE_MODEL%"
                                               withString:deviceModel];
    
    string = [string stringByReplacingOccurrencesOfString:@"%ADVERTISER_ID%"
                                               withString:@""];
    
    NSString *guid = [MMAdManager getInstance].current_vcm_id;
    guid = guid ? guid : @"";
    string = [string stringByReplacingOccurrencesOfString:@"%GUID%"
                                               withString:guid];
    
    string = [string stringByReplacingOccurrencesOfString:@"%PRODUCT_BUNDLE_ID%"
                                               withString:[[NSBundle mainBundle] bundleIdentifier]];
    
    string = [string stringByReplacingOccurrencesOfString:@"%IS_PAYING_USER%"
                                               withString:[NSString stringWithFormat:@"%d", [MMAdManager getInstance].isPayingUser]];
    
    string = [string stringByReplacingOccurrencesOfString:@"%IS_PAID_CONTENT%"
                                               withString:[NSString stringWithFormat:@"%d", [MMAdManager getInstance].isPaidContent]];
    
    NSString *version = [NSString stringWithFormat:@"%@",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
    version = version ? version : @"";
    string = [string stringByReplacingOccurrencesOfString:@"%VERSION%"
                                               withString:version];
    
    CTTelephonyNetworkInfo *netinfo = [[CTTelephonyNetworkInfo alloc] init];
    NSString *carrier = [[netinfo subscriberCellularProvider] carrierName];
    carrier = [carrier stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    carrier = carrier ? carrier : @"";
    string = [string stringByReplacingOccurrencesOfString:@"%CARRIER%"
                                               withString:carrier];
    
    CLLocation *currentLocation = [MMAdManager getInstance].makoLocationManager.currentLocation;
    
    if(![MMAdManager getInstance].makoLocationManager)
    {
        [MLManager sharedInstance].locationExpiryForAppUsage = [MMAdManager getInstance].locationExpiryForAppUsageReport;
        currentLocation = [MLManager sharedInstance].currentLocation;
    }
    
    NSString *lat = @"";
    NSString *lng = @"";
    
    if(currentLocation)
    {
        lat = [NSString stringWithFormat:@"%lf", currentLocation.coordinate.latitude];
        lng = [NSString stringWithFormat:@"%lf", currentLocation.coordinate.longitude];
    }
    string = [string stringByReplacingOccurrencesOfString:@"%LAT%"
                                               withString:lat];
    
    string = [string stringByReplacingOccurrencesOfString:@"%LNG%"
                                               withString:lng];
    
    
    NSLocale *locale = [NSLocale currentLocale];
    NSString *language = [locale displayNameForKey:NSLocaleIdentifier
                                             value:[locale localeIdentifier]];
    
    language = language ? language : @"";
    
    string = [string stringByReplacingOccurrencesOfString:@"%LOCALE%"
                                               withString:language];
    
    NSString * languageCode = [[NSLocale preferredLanguages] objectAtIndex:0];
    languageCode = languageCode ? languageCode : @"";
    string = [string stringByReplacingOccurrencesOfString:@"%LANGUAGE%"
                                               withString:languageCode];

    string = [string stringByReplacingOccurrencesOfString:@"%LANGUAGE%"
                                               withString:language];
    
    
    self.pushTrigger = self.pushTrigger ? self.pushTrigger : @"";
    string = [string stringByReplacingOccurrencesOfString:@"%PUSH_TRIGGER%"
                                               withString:self.pushTrigger];
    
    self.regId = self.regId ? self.regId : @"";
    string = [string stringByReplacingOccurrencesOfString:@"%REG_ID%"
                                               withString:self.regId];
    
    string = [string stringByReplacingOccurrencesOfString:@"%DEVICE_STR%"
                                               withString:@""];
    
    string = [string stringByReplacingOccurrencesOfString:@"%OS_TYPE%"
                                               withString:@"iOS"];
    
    NSString *osVersion = [[UIDevice currentDevice] systemVersion];
    string = [string stringByReplacingOccurrencesOfString:@"%OS_VERSION%"
                                               withString:osVersion];
    
    //NSString* pushTags = [ConfigData stringForKey:MAKO_PUSH_TAGS_STATS inGroup:GROUP_ANALYTICS];
    self.pushTags = self.pushTags ? self.pushTags : @"";
    string = [string stringByReplacingOccurrencesOfString:@"%PUSH_TAGS%"
                                               withString:self.pushTags];
    
    
    string = [string stringByReplacingOccurrencesOfString:@"%TIMESTAMP%"
                                               withString:[NSString stringWithFormat:@"%f", [[NSDate date] timeIntervalSince1970]]];
    
    if([MMAdManager getInstance].isResumeVideoPlay)
    {
        string = [string stringByReplacingOccurrencesOfString:@"%VIDEO_FROM_TIME%"
                                                   withString:@"true"];
    }
    if(string.length > 2 &&
       [[string substringToIndex:1] isEqualToString:@"%"] &&
       [[string substringFromIndex:string.length - 1] isEqualToString:@"%"])
    {
        string = @"";
    }
    return string;
}

//
//- (NSString *)getGUID {
//    CFUUIDRef theUUID = CFUUIDCreate(NULL);
//    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
//    CFRelease(theUUID);
//    return (__bridge NSString *)string;
//}

@end
