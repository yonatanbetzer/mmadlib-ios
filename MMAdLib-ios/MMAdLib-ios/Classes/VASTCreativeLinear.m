//
//  VASTCreativeLinear.m
//  MMAdLib-ios
//
//  Created by Yonatan Betzer on 10/26/14.
//  Copyright (c) 2014 mako. All rights reserved.
//

#import "VASTCreativeLinear.h"
#import "VASTMediaFile.h"
#import "VASTVideoClick.h"

@implementation VASTCreativeLinear

+(VASTCreativeLinear*)fromDictionary:(NSDictionary*)dictionary
{
    if(dictionary[@"Linear"])
    {
        dictionary = dictionary[@"Linear"];
    }
    else
    {
        return nil;
    }
    VASTCreativeLinear *result = [[VASTCreativeLinear alloc] init];
    result.type = kMFCreativeTypeLinear;
    NSString *durationString = dictionary[@"Duration"];
    if(durationString)
    {
        NSArray *durationParts = [durationString componentsSeparatedByString:@":"];
        if(durationParts.count == 3)
        {
            NSInteger hours = [durationParts[0] integerValue];
            NSInteger minutes = [durationParts[1] integerValue];
            CGFloat seconds = [durationParts[2] floatValue];
            result.duration = (hours * 60.0 * 60.0) + (minutes * 60.0) + seconds;
        }
    }
    NSString *skipOffset = dictionary[@"skipoffset"];
    if(skipOffset.length == 0)
    {
        skipOffset = dictionary[@"_skipoffset"];
    }
    if(skipOffset.length > 0)
    {
        if([skipOffset rangeOfString:@"%"].length > 0)
        {
            skipOffset = [skipOffset stringByReplacingOccurrencesOfString:@"%" withString:@""];
            CGFloat percentage = [skipOffset floatValue];
            result.skipOffset = result.duration * percentage / 100.0;
        }
        else
        {
            NSArray *offsetParts = [skipOffset componentsSeparatedByString:@":"];
            if(offsetParts.count == 3)
            {
                NSInteger hours = [offsetParts[0] integerValue];
                NSInteger minutes = [offsetParts[1] integerValue];
                CGFloat seconds = [offsetParts[2] floatValue];
                result.skipOffset = (hours * 60.0 * 60.0) + (minutes * 60.0) + seconds;
            }
        }
    }
    [result parseTracking:dictionary];
    result.mediaFiles = [VASTMediaFile fromArray:dictionary[@"MediaFiles"]];
    result.videoClicks = [VASTVideoClick fromArray:dictionary[@"VideoClicks"]];
    
    result.trackableChildren = [NSMutableArray array];
    for (VASTTrackable *trackable in result.mediaFiles) {
        [result.trackableChildren addObject:trackable];
    }
    for (VASTTrackable *trackable in result.videoClicks) {
        [result.trackableChildren addObject:trackable];
    }
    return result;
}

+(NSMutableArray*)fromArray:(NSArray*)array
{
    if([array isKindOfClass:[NSDictionary class]])
    {
        array = @[array];
    }
    
    NSMutableArray *result = [[NSMutableArray alloc] init];
    
    for(NSDictionary *dic in array)
    {
        NSDictionary *dictionary = dic[@"Creative"];
        if([dictionary isKindOfClass:[NSArray class]])
        {
            NSArray *arr = (NSArray*)dictionary;
            for (NSDictionary *dic2 in arr) {
                VASTCreativeLinear *ad = [VASTCreativeLinear fromDictionary:dic2];
                if(ad)
                {
                    [result addObject:ad];
                }
            }
        }
        else if([dictionary isKindOfClass:[NSDictionary class]])
        {
            VASTCreativeLinear *ad = [VASTCreativeLinear fromDictionary:dictionary];
            if(ad)
            {
                [result addObject:ad];
            }
        }
    }
    
    return result;
}

-(NSURL*)playableUrl
{
    for (VASTMediaFile *mediaFile in self.mediaFiles) {
        NSURL *url = [mediaFile playableUrl];
        if(url)
        {
            for(VASTVideoClick *click in self.videoClicks)
            {
                click.isActive = YES;
            }
            self.isActive = YES;
            return url;
        }
    }
    return nil;
}

-(NSURL*)clickThroughUrl
{
    if(self.isActive)
    {
        for(VASTVideoClick *click in self.videoClicks)
        {
            NSArray *clickThroughArray = click.clickThrough;
            if(clickThroughArray.count > 0)
            {
                NSString *urlString = clickThroughArray[0];
                return [NSURL URLWithString:urlString];
            }
        }
    }
    return nil;
}

@end
