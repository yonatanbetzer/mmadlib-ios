//
//  VASTTrackable.m
//  MMAdLib-ios
//
//  Created by Yonatan Betzer on 10/26/14.
//  Copyright (c) 2014 mako. All rights reserved.
//

#import "VASTTrackable.h"
#import "VASTVideoClick.h"

@implementation VASTTrackable
{
    NSMutableArray *errorUrls;
    NSMutableArray *impressionUrls;
    NSMutableArray *startUrls;
    NSMutableArray *completeUrls;
    NSMutableArray *firstQuartileUrls;
    NSMutableArray *middleUrls;
    NSMutableArray *thirdQuartileUrls;
    NSMutableArray *pauseUrls;
    NSMutableArray *resumeUrls;
    NSMutableArray *muteUrls;
    NSMutableArray *unmuteUrls;
    NSMutableArray *skipUrls;
    NSMutableArray *progressUrls;
    NSMutableArray *clickUrls;
}

-(void)parseTracking:(NSDictionary*)dictionary
{
    errorUrls = [NSMutableArray array]; //
    impressionUrls = [NSMutableArray array]; //
    startUrls = [NSMutableArray array]; //
    completeUrls = [NSMutableArray array]; //
    firstQuartileUrls = [NSMutableArray array]; //
    middleUrls = [NSMutableArray array]; //
    thirdQuartileUrls = [NSMutableArray array]; //
    pauseUrls = [NSMutableArray array]; //
    resumeUrls = [NSMutableArray array]; //
    muteUrls = [NSMutableArray array]; //
    unmuteUrls = [NSMutableArray array]; //
    skipUrls = [NSMutableArray array]; //
    progressUrls = [NSMutableArray array]; //
    clickUrls = [NSMutableArray array]; //
    
    NSDictionary *trackingEvents = dictionary[@"TrackingEvents"];
    NSArray *tracking = trackingEvents[@"Tracking"];
    for (NSDictionary *trackingDictionary in tracking) {
        if([trackingDictionary isKindOfClass:[NSDictionary class]])
        {
            NSString *event = trackingDictionary[@"_event"];
            NSString *url = trackingDictionary[@"__text"];
            if([event isEqualToString:@"firstQuartile"]) [firstQuartileUrls addObject:url];
            if([event isEqualToString:@"midpoint"]) [middleUrls addObject:url];
            if([event isEqualToString:@"thirdQuartile"]) [thirdQuartileUrls addObject:url];
            if([event isEqualToString:@"complete"]) [completeUrls addObject:url];
            if([event isEqualToString:@"mute"]) [muteUrls addObject:url];
            if([event isEqualToString:@"unmute"]) [unmuteUrls addObject:url];
            if([event isEqualToString:@"pause"]) [pauseUrls addObject:url];
            if([event isEqualToString:@"resume"]) [resumeUrls addObject:url];
            if([event isEqualToString:@"start"]) [startUrls addObject:url];
            if([event isEqualToString:@"skip"]) [skipUrls addObject:url];
            if([event isEqualToString:@"progress"]) [progressUrls addObject:url];
            
        }
    }
    NSArray *impressions = dictionary[@"Impression"];
    impressions = [VASTTrackable makeArrayOfValues:impressions];
    [impressionUrls addObjectsFromArray:impressions];
    
    NSArray *errors = dictionary[@"Error"];
    errors = [VASTTrackable makeArrayOfValues:errors];
    [errorUrls addObjectsFromArray:errors];
    
    if([self isKindOfClass:[VASTVideoClick class]])
    {
        VASTVideoClick *videoClick = (VASTVideoClick *)self;
        [clickUrls addObjectsFromArray:videoClick.clickTracking];
    }
}

-(void)reportTime:(NSTimeInterval)time fromTotalDuration:(NSTimeInterval)duration
{
    if(self.isActive)
    {
        if(time / duration >= 0.25 && firstQuartileUrls.count > 0)
        {
            [self reportFirstQuartile];
        }
        if(time / duration >= 0.5 && middleUrls.count > 0)
        {
            [self reportMiddle];
        }
        if(time / duration >= 0.75 && thirdQuartileUrls.count > 0)
        {
            [self reportThirdQuartile];
        }
        for (VASTTrackable *trackable in self.trackableChildren) {
            [trackable reportTime:time fromTotalDuration:duration];
        }
    }
}

-(void)reportFirstQuartile
{
    for (NSString *url in firstQuartileUrls) {
        [self sendReportToURL:url];
    }
    [firstQuartileUrls removeAllObjects];
}

-(void)reportMiddle
{
    for (NSString *url in middleUrls) {
        [self sendReportToURL:url];
    }
    [middleUrls removeAllObjects];
}

-(void)reportThirdQuartile
{
    for (NSString *url in thirdQuartileUrls) {
        [self sendReportToURL:url];
    }
    [thirdQuartileUrls removeAllObjects];
}

-(void)reportImpression
{
    if(self.isActive)
    {
        for (NSString *url in impressionUrls) {
            [self sendReportToURL:url];
        }
        for (VASTTrackable *trackable in self.trackableChildren) {
            [trackable reportImpression];
        }
    }
    [impressionUrls removeAllObjects];
}

-(void)reportStart
{
    if(self.isActive)
    {
        // NSLog(@"%s", __FUNCTION__);
        for (NSString *url in startUrls) {
            [self sendReportToURL:url];
        }
        for (VASTTrackable *trackable in self.trackableChildren) {
            [trackable reportStart];
        }
    }
    [startUrls removeAllObjects];
}

-(void)reportError
{
    if(self.isActive)
    {
        // NSLog(@"%s", __FUNCTION__);
        for (NSString *url in errorUrls) {
            [self sendReportToURL:url];
        }
        for (VASTTrackable *trackable in self.trackableChildren) {
            [trackable reportError];
        }
    }
    [errorUrls removeAllObjects];
}

-(void)reportComplete
{
    if(self.isActive)
    {
        // NSLog(@"%s", __FUNCTION__);
        for (NSString *url in completeUrls) {
            [self sendReportToURL:url];
        }
        for (VASTTrackable *trackable in self.trackableChildren) {
            [trackable reportComplete];
        }
    }
    [completeUrls removeAllObjects];
}

-(void)reportPause
{
    if(self.isActive)
    {
        // NSLog(@"%s", __FUNCTION__);
        for (NSString *url in pauseUrls) {
            [self sendReportToURL:url];
        }
        for (VASTTrackable *trackable in self.trackableChildren) {
            [trackable reportPause];
        }
    }
}

-(void)reportResume
{
    if(self.isActive)
    {
        // NSLog(@"%s", __FUNCTION__);
        for (NSString *url in resumeUrls) {
            [self sendReportToURL:url];
        }
        for (VASTTrackable *trackable in self.trackableChildren) {
            [trackable reportResume];
        }
    }
}

-(void)reportMute
{
    if(self.isActive)
    {
        // NSLog(@"%s", __FUNCTION__);
        for (NSString *url in muteUrls) {
            [self sendReportToURL:url];
        }
        for (VASTTrackable *trackable in self.trackableChildren) {
            [trackable reportMute];
        }
    }
}

-(void)reportUnmute
{
    if(self.isActive)
    {
        // NSLog(@"%s", __FUNCTION__);
        for (NSString *url in unmuteUrls) {
            [self sendReportToURL:url];
        }
        for (VASTTrackable *trackable in self.trackableChildren) {
            [trackable reportUnmute];
        }
    }
}

-(void)reportSkip
{
    if(self.isActive)
    {
        // NSLog(@"%s", __FUNCTION__);
        for (NSString *url in skipUrls) {
            [self sendReportToURL:url];
        }
        for (VASTTrackable *trackable in self.trackableChildren) {
            [trackable reportSkip];
        }
    }
    [skipUrls removeAllObjects];
}

-(void)reportProgress
{
    if(self.isActive)
    {
        // NSLog(@"%s", __FUNCTION__);
        for (NSString *url in progressUrls) {
            [self sendReportToURL:url];
        }
        for (VASTTrackable *trackable in self.trackableChildren) {
            [trackable reportProgress];
        }
    }
}

-(void)reportClick
{
    if(self.isActive)
    {
        // NSLog(@"%s", __FUNCTION__);
        for (NSString *url in clickUrls) {
            [self sendReportToURL:url];
        }
        for (VASTTrackable *trackable in self.trackableChildren) {
            [trackable reportClick];
        }
    }
}

+(NSArray*)makeArrayOfValues:(NSArray*)array
{
    if([array isKindOfClass:[NSString class]])
    {
        return @[array];
    }
    if([array isKindOfClass:[NSDictionary class]])
    {
        array = @[array];
    }
    NSMutableArray *result = [NSMutableArray array];
    if([array isKindOfClass:[NSArray class]])
    {
        for (NSDictionary *dic in array) {
            if([dic isKindOfClass:[NSDictionary class]])
            {
                [result addObject:dic[@"__text"]];
            }
            else if([dic isKindOfClass:[NSString class]])
            {
                [result addObject:dic];
            }
        }
    }
    
    return result;
}


-(void)sendReportToURL:(NSString*)urlString
{
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    AFJSONRequestOperation *operation = [AFJSONRequestOperation JSONRequestOperationWithRequest:request success:^(NSURLRequest *request, NSHTTPURLResponse *response, id JSON) {
        
    } failure:nil];
    [operation start];
}


//
//-(NSString *)description
//{
//    NSMutableArray *result = [NSMutableArray array];
//    [result addObject:NSStringFromClass([self class])];
//
//    for (NSObject *obj in self.trackableChildren) {
//        NSString *subString = [NSString stringWithFormat:@"%@", obj];
//        [result addObject:subString];
//    }
//
//    return [result componentsJoinedByString:@"<br>"];
//}

@end
