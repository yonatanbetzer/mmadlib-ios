//
//  MMLandingPageViewController.m
//  MMAd
//
//  Copyright (c) 2013 Mako. All rights reserved.
//

#import "MMLandingPageViewController.h"
#import "MMAdUtil.h"
#import <MessageUI/MessageUI.h>
#import "MMAdSMSHelper.h"

@interface MMLandingPageViewController ()

@property (nonatomic, strong) MMAdSMSHelper* smsHelper;

@end

#define BTN_CLOSE_W 32
#define BTN_CLOSE_H 32
#define TRANSPARENT_PADDING 6
#define DIAL_ALERT_TAG 2

@implementation MMLandingPageViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        statusBarWasHiddenInTheApp = [UIApplication sharedApplication].statusBarHidden;
        [UIApplication sharedApplication].statusBarHidden = YES;
        self.supportedOrientations = UIInterfaceOrientationMaskAll;
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    //
    // set background view screen snapshop
    //
    if (snapshotImgView){
        snapshotImgView = [[UIImageView alloc] initWithFrame:self.view.bounds];
        snapshotImgView.contentMode = UIViewContentModeScaleAspectFit;
        snapshotImgView.image = _snapshotImg;
        [self.view addSubview:snapshotImgView];
    }
    //
    // load the landing page
    //
    landingPageWebView = [[UIWebView alloc] initWithFrame:self.view.bounds];
    landingPageWebView.delegate = self;
    [self.view addSubview:landingPageWebView];
    landingPageWebView.alpha = 0;
    // TODO: read about cache policy
    
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:self.url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:kLANDINGPAGE_REQUEST_TIMEOUT];
    [landingPageWebView loadRequest:request];
    
    //
    // add the X button
    //
    int btnPadding = MIN(self.view.bounds.size.width, self.view.bounds.size.height) * 0.03;
    btnClose = [[UIButton alloc] initWithFrame:CGRectMake(btnPadding, btnPadding, BTN_CLOSE_W,BTN_CLOSE_H)];
//    [btnClose setTitle:@"X" forState:UIControlStateNormal];
    [btnClose setBackgroundImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
    btnClose.titleLabel.font = [UIFont boldSystemFontOfSize:16];
    [btnClose addTarget:self action:@selector(dismissLandingPage) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnClose];
    //    btnClose.alpha = 0;
    
    btnCloseTop = [UIButton buttonWithType:UIButtonTypeCustom];
    btnCloseTop.frame = CGRectMake(btnPadding, btnPadding, BTN_CLOSE_W,BTN_CLOSE_H);
    [btnCloseTop addTarget:self action:@selector(dismissLandingPage) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnCloseTop];
    
    
    
    spinny = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    spinny.center = self.view.center;
    [self.view addSubview:spinny];
    [spinny startAnimating];
    
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [self prefersStatusBarHidden];
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    }
    
    // init the sms helper
    self.smsHelper = [[MMAdSMSHelper alloc] init];
    
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}


-(void)viewWillLayoutSubviews{
    landingPageWebView.frame = self.view.bounds;
    int btnPadding = MIN(self.view.bounds.size.width, self.view.bounds.size.height) * 0.03;
    [btnClose setFrame:CGRectMake(self.view.bounds.size.width - BTN_CLOSE_W - btnPadding, btnPadding, BTN_CLOSE_W,BTN_CLOSE_H)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) dealloc {
    landingPageWebView.delegate = nil;
}

- (void) displayLandingPageWithURL:(NSURL*)landingPageURL{
    self.url = landingPageURL;
    UIViewController *rootVC = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    UIViewController *vcToPresentOn = rootVC;
    
    if ([vcToPresentOn respondsToSelector:@selector(presentedViewController)]){
        while (vcToPresentOn.presentedViewController){
            vcToPresentOn = vcToPresentOn.presentedViewController;
        }
    }
    
    if ([vcToPresentOn respondsToSelector:@selector(presentViewController:animated:completion:)]){
        [vcToPresentOn presentViewController:self animated:NO completion:^{
            
        }];
    }
}

- (void) dismissLandingPage{
    [UIApplication sharedApplication].statusBarHidden = statusBarWasHiddenInTheApp;
    
    if ([self respondsToSelector:@selector(dismissViewControllerAnimated:completion:)])
        [self dismissViewControllerAnimated:NO completion:^{
            AdLog(@"LandingPage done dismmising");
        }];
    
    if ([_landingPageDelegate respondsToSelector:@selector(landingPageWasDismissed)]){
        [_landingPageDelegate landingPageWasDismissed];
    }
}

# pragma mark - UIWebViewDelegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    NSString * urlString = [[request URL] absoluteString];
    // check for open in external browser param (used with 3rd party URLs)
    NSRange range = [urlString rangeOfString:@"makoOpenExternal=1" options:NSCaseInsensitiveSearch];
    if (range.location != NSNotFound){
        self.openInExternalBrowser = YES;
    }
    
    if ([self.smsHelper handleUrlString:urlString viewController:self])
        return NO;
    
    if (self.openInExternalBrowser){
        UIApplication * app = [UIApplication sharedApplication];
        if ([app canOpenURL:request.URL]){
            [app openURL:request.URL];
        }
        [self dismissLandingPage];
        return NO;
    }else if ([request.URL.host hasPrefix:@"itunes.apple.com"] ){
        [[UIApplication sharedApplication] openURL:[request URL]];
        [self dismissLandingPage];
        return NO;
        
    }else if ([request.URL.scheme isEqualToString:@"http"] || [[request URL].scheme isEqualToString:@"https"]){
        return YES;
        
    }else{ // special url (tel:// or mailto: or sms://)
        
        if ([request.URL.scheme isEqualToString:@"tel"]){ // phone no.
            
            if ([[UIApplication sharedApplication] canOpenURL:request.URL]) {
                // can make phone calls
                [landingPageWebView stopLoading];
                
                NSString * strNumber = request.URL.host;
                if (!strNumber)
                    // this is a hack to support also URL's of type "tel:5555555"
                    strNumber = [request.URL.absoluteString substringFromIndex:4];
                
                UIAlertView * dialAlert = [[UIAlertView alloc] initWithTitle:nil
                                                                     message:strNumber
                                                                    delegate:self
                                                           cancelButtonTitle:@"ביטול"
                                                           otherButtonTitles:@"התקשר", nil];
                dialAlert.tag = DIAL_ALERT_TAG;
                [dialAlert show];
                return NO;
            } else {
                UIAlertView *Notpermitted=[[UIAlertView alloc] initWithTitle:@"הודעה"
                                                                     message:@"מכשירך לא תומך בשיחות טלפון."
                                                                    delegate:nil
                                                           cancelButtonTitle:@"OK"
                                                           otherButtonTitles:nil];
                [Notpermitted show];
                [self dismissLandingPage];
                return NO;
            }
        }else
            if ([request.URL.scheme isEqualToString:@"sms"]){ // sms
                if ([MFMessageComposeViewController canSendText]) {
                    // can send SMS
                    [[UIApplication sharedApplication] openURL:request.URL];
                    [self dismissLandingPage];
                    return NO;
                } else {
                    UIAlertView *Notpermitted=[[UIAlertView alloc] initWithTitle:@"הודעה"
                                                                         message:@"מכשירך לא תומך בשליחת הודעות טקסט"
                                                                        delegate:nil
                                                               cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil];
                    [Notpermitted show];
                    return NO;
                }
            } else if ([request.URL.scheme isEqualToString:@"mailto"]){ // email
                if ([MFMailComposeViewController canSendMail]) {
                    // can send Email
                    [[UIApplication sharedApplication] openURL:request.URL];
                    [self dismissLandingPage];
                    return NO;
                } else {
                    UIAlertView *Notpermitted=[[UIAlertView alloc] initWithTitle:@"הודעה"
                                                                         message:@"מכשירך לא מאפשר שליחת מייל"
                                                                        delegate:nil cancelButtonTitle:@"OK"
                                                               otherButtonTitles:nil];
                    [Notpermitted show];
                    return NO;
                }
            } else { // check application prefix
                UIApplication * app = [UIApplication sharedApplication];
                
                if ([app canOpenURL:request.URL]){
                    [app openURL:request.URL];
                    [self dismissLandingPage];
                    return NO;
                }
            }
    }
    
    //    [spinny stopAnimating];
    
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    
    [spinny stopAnimating];
    // show the webview
    [UIView animateWithDuration:0.75 animations:^{
        landingPageWebView.alpha = 1;
        //        btnClose.alpha = 1;
        
        
    } completion:^(BOOL finished){
        AdLog(@"showing the landing page");
    }];
    
}


- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [spinny stopAnimating];
    
    // Ignore "Frame Load Interrupted" errors. Seen after app store links / tel: links.
    if (error.code == 102 && [error.domain isEqual:@"WebKitErrorDomain"])
        return;
    // Igonre NSURLErrorDomain error. in case the redirect happens before the previous request has been completed
    if (error.code == -999 && [error.domain isEqual:@"NSURLErrorDomain"])
        return;
    
    //    if (_eMaavaronMode == MaavaronModeLandingPage && error.code == -1003 && [error.domain isEqual:@"NSURLErrorDomain"])
    //        return;
    [self dismissLandingPage];
}


#pragma mark - alert view delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == DIAL_ALERT_TAG){ // dial alert message
        switch (buttonIndex) {
            case 1:
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",alertView.message]]];
                break;
                
            default:
                break;
        }
        
        [self dismissLandingPage];
    }
}


#pragma mark - orientation
-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return [self shouldAutorotate] && ([self supportedInterfaceOrientations] & (1 << toInterfaceOrientation));
}

// iOS6
- (BOOL) shouldAutorotate{
    return YES;
}
- (NSUInteger)supportedInterfaceOrientations {
    return self.supportedOrientations;
}


@end
