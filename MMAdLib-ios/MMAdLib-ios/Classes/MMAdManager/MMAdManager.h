//
//  MMAdManager.h
//  MMAd
//
//  Copyright (c) 2013 Mako. All rights reserved.
//

#import <MakoLocationStaticLib/MakoLocationStaticLib.h>

#define DONE_BUTTON                             @"done_button"
#define DONE_BUTTON_LOCATION_STRING             @"done_button_location"
#define DONE_BUTTON_MARGIN_X_STRING             @"done_button_margin_x"
#define DONE_BUTTON_MARGIN_Y_STRING             @"done_button_margin_y"
#define DONE_BUTTON_HEIGHT                      @"done_button_height"
#define DONE_BUTTON_WIDTH                       @"done_button_width"
#define DONE_BUTTON_TEXT_COLOR                  @"done_button_text_color"
#define DONE_BUTTON_TEXT                        @"done_button_text"



@protocol MMAdManagerProtocol <NSObject>
@optional
- (void) onMMAdConfigLoaded;
- (void) onMMAdConfigFailedWithError:(NSError*)error;
@end

@interface MMAdManager : NSObject {
    // private
    BOOL isLoading;
    NSDictionary *configDic;
}

// public
@property (nonatomic, assign) id<MMAdManagerProtocol> delegate;
@property (nonatomic, strong) NSString* advertising_tree_force_replace;
@property (nonatomic, strong) NSDictionary* lastMakoCatDFP;
@property (nonatomic, strong) NSDictionary* lastContext;
@property (nonatomic, strong) NSDictionary* server_variables;
@property (nonatomic, strong) NSString* current_vcm_id;
@property (nonatomic, strong) NSString* current_user_uuid;
@property (nonatomic, strong) NSString* app_id;
@property (nonatomic, strong) MLManager *makoLocationManager;
@property (nonatomic) int locationExpiryForAppUsageReport;
@property (nonatomic) BOOL isPayingUser;
@property (nonatomic) BOOL isPaidContent;
@property (nonatomic) BOOL isResumeVideoPlay;
@property (nonatomic) BOOL externalDeviceIsPlayingTheVideo;
@property (nonatomic) CGFloat interstitialGracePeriodSeconds;
@property (nonatomic) NSTimeInterval firstInterstitialShowTimeInSeconds;
@property (nonatomic) float ad_volume;


+ (MMAdManager*) getInstance;

// Application needs to call this method at least once to load the configuration file.
// this will load the conifguration asynchronously.
// If it finished loading the config file successfully the application can ask for specific banner/interstitial configuration,
// else (if it didn't finish or failed to load) it will return nil
- (void) loadConfigurationFileFromURL:(NSURL*)configFileURL locationExpiryForAppUsageReport:(int)locationExpiryForAppUsageReport andDelegate:(id<MMAdManagerProtocol>)delegate;

- (NSDictionary*)formatDictionaryForIU:(NSString*)currentIU formatID:(NSString*)formatIdString;
- (NSDictionary*)defaultMakoCatDFP;
- (NSDictionary*)homepageMakoCatDFP;
- (NSString*)IMAurl;
- (NSDictionary*)makoCatDFPDictForAreaID:(NSString*)areaID;
- (NSDictionary*)adAreaConfigForForIU:(NSString*)iu;
- (void)refreshPageLoadServerVariables;
- (NSString*)defaultVidId;
- (NSString*)defaultLiveVidId;

- (NSDictionary*)getDoneButtonPreferences;

@end
