//
//  MMAdManager.m
//  MMAd
//
//  Copyright (c) 2013 Mako. All rights reserved.
//

#import "MMAdManager.h"
#import "MMAdUtil.h"
#import "MMBannerView.h"
#import "MMInterstitialAd.h"
#import "MMVideoConfiguration.h"

@implementation MMAdManager
{
    NSMutableArray *timers;
    NSMutableArray *varsToRefreshOnPageLoad;
}
static MMAdManager *adManager;

+ (MMAdManager*) getInstance{
    
    // create a new object only once
    static dispatch_once_t once;
    dispatch_once(&once, ^ { adManager = [[MMAdManager alloc] init];});
    return adManager;
}

-(void)dealloc
{
    for (NSTimer *timer in timers) {
        [timer invalidate];
    }
    [timers removeAllObjects];
}
#pragma mark Load Configuration

- (void) loadConfigurationFileFromURL:(NSURL*)configFileURL locationExpiryForAppUsageReport:(int)locationExpiryForAppUsageReport andDelegate:(id<MMAdManagerProtocol>)delegate{
    @synchronized(self) {
        if (isLoading){
            AdLog(@"Warning: MMAdManager is trying to load a configuration file while it is currenlty loading");
            return;
        } else {
            self.locationExpiryForAppUsageReport = locationExpiryForAppUsageReport;
            self.delegate = delegate;
            isLoading = YES;
            // create a request and load the config file
            NSURLRequest *request = [NSURLRequest requestWithURL:configFileURL cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:kAD_MANAGER_LOADING_TIMEOUT];
            
            AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
            
            __weak id<MMAdManagerProtocol> weakDelegate = delegate;
            [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *op, id responseObject){
                
                NSError* error = nil;
                
                configDic = [NSJSONSerialization JSONObjectWithData: [op.responseString dataUsingEncoding:NSUTF8StringEncoding]
                                                            options: NSJSONReadingMutableContainers
                                                              error: &error];
                if (configDic[@"interstitial_grace_period_seconds"]) {
                    [MMAdManager getInstance].interstitialGracePeriodSeconds = [configDic[@"interstitial_grace_period_seconds"] floatValue];
                }
                
                if(configDic[@"force-replace-tree"])
                {
                    [MMAdManager getInstance].advertising_tree_force_replace = configDic[@"force-replace-tree"];
                }
                
                if(configDic[@"ad_volume"] && !isnan([configDic[@"ad_volume"] floatValue]))
                {
                    float adVolume = [configDic[@"ad_volume"] floatValue];
                    [MMAdManager getInstance].ad_volume = adVolume / 100.0;
                }
                
                if(configDic[@"mako_location_config_url"]) {
                    self.makoLocationManager = [MLManager sharedInstance];
                    [self.makoLocationManager loadWithUrl:configDic[@"mako_location_config_url"]
                          locationExpiryForAppUsageReport:locationExpiryForAppUsageReport
                                                  success:^{
                                                      [self.makoLocationManager start];
                                                  } failed:^{
                                                      
                                                  }
                     ];
                }
                isLoading = NO;
                
                [self startServerVariables];
                
                if (weakDelegate && [weakDelegate respondsToSelector:@selector(onMMAdConfigLoaded)])
                {
                    [weakDelegate onMMAdConfigLoaded];
                }
            }
                                             failure:^(AFHTTPRequestOperation *op, NSError *error){
                                                 AdLog(@"failure:%@", error);
                                                 isLoading = NO;
                                                 if (weakDelegate && [weakDelegate respondsToSelector:@selector(onMMAdConfigFailedWithError:)])
                                                     [weakDelegate onMMAdConfigFailedWithError:error];
                                                 
                                             }];
            [operation start];
            
        }
    }
}

-(void)startServerVariables
{
    
    timers = [[NSMutableArray alloc] init];
    varsToRefreshOnPageLoad = [[NSMutableArray alloc] init];
    for (NSDictionary *server_variable in configDic[@"server_variables"]) {
        if([server_variable[@"active"] isEqualToString:@"true"])
        {
            if ([server_variable[@"refresh_on_load_page"] isEqualToString:@"true"]) {
                [varsToRefreshOnPageLoad addObject:server_variable];
            }
            if ([server_variable[@"polling_interval"] integerValue] > 0) {
                NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:[server_variable[@"polling_interval"] integerValue] target:self selector:@selector(serverVariablesTimerTick:) userInfo:server_variable repeats:YES];
                [timers addObject:timer];
            }
        }
    }
    [self refreshPageLoadServerVariables];
}

-(void)serverVariablesTimerTick:(NSTimer*)timer
{
    NSDictionary *server_variable = timer.userInfo;
    [self refreshServerVariable:server_variable];
}

-(void)refreshPageLoadServerVariables
{
    for (NSDictionary *server_variable in varsToRefreshOnPageLoad) {
        [self refreshServerVariable:server_variable];
    }
    if(varsToRefreshOnPageLoad.count == 0)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"serverVariablesLoaded" object:nil];
    }
}

-(void)refreshServerVariable:(NSDictionary*)server_variable
{
    if(![MMAdManager getInstance].server_variables)
    {
        [MMAdManager getInstance].server_variables = [[NSMutableDictionary alloc] init];
    }
    NSURL *url = [NSURL URLWithString:server_variable[@"url"]];
    
    AFHTTPClient *httpClient = [[AFHTTPClient alloc] initWithBaseURL:url];
    NSMutableURLRequest *request = [httpClient requestWithMethod:@"GET"
                                                            path:[url absoluteString]
                                                      parameters:nil];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    [httpClient registerHTTPOperationClass:[AFHTTPRequestOperation class]];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *response = operation.responseString;
        NSArray *replacements = server_variable[@"string_replace"];
        for (NSDictionary* replacement in replacements) {
            NSString *search_string = replacement[@"search_string"];
            NSString *replace_string = replacement[@"replace_string"];
            response = [response stringByReplacingOccurrencesOfString:search_string withString:replace_string];
        }
        [[MMAdManager getInstance].server_variables setValue:response forKey:server_variable[@"variable_string"]];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"serverVariablesLoaded" object:nil];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"serverVariablesLoaded" object:nil];
    }];
    
    [operation start];
}

-(NSDictionary*)defaultMakoCatDFP
{
    return configDic[@"defaultMakoCatDFP"];
}

-(NSDictionary*)homepageMakoCatDFP
{
    return configDic[@"homepageMakoCatDFP"];
}

-(NSString*)IMAurl {
    return configDic[@"IMA_url"];
}

-(NSString*)defaultVidId {
    return configDic[@"default_vid_id"];
}

-(NSString*)defaultLiveVidId {
    return configDic[@"default_live_vid_id"];
}

-(NSDictionary*)makoCatDFPDictForAreaID:(NSString*)areaID {
    
    NSDictionary * dict = [[MMAdManager getInstance] defaultMakoCatDFP];
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionaryWithDictionary:dict];
    
    if (mutableDict) {
        NSString *iuString = mutableDict[@"iu"];
        
        if (iuString) {
            iuString = [iuString stringByReplacingOccurrencesOfString:@"default" withString:areaID];
            [mutableDict setObject:iuString forKey:@"iu"];
            return mutableDict;
        }
    }
    
    return nil;
}



-(NSDictionary*)adAreaConfigForForIU:(NSString*)iu
{
    
    NSArray *adAreasArray = [configDic objectForKey:@"ad_areas"];
    
    NSArray *iuParts = [iu componentsSeparatedByString:@"/"];
    
    for (int i=(int)iuParts.count; i > 2 ; i--) {
        
        NSMutableArray *parts = [[NSMutableArray alloc] init];
        
        for (int j=2; j<i; j++) {
            
            if ([iuParts count] > j) {
                [parts addObject:iuParts[j]];
            }
        }
        
        NSString *subIU = [parts componentsJoinedByString:@"/"];
        
        for (NSDictionary *adAreaDict in adAreasArray) {
            NSString *areaID = adAreaDict[@"area_id"];
            if ([areaID isEqualToString:subIU]) {
                return adAreaDict;
            }
        }
    }
    return nil;
}

-(NSDictionary*)formatDictionaryForIU:(NSString*)currentIU formatID:(NSString*)formatIdString {
    if(!currentIU)
    {
        currentIU = [self defaultMakoCatDFP][@"iu"];
    }
    if([[currentIU substringFromIndex:currentIU.length - 1] isEqualToString:@"/"])
    {
        currentIU = [currentIU substringToIndex:currentIU.length - 1];
    }
    currentIU = [NSString stringWithFormat:@"%@/exact", currentIU];
    NSDictionary *areaConfig = [self adAreaConfigForForIU:currentIU];
    id formats = [areaConfig objectForKey:@"formats"];
    if ([formats isKindOfClass:[NSArray class]]) {
        NSArray *formatsArray = (NSArray*)formats;
        return [self formatDictionary:formatsArray ForFormatId:formatIdString];
    }
    
    return nil;
}

-(NSDictionary*)formatDictionary:(NSArray*)formatsArray ForFormatId:(NSString*)formatIdString {
    for (NSDictionary *format in formatsArray) {
        if ([[format objectForKey:@"id"] isEqualToString:formatIdString]) {
            return format;
        }
    }
    return nil;
}



/*
 
 "done_button_location": "top-left"
	"done_button_margin_x": "10"
	"done_button_margin_y": "10"
 
 */
- (NSDictionary*)getDoneButtonPreferences {
    
    return configDic[DONE_BUTTON];
}


@end
