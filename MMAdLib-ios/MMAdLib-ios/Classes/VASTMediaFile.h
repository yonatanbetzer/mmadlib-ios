//
//  VASTMediaFile.h
//  MMAdLib-ios
//
//  Created by Yonatan Betzer on 10/26/14.
//  Copyright (c) 2014 mako. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VASTTrackable.h"

@interface VASTMediaFile : VASTTrackable

@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSString *delivery;
@property (nonatomic, strong) NSString *type;
@property (nonatomic) CGFloat width;
@property (nonatomic) CGFloat height;

+(VASTMediaFile*)fromDictionary:(NSDictionary*)dictionary;
+(NSMutableArray*)fromArray:(NSArray*)array;
-(NSURL*)playableUrl;
@end
