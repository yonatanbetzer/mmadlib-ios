//
//  MMInterstitial.m
//  Mako
//
//  Created by Rona Assouline on 6/17/14.
//
//

#import "MMInterstitial.h"

@interface MMInterstitial()

@end

@implementation MMInterstitial

#pragma mark - Application interface

// when the interstitial is done loading
- (void)interstitialAdActionDidFinish:(MMInterstitial *)interstitial{
    
}

- (void)setSupportedOrientations:(NSUInteger)supportedOrientations{
    // set supported orientations for view controller
    interstitialViewController.supportedOrientations = supportedOrientations;
}

- (void)interstitialAdDidLoadAd{
    self.loaded = YES;
    if (self.delegate && [self.delegate respondsToSelector:@selector(interstitialDidLoad:)]){
        [self.delegate interstitialDidLoad:self];
    }
}

- (void)interstitialAdDidFailWithError:(NSError *)error{
    if ([self.delegate respondsToSelector:@selector(interstitial:didFailLoadingWithError:)])
        [self.delegate interstitial:self didFailLoadingWithError:error];
}


- (void) interstitialAdWasDismissed {
    [UIApplication sharedApplication].statusBarHidden = statusBarWasHiddenInTheApp;

//    if ([interstitialViewController respondsToSelector:@selector(dismissViewControllerAnimated:completion:)])
//        NSLog(@"Will dismiss");
}


@end
