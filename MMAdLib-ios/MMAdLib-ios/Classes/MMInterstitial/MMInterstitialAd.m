//
//  MMInterstitialAd.m
//  MMAd
//
//  Copyright (c) 2013 Mako. All rights reserved.
//

#import "MMInterstitialViewController.h"

#import "MMInterstitialAd.h"
#import "MMAdUtil.h"
#import "MMAdManager.h"
#import "DFPInterstitial.h"
#import "MMParameterStringReplacement.h"
#import "GADRequest.h"

@interface MMInterstitialAd (Private) <MMInterstitialViewControllerDelegate, GADInterstitialDelegate>



@end


@implementation MMInterstitialAd {
    UIViewController *rootViewController;
    BOOL didDismissScreen;
}

- (void) dealloc {
    interstitialViewController.interstitialControllerDelegate = nil;
    
    if ([interstitialViewController respondsToSelector:@selector(presentingViewController)]){
        if (interstitialViewController.presentingViewController)
            [self interstitialAdWasDismissed];
    }
}

- (id)initHomepageWithReplaceDict:(NSDictionary *)appStateDict
                         formatID:(NSString*)formatIDString
                         delegate:(id<MMInterstitialDelegate>)delegateParam
{
    return [self initWithMakoCatDFPDict:[[MMAdManager getInstance] homepageMakoCatDFP]
                            replaceDict:appStateDict
                               formatID:formatIDString
                               delegate:delegateParam];
}

- (id)initWithMakoCatDFPDict:(NSDictionary*)catDFPDict
                 replaceDict:(NSDictionary *)appStateDict
                    formatID:(NSString*)formatIDString
                    delegate:(id<MMInterstitialDelegate>)delegateParam
{
    self = [super init];
    
    if (self) {
        
        self.delegate = delegateParam;
        NSTimeInterval timeInSeconds = [[NSDate date] timeIntervalSince1970];
        NSTimeInterval timePassedInSeconds = timeInSeconds - [MMAdManager getInstance].firstInterstitialShowTimeInSeconds;
        
        if (timePassedInSeconds < [MMAdManager getInstance].interstitialGracePeriodSeconds) {
            [self interstitialDidDismissScreen:nil];
        }
        else
        {
            
            [MMAdManager getInstance].firstInterstitialShowTimeInSeconds = [[NSDate date] timeIntervalSince1970];
            
            interstitial = [[DFPInterstitial alloc] init];
            interstitial.delegate = self;
            
            if(!catDFPDict)
            {
                catDFPDict =[[MMAdManager getInstance] defaultMakoCatDFP];
            }
            
            NSString *iu = catDFPDict[@"iu"];
            NSDictionary *format = [[MMAdManager getInstance] formatDictionaryForIU:iu formatID:formatIDString];
            NSDictionary *config = [[MMAdManager getInstance] adAreaConfigForForIU:iu];
            if(config && config[@"force_replace_iu"])
            {
                iu = config[@"force_replace_iu"];
            }
            if([MMAdManager getInstance].advertising_tree_force_replace)
            {
                iu = [MMAdUtil stringByReplacingTreeOnIU:iu];
            }
            NSDictionary *makoCatDFPDictExtration = [MMAdUtil extractMakoCatDFPDictionary:catDFPDict];
            NSDictionary *makoCatDFP = [[[MMParameterStringReplacement alloc] init] replaceVariablesInCustParams:makoCatDFPDictExtration];
            
            if(format)
            {
                if(iu.length > 0 && [[iu substringFromIndex:iu.length - 1] isEqualToString:@"/"])
                {
                    iu = [iu substringToIndex:iu.length - 1];
                }
                interstitial.adUnitID= [NSString stringWithFormat:@"%@/%@",iu, [format objectForKey:@"name"]];
                
                interstitialRequest = [MMAdUtil createRequestWithDict:makoCatDFP];
                
                self.supportedOrientations = IS_DEVICE_IPAD() ? UIInterfaceOrientationMaskLandscape : UIInterfaceOrientationMaskPortrait;
            }
            else
            {
                [self interstitialDidDismissScreen:nil];
            }
        }
    }
    
    return self;
}


-(void)loadRequest {
    
    if (interstitialRequest) {
        [interstitial loadRequest:interstitialRequest];
    }
    
    else {
        [self interstitialDidDismissScreen:nil];
    }
}


#pragma mark - GADInterstitialDelegate


- (void)interstitialDidReceiveAd:(GADInterstitial *)ad {
    rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    [interstitial presentFromRootViewController:rootViewController];
    [self interstitialAdDidLoadAd];
}

- (void)interstitial:(GADInterstitial *)ad didFailToReceiveAdWithError:(GADRequestError *)error {
    [self interstitialAdDidFailWithError:error];
}

- (void)interstitialDidDismissScreen:(GADInterstitial *)ad{
    if (didDismissScreen) return;
    didDismissScreen = YES;
    if ([self.delegate respondsToSelector:@selector(interstitialWasDismissed:)])
        [self.delegate interstitialWasDismissed:self];
    
}

- (void)interstitialWillDismissScreen:(GADInterstitial *)interstitial {
    [self performSelector:@selector(interstitialDidDismissScreen:) withObject:self afterDelay:2];
}


@end
