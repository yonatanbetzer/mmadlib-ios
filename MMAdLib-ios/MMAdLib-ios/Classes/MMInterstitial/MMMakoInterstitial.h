//
//  MMMakoInterstitial.h
//  Mako
//
//  Created by Rona Assouline on 6/17/14.
//
//

#import <Foundation/Foundation.h>
#import "DFPInterstitial.h"
#import "MMInterstitial.h"


@class MMInterstitialViewController;

@interface MMMakoInterstitial : MMInterstitial <UIWebViewDelegate, MMLandingPageDelegate>

- (id)initWithMakoCatDFPDict:(NSDictionary*)catDFPDict
                 replaceDict:(NSDictionary *)appStateDict
                    formatID:(NSString*)formatIDString
                    delegate:(id<MMInterstitialDelegate>)delegate;

- (id)initHomepageWithReplaceDict:(NSDictionary *)appStateDict
                         formatID:(NSString*)formatIDString
                    delegate:(id<MMInterstitialDelegate>)delegate;
- (void) load;
- (void) unload;
- (BOOL) presentFullScreen;

@end
