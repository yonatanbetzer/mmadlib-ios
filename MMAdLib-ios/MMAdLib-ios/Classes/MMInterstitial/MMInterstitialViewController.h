//
//  MMInterstitialViewController.h
//  MMAd
//
//  Copyright (c) 2013 Mako. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MMLandingPageViewController.h"

@protocol MMInterstitialViewControllerDelegate <NSObject>
@optional


@required
- (void)interstitialAdDidLoadAd;
- (void)interstitialAdWasDismissed;
- (void)interstitialAdDidFailWithError:(NSError *)error;
@end



@interface MMInterstitialViewController : UIViewController <UIWebViewDelegate, MMLandingPageDelegate> {

    BOOL isLoading;
    UIWebView *interstitialWebView;
    UIButton *btnClose;
    UIButton *btnCloseTop;
    NSInteger _webViewLoads;           // counter for webview requests and replies.
    BOOL _didGetSuccessString;         // flag that indicates we got success during one of the calls.
    BOOL _pageTerminationWasDelegated; // after we report to the application that the page was loaded/failed, no more reports should be send
    
    NSTimer *interstitialLoadTimedOutTimer;
    
    
    NSTimer *autoCloseTimer;
    NSInteger autoCloseTime;
    
    // interstitial action
    // MMLandingPageViewController *landingPageViewController;
    
    // report system
    NSURL *reportURL;
    
//    BOOL didSendLoadStatusToDelegate;
}
@property (nonatomic, weak) id <MMInterstitialViewControllerDelegate> interstitialControllerDelegate;
@property (nonatomic, strong) NSString *areaId;
@property (nonatomic, strong) NSString *category;
@property (nonatomic) NSUInteger supportedOrientations;

- (void) loadWithUrlString:(NSString*)urlString timeOut:(NSInteger)timeOutInteger;
- (void) unload;
@end
