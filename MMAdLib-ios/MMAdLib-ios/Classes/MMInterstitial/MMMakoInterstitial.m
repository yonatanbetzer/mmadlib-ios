//
//  MMMakoInterstitial.m
//  Mako
//
//  Created by Rona Assouline on 6/17/14.
//
//

#import "MMMakoInterstitial.h"
#import "MMInterstitialViewController.h"
#import "MMParameterStringReplacement.h"
#import "MMAdLib.h"


@interface MMMakoInterstitial () <MMInterstitialViewControllerDelegate, GADInterstitialDelegate>

@property (nonatomic, strong) NSDictionary *makoInterstitialForamtDict;

@end

@implementation MMMakoInterstitial


- (id)initHomepageWithReplaceDict:(NSDictionary *)appStateDict
                    formatID:(NSString*)formatIDString
                         delegate:(id<MMInterstitialDelegate>)delegate
{
    return [self initWithMakoCatDFPDict:[[MMAdManager getInstance] homepageMakoCatDFP]
                            replaceDict:appStateDict
                               formatID:formatIDString
                               delegate:delegate];
}

- (id)initWithMakoCatDFPDict:(NSDictionary*)catDFPDict
                 replaceDict:(NSDictionary *)appStateDict
                    formatID:(NSString*)formatIDString
                    delegate:(id<MMInterstitialDelegate>)delegate
{
    
    self = [super init];
    if (self) {
        self.delegate = delegate;
        catDFPDict = [MMAdUtil extractMakoCatDFPDictionary:catDFPDict];
        if(!catDFPDict)
        {
            catDFPDict =[[MMAdManager getInstance] defaultMakoCatDFP];
        }
        

        NSString *iu = catDFPDict[@"iu"];
        NSDictionary *format = [[MMAdManager getInstance] formatDictionaryForIU:iu formatID:formatIDString];
        NSDictionary *config = [[MMAdManager getInstance] adAreaConfigForForIU:iu];
        if(config && config[@"force_replace_iu"])
        {
            iu = config[@"force_replace_iu"];
        }
        if([MMAdManager getInstance].advertising_tree_force_replace)
        {
            iu = [MMAdUtil stringByReplacingTreeOnIU:iu];
        }
        
        self.makoInterstitialForamtDict = format;
        
        interstitialViewController = [[MMInterstitialViewController alloc] initWithNibName:nil bundle:nil];
        interstitialViewController.interstitialControllerDelegate = self;
        
        self.supportedOrientations = IS_DEVICE_IPAD() ? UIInterfaceOrientationMaskLandscape : UIInterfaceOrientationMaskPortrait;
    }
    return self;
}


- (void) load {
    self.loaded = NO;
    if(self.makoInterstitialForamtDict)
    {
        NSString *urlString = [self.makoInterstitialForamtDict objectForKey:@"url"];
        NSInteger timeOutInteger = (NSInteger)[self.makoInterstitialForamtDict objectForKey:@"request_timeout"];
        
        [interstitialViewController loadWithUrlString:urlString timeOut:timeOutInteger];
    }
    
    else {
        if ([self.delegate respondsToSelector:@selector(interstitial:didFailLoadingWithError:)])
            [self.delegate interstitial:self didFailLoadingWithError:nil];
        
    }
}
- (void) unload{
    self.loaded = NO;
    [interstitialViewController unload];
}

- (void)interstitialAdDidLoadAd
{
    [super interstitialAdDidLoadAd];
    self.loaded = YES;
    [self presentFullScreen];
}


// when the interstitial exist button was pressed
- (void)interstitialAdDidUnload:(MMInterstitial *)interstitial{
    
}

- (void)interstitialAdWasDismissed {
    [self unload];
//    NSLog(@"Mako interstitial will dismiss");
    [interstitialViewController dismissViewControllerAnimated:YES completion:nil];
    
    
    if ([self.delegate respondsToSelector:@selector(interstitialWasDismissed:)])
        [self.delegate interstitialWasDismissed:self];
}


- (void)interstitialAdDidFailWithError:(NSError *)error {
 
    if ([self.delegate respondsToSelector:@selector(interstitialWasDismissed:)])
        [self.delegate interstitialWasDismissed:self];
    
}


- (void)interstitialAd:(MMInterstitial *)interstitial didFailWithError:(NSError *)error {
    if ([self.delegate respondsToSelector:@selector(interstitialWasDismissed:)])
        [self.delegate interstitialWasDismissed:self];
}

- (BOOL) presentFullScreen {
    if (self.isLoaded){
        // present the interstitial
        statusBarWasHiddenInTheApp = [UIApplication sharedApplication].statusBarHidden;
        [UIApplication sharedApplication].statusBarHidden = YES;
        
        UIViewController *rootVC = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
        UIViewController *vcToPresentOn = rootVC;
        
        if ([vcToPresentOn respondsToSelector:@selector(presentedViewController)]){
            while (vcToPresentOn.presentedViewController){
                if ([vcToPresentOn respondsToSelector:@selector(presentedViewController)])
                {
                    vcToPresentOn = vcToPresentOn.presentedViewController;
                }
            }
        }
        
        if ([vcToPresentOn respondsToSelector:@selector(presentViewController:animated:completion:)]){
            [vcToPresentOn presentViewController:interstitialViewController animated:NO completion:^{
                
            }];
        }
        
        return YES;
    } else {
        return NO;
    }
}


- (void) dealloc {
    
    interstitialViewController.interstitialControllerDelegate = nil;
    // dismiss the interstitial
    
}
@end
