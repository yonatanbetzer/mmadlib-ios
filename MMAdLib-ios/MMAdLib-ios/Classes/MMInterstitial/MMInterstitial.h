//
//  MMInterstitial.h
//  Mako
//
//  Created by Rona Assouline on 6/17/14.
//
//

#import <Foundation/Foundation.h>
#import "MMInterstitialViewController.h"

@class MMInterstitialViewController;
@class MMInterstitial;

@protocol MMInterstitialDelegate <NSObject>

@optional

// when the interstitial is done loading
- (void)interstitialDidLoad:(MMInterstitial *)interstitial;
// if it failed to load the ad.
- (void)interstitial:(MMInterstitial *)interstitial didFailLoadingWithError:(NSError *)error;
// when the interstitial exist button was pressed
- (void)interstitialWasDismissed:(MMInterstitial *)interstitial;

@end


@interface MMInterstitial : NSObject <MMInterstitialViewControllerDelegate>{
    MMInterstitialViewController *interstitialViewController;
    BOOL statusBarWasHiddenInTheApp;
}

@property(nonatomic, weak) id<MMInterstitialDelegate> delegate;
@property (nonatomic, getter=isLoaded) BOOL loaded;
@property (nonatomic) NSUInteger supportedOrientations;


@end
