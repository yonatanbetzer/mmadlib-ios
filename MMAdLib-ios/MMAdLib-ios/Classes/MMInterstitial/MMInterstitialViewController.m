//
//  MMInterstitialViewController.m
//  MMAd
//
//  Copyright (c) 2013 Mako. All rights reserved.
//

#import "MMInterstitialViewController.h"
#import "MMAdManager.h"
#import "MMAdUtil.h"

#define BTN_CLOSE_W 50
#define BTN_CLOSE_H 50
#define TRANSPARENT_PADDING 6
#define DIAL_ALERT_TAG 2


@interface MMInterstitialViewController ()

@end

@implementation MMInterstitialViewController
{
    // MMLandingPageViewController *landingPageViewController;
}

- (void) dealloc {
    //    landingPageViewController.landingPageDelegate = nil;
    [autoCloseTimer invalidate];
    autoCloseTimer = nil;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        interstitialWebView = [[UIWebView alloc] initWithFrame:CGRectMake(-1, -1, 1, 1)];
        interstitialWebView.delegate = self;
        self.supportedOrientations = IS_DEVICE_IPAD() ? UIInterfaceOrientationMaskLandscape : UIInterfaceOrientationMaskPortrait;
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    interstitialWebView.frame = self.view.bounds;
    [self.view addSubview:interstitialWebView];
    
    // add the X button
    btnClose = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, BTN_CLOSE_W,BTN_CLOSE_H)];
    [btnClose setImageEdgeInsets:UIEdgeInsetsMake(5, 5, 20, 20)];
    [btnClose setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
    [btnClose addTarget:self action:@selector(dismissInterstitial) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnClose];
    
    // hide status bar in ios7
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)])
    {
        [self prefersStatusBarHidden];
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    }
    
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

-(void)viewWillLayoutSubviews{
    interstitialWebView.frame = self.view.bounds;
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (autoCloseTime > 0)
        autoCloseTimer = [NSTimer scheduledTimerWithTimeInterval:autoCloseTime target:self selector:@selector(dismissInterstitial) userInfo:nil repeats:NO];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [autoCloseTimer invalidate];
    autoCloseTimer = nil;
}

- (void) dismissInterstitial {
    [self.interstitialControllerDelegate interstitialAdWasDismissed];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) loadWithUrlString:(NSString*)urlString timeOut:(NSInteger)timeOutInteger {
    @synchronized(self){
        
        if (!isLoading){
            isLoading = YES;
            
            // if webview was already displayed, create a new instance to avoid showing the previous content for a brief second
            if (interstitialWebView.superview){
                // removing the old webview
                [interstitialWebView stopLoading];
                [interstitialWebView removeFromSuperview];
                
                // creating a new webview
                interstitialWebView = [[UIWebView alloc] initWithFrame:self.view.bounds];
                interstitialWebView.delegate = self;
                [self.view insertSubview:interstitialWebView belowSubview:btnClose];
            }
            
            // flags to detect that the page realy finished loading.
            _webViewLoads = 0; _didGetSuccessString = NO; _pageTerminationWasDelegated = NO;
            
            // take the url from the bannerConfig, create a request the load the webview
            
            NSURL *url = [NSURL URLWithString:urlString];
            NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
            [interstitialWebView loadRequest:request];
            
            NSInteger requestTimeout = timeOutInteger > 0 ? timeOutInteger : kINTERSTITIAL_LOAD_TIMED_OUT;
            interstitialLoadTimedOutTimer = [NSTimer scheduledTimerWithTimeInterval:requestTimeout target:self selector:@selector(interstitialLoadTimedOut:) userInfo:nil repeats:NO];
            
            // TODO: set from config
            //autoCloseTime = interstitialConfiguration.autoClose;
            
        } else {
            AdLog(@"Warning: trying to load interstitial while interstital is loading");
        }
    }
}

- (void) unload {
    @synchronized(self){
        [interstitialLoadTimedOutTimer invalidate];
        interstitialLoadTimedOutTimer = nil;
        [interstitialWebView stopLoading];
        isLoading = NO;
    }
}

- (void) interstitialLoadTimedOut:(NSTimer*)timer {
    NSError *error = [[NSError alloc] initWithDomain:@"Interstitial Load Timed Out" code:1 userInfo:nil];
    [self failedLoadingInterstitial:error];
}


# pragma mark - Interstitial Finished Loading


- (void) doneLoadingInterstitial {
    @synchronized(self){
        [interstitialLoadTimedOutTimer invalidate];
        interstitialLoadTimedOutTimer = nil;
        isLoading = NO;
    }
    if([_interstitialControllerDelegate respondsToSelector:@selector(interstitialAdDidLoadAd)])
    {
        [_interstitialControllerDelegate interstitialAdDidLoadAd];
    }
}

- (void) failedLoadingInterstitial:(NSError*)error {
    @synchronized(self){
        [interstitialLoadTimedOutTimer invalidate];
        interstitialLoadTimedOutTimer = nil;
        isLoading = NO;
    }
    if([_interstitialControllerDelegate respondsToSelector:@selector(interstitialAdDidFailWithError:)])
    {
        [_interstitialControllerDelegate interstitialAdDidFailWithError:error];
    }
}

# pragma mark - Banner Action

- (void) landingPageWasDismissed {
    [self dismissInterstitial];
}
- (void) loadLandningPageWithURL:(NSURL*)url{
    [autoCloseTimer invalidate];
    autoCloseTimer = nil;
    
    NSString * htmlBody = [interstitialWebView stringByEvaluatingJavaScriptFromString:@"document.body.innerHTML"];
    BOOL bOpenExternal = ([htmlBody rangeOfString:@"#EXTERNAL#"].location != NSNotFound);
    //
    //    landingPageViewController = [[MMLandingPageViewController alloc] initWithNibName:nil bundle:nil];
    //    UIImage *screenshotImage = [MMAdUtil screenshot];
    //    landingPageViewController.snapshotImg = screenshotImage;
    //    landingPageViewController.landingPageDelegate = self;
    //    landingPageViewController.supportedOrientations = self.supportedOrientations;
    //    landingPageViewController.openInExternalBrowser = bOpenExternal;
    //
    //    [landingPageViewController displayLandingPageWithURL:url];
    //
    //    NSLog(@"url = %@", url);
    [self dismissInterstitial];
    if(bOpenExternal)
    {
        [[UIApplication sharedApplication] openURL:url];
    }
    else
        if(url)
        {
            // - (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication
            
            id<UIApplicationDelegate> appDelegate = [UIApplication sharedApplication].delegate;
            if([appDelegate respondsToSelector:@selector(application:openURL:sourceApplication:annotation:)])
            {
                [appDelegate application:[UIApplication sharedApplication] openURL:url sourceApplication:nil annotation:nil];
            }
        }
    
    
}

#pragma mark - UIWebViewDelegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        NSURL *landingPageURL = request.URL;
        if (landingPageURL){
            [self loadLandningPageWithURL:landingPageURL];
        }
        return NO;
    }
    
    
    NSURL *requestURL = request.URL;
    NSString *scheme = [requestURL.scheme lowercaseString];
    scheme = [scheme lowercaseString];
    if ([scheme isEqualToString:@"mmad"]){
        NSString *action = [requestURL.host lowercaseString];
        if ([action isEqualToString:@"setparam"]){
            NSDictionary *dic = [MMAdUtil getArgsFromURL:requestURL];
            NSString *autoClose = [dic objectForKey:@"auto-close"];
            if ([autoClose isEqualToString:@"0"])
                [autoCloseTimer invalidate];
            autoCloseTimer = nil;
        } else if ([action isEqualToString:@"performaction"]){
            NSDictionary *dic = [MMAdUtil getArgsFromURL:requestURL];
            NSString *type = [dic objectForKey:@"type"];
            if ([type isEqualToString:@"skip"]){
                [self dismissInterstitial];
            }
        }
        return NO;
    }
    
    _webViewLoads++;
    
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView{
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    // remove the margin from the html, because the image in the banner fits the size of the banner, but the recieved html is usually larger
    _webViewLoads--;
    
    [webView stringByEvaluatingJavaScriptFromString:@"document.body.style.margin='0';"];
    
    [self searchAndSetTheReportURL:webView];
    
    NSString * htmlBody = [webView stringByEvaluatingJavaScriptFromString:@"document.body.innerHTML"];
    
    // try to find kAD_SUCCESS_STRING to signal as was loaded successfuly
    if ([htmlBody rangeOfString:kAD_SUCCESS_STRING].location != NSNotFound)
        _didGetSuccessString = YES;
    
    // if we got answers for all load requests, and got a success string on one of the calls, we can report success load of the banner
    if (!_pageTerminationWasDelegated  && _didGetSuccessString){
        _pageTerminationWasDelegated = YES;
        [self doneLoadingInterstitial];
    }
    
    // if we got ansers for all load requets, but did not get the success string in any of them, we can report faliure.
    if (!_pageTerminationWasDelegated && _webViewLoads <=0 && !_didGetSuccessString){
        _pageTerminationWasDelegated = YES;
        NSError *error = [[NSError alloc] initWithDomain:kMMAdDomainError code:kMMAdNoAdSuccessString userInfo:nil];
        [self failedLoadingInterstitial:error];
    }
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    _webViewLoads--;
    
    if (!_pageTerminationWasDelegated && _webViewLoads <= 0 && !_didGetSuccessString){
        _pageTerminationWasDelegated = YES;
        [self failedLoadingInterstitial:error];
    }
    if (!_pageTerminationWasDelegated && _webViewLoads <= 0 && _didGetSuccessString){
        _pageTerminationWasDelegated = YES;
        [self doneLoadingInterstitial];
    }
    
}

#pragma mark - Report System
- (void) searchAndSetTheReportURL:(UIWebView*)webView {
    NSString *html = [webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.outerHTML"];
    NSString *strAdReportURL = [MMAdUtil getAdPlayUrl:html];
    if (strAdReportURL)
        reportURL = [[NSURL alloc] initWithString:strAdReportURL];
}

#pragma mark - Orientation
-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return YES;
}

// iOS6
- (BOOL) shouldAutorotate{
    return YES;
}
- (NSUInteger)supportedInterfaceOrientations {
    return IS_DEVICE_IPAD() ? UIInterfaceOrientationMaskLandscape : UIInterfaceOrientationMaskPortrait;
}

@end
