//
//  MMInterstitialAd.h
//  MMAd
//
//  Copyright (c) 2013 Mako. All rights reserved.
//


//#import "MMInterstitialViewController.h"
#import "DFPInterstitial.h"
#import "MMInterstitial.h"

@class MMInterstitialViewController;

@class MMInterstitialAd;
@class MMInterstitialConfiguration;

//@protocol MMInterstitialViewControllerDelegate;



@interface MMInterstitialAd : MMInterstitial {
    DFPInterstitial *interstitial;
    GADRequest *interstitialRequest;
}


- (id)initWithMakoCatDFPDict:(NSDictionary*)catDFPDict
                 replaceDict:(NSDictionary *)appStateDict
                    formatID:(NSString*)formatIDString
                    delegate:(id<MMInterstitialDelegate>)delegate;


- (id)initHomepageWithReplaceDict:(NSDictionary *)appStateDict
                         formatID:(NSString*)formatIDString
                    delegate:(id<MMInterstitialDelegate>)delegate;


-(void)loadRequest;


@end
