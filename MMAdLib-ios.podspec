Pod::Spec.new do |s|
  s.name         = "MMAdLib-ios"
  s.version      = "1.1.2"
  s.summary      = "Mako Mobile Ad library for ios"
  s.homepage         = "http://www.mako.co.il"
  s.license      = { :type => 'Keshet Broadcasting Ltd.', :text => <<-LICENSE
      This property is Keshet Broadcasting Ltd. private property, and no one allow to use it beside Keshet Broadcasting Ltd. 
      LICENSE
    }
		
  s.author       = { "Ran Greenberg" => "ran.greenberg@mako.co.il" }
  s.source       = { 
    :git => "https://rangreenberg@bitbucket.org/rangreenberg/mmadlib-ios.git",
	:tag => 'v1.1.2'
  }

  s.platform     = :ios, '5.0'
  s.source_files = 'MMAdLib-ios/MMAdLib-ios/Classes/**/*.{h,m}'
  s.requires_arc = true
  s.frameworks = 'AdSupport', 'AudioToolbox', 'AVFoundation', 'CoreGraphics', 'CoreTelephony', 'MessageUI', 'StoreKit', 'SystemConfiguration'
  s.xcconfig = { "HEADER_SEARCH_PATHS" => "$(SDKROOT)/usr/include/libxml2",'OTHER_LDFLAGS' => '-ObjC -lxml2'  }
  s.dependency 'AFNetworking', '~> 1.3'
  s.dependency 'TouchXML', '~> 0.1'
  s.dependency 'Google-AdMob-Ads-SDK', '~> 6.8'
  s.dependency 'TouchJSON', '~> 1.1'

end
